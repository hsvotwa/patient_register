﻿using PatientRegister.Classes;
using PatientRegister.Models;
using PatientRegister.Repositories;
using PatientRegister.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace PatientRegister.Controllers {
    public class DiagnosisController : Controller {
        UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());

        public ActionResult Create() {
            UserSession.checkLogIn();
            return View("Edit", new Diagnosis());
        }

        public ActionResult Edit(Guid _sGuid) {
            UserSession.checkLogIn();
            Diagnosis _Diagnosis = g_UnitOfWork.Diagnoses.GetById(_sGuid);
            return View(_Diagnosis);
        }

        [HttpPost]
        public JsonResult Save(Diagnosis _Diagnosis) {
            UserSession.checkLogIn();
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                Diagnosis _DiagnosisCurr = g_UnitOfWork.Diagnoses.GetById(_Diagnosis.DiagnosisID);
                if (_DiagnosisCurr == null) {
                    g_UnitOfWork.Diagnoses.Insert(_Diagnosis);
                } else {
                    _DiagnosisCurr.Name = _Diagnosis.Name;
                    _DiagnosisCurr.StatusID = _Diagnosis.StatusID;
                }
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                    _sDescription = "Diagnosis successfully saved.";
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription });
        }

        public ActionResult List() {
            UserSession.checkLogIn();
            return View();
        }

        public ActionResult ListView(DiagnosisFilterViewModel _DiagnosisFilterViewModel) {
            UserSession.checkLogIn();
            return View(getFilteredData(_DiagnosisFilterViewModel));
        }

        public ActionResult Detail(Guid _sGuid) {
            UserSession.checkLogIn();
            return View(g_UnitOfWork.Diagnoses.GetById(_sGuid));
        }

        private IEnumerable<Diagnosis> getFilteredData(DiagnosisFilterViewModel _DiagnosisFilterViewModel) {
            return g_UnitOfWork.Diagnoses.GetFiltered(_Diagnosis =>
                (string.IsNullOrEmpty(_DiagnosisFilterViewModel.SearchText) || _Diagnosis.Name.Contains(_DiagnosisFilterViewModel.SearchText)) &&
                (_DiagnosisFilterViewModel.StatusID == (int)EnumStatus.none || _Diagnosis.StatusID == _DiagnosisFilterViewModel.StatusID)
            );
        }

        private DataSet getExportDataSet(DiagnosisFilterViewModel _DiagnosisFilterViewModel) {
            UserSession.checkLogIn();
            IEnumerable<Diagnosis> _Diagnosiss = getFilteredData(_DiagnosisFilterViewModel);
            DataSet _Ds = new DataSet("Data");
            DataTable _Dt = new DataTable("DataTable");
            _Dt.Columns.Add("name", typeof(string));
            _Dt.Columns.Add("status", typeof(string));
            foreach (Diagnosis _Diagnosis in _Diagnosiss) {
                _Dt.Rows.Add(
                    _Diagnosis.Name,
                    _Diagnosis.Status.Name
                    );
            }
            _Ds.Tables.Add(_Dt);
            return _Ds;
        }

        public ActionResult DownloadExcel(DiagnosisFilterViewModel _DiagnosisFilterViewModel) {
            try {
                string _sFilePath = ExcelExports.getExcelDocument(getExportDataSet(_DiagnosisFilterViewModel), "Diagnoses");
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //ErrorLog.handleError("Class", "", "downloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }

        public ActionResult DownloadPdf(DiagnosisFilterViewModel _DiagnosisFilterViewModel) {
            try {
                UserSession.checkLogIn();
                float[] _fColumnDefinitionSize = new float[] { 70F, 30F };
                string _sFilePath = pdfExports.generatePdfReport(getExportDataSet(_DiagnosisFilterViewModel), new string[] { }, _fColumnDefinitionSize, "Diagnoses", "Diagnoses");
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //  ErrorLog.handleError("Class", "", "DownloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }
    }
}