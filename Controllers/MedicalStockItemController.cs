﻿using PatientRegister.Classes;
using PatientRegister.Models;
using PatientRegister.Repositories;
using PatientRegister.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace PatientRegister.Controllers {
    public class MedicalStockItemController : Controller {
        UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());

        public ActionResult Create() {
            UserSession.checkLogIn();
            return View("Edit", new MedicalStockItem());
        }

        public ActionResult Edit(Guid _sGuid) {
            UserSession.checkLogIn();
            MedicalStockItem _MedicalStockItem = g_UnitOfWork.MedicalStockItems.GetById(_sGuid);
            return View(_MedicalStockItem);
        }

        [HttpPost]
        public JsonResult Save(MedicalStockItem _MedicalStockItem) {
            UserSession.checkLogIn();
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                MedicalStockItem _MedicalStockItemCurr = g_UnitOfWork.MedicalStockItems.GetById(_MedicalStockItem.MedicalStockItemID);
                if (_MedicalStockItemCurr == null) {
                    g_UnitOfWork.MedicalStockItems.Insert(_MedicalStockItem);
                } else {
                    _MedicalStockItemCurr.Name = _MedicalStockItem.Name;
                    _MedicalStockItemCurr.StatusID = _MedicalStockItem.StatusID;
                }
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                    _sDescription = "Medical Stock Item successfully saved.";
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription });
        }

        public ActionResult List() {
            UserSession.checkLogIn();
            return View();
        }

        public ActionResult ListView(MedicalStockItemFilterViewModel _MedicalStockItemFilterViewModel) {
            UserSession.checkLogIn();
            IEnumerable<MedicalStockItem> _MedicalStockItems = g_UnitOfWork.MedicalStockItems.GetFiltered(_MedicalStockItem =>
                (string.IsNullOrEmpty(_MedicalStockItemFilterViewModel.SearchText) || _MedicalStockItem.Name.Contains(_MedicalStockItemFilterViewModel.SearchText)) &&
                (_MedicalStockItemFilterViewModel.StatusID == (int)EnumStatus.none || _MedicalStockItem.StatusID == _MedicalStockItemFilterViewModel.StatusID)
            );
            return View(_MedicalStockItems);
        }

        public ActionResult Detail(Guid _sGuid) {
            UserSession.checkLogIn();
            MedicalStockItem _MedicalStockItem = g_UnitOfWork.MedicalStockItems.GetById(_sGuid);
            return View(_MedicalStockItem);
        }

        public ActionResult DownloadExcel(BranchFilterViewModel _BranchFilterViewModel) {
            try {
                UserSession.checkLogIn();
                IEnumerable<MedicalStockItem> _Branches = g_UnitOfWork.MedicalStockItems.GetFiltered(_Branch =>
                    (string.IsNullOrEmpty(_BranchFilterViewModel.SearchText) || _Branch.Name.Contains(_BranchFilterViewModel.SearchText)) &&
                    (_BranchFilterViewModel.StatusID == (int)EnumStatus.none || _Branch.StatusID == _BranchFilterViewModel.StatusID)
                );
                DataSet _Ds = new DataSet("Data");
                DataTable _Dt = new DataTable("DataTable");
                _Dt.Columns.Add("name", typeof(string));
                _Dt.Columns.Add("status", typeof(string));
                foreach (MedicalStockItem _Branch in _Branches) {
                    _Dt.Rows.Add(_Branch.Name, _Branch.Status.Name);
                }
                _Ds.Tables.Add(_Dt);
                string _sFilePath = ExcelExports.getExcelDocument(_Ds, "MedicalStockItems");
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //ErrorLog.handleError("Class", "", "downloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }

        public ActionResult DownloadPdf(BranchFilterViewModel _BranchFilterViewModel) {
            try {
                UserSession.checkLogIn();
                IEnumerable<MedicalStockItem> _Symptoms = g_UnitOfWork.MedicalStockItems.GetFiltered(_Branch =>
                    (string.IsNullOrEmpty(_BranchFilterViewModel.SearchText) || _Branch.Name.Contains(_BranchFilterViewModel.SearchText)) &&
                    (_BranchFilterViewModel.StatusID == (int)EnumStatus.none || _Branch.StatusID == _BranchFilterViewModel.StatusID)
                );
                DataSet _Ds = new DataSet("Data");
                DataTable _Dt = new DataTable("DataTable");
                _Dt.Columns.Add("name", typeof(string));
                _Dt.Columns.Add("status", typeof(string));
                foreach (MedicalStockItem _Branch in _Symptoms) {
                    _Dt.Rows.Add(_Branch.Name, _Branch.Status.Name);
                }
                _Ds.Tables.Add(_Dt);
                float[] _fColumnDefinitionSize = new float[] { 100F, 100F };
                string _sFilePath = pdfExports.generatePdfReport(_Ds, new string[] { }, _fColumnDefinitionSize, "MedicalStockItems", "MedicalStockItems");
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //  ErrorLog.handleError("Class", "", "DownloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }
    }
}