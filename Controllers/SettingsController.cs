﻿using PatientRegister.Classes;
using PatientRegister.Models;
using PatientRegister.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PatientRegister.Controllers {
    public class SettingsController : Controller {
        UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());

        public ActionResult Edit() {
            UserSession.checkLogIn();
            //Referral
            ViewBag.ReferralCount = Configuration.getValue(EnumConfiguration.referral_record_count);
            //Practise Name
            ViewBag.PractiseName = Configuration.getValue(EnumConfiguration.practice_name);
            return View();
        }

        public ActionResult Detail() {
            UserSession.checkLogIn();
            //Referral
            ViewBag.ReferralCount = Configuration.getValue(EnumConfiguration.referral_record_count);
            //Practise Name
            ViewBag.PractiseName = Configuration.getValue(EnumConfiguration.practice_name);
            return View();
        }

        [HttpPost]
        public ActionResult Save(FormCollection _FormCollection) {
            UserSession.checkLogIn();
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                //Referral
                IEnumerable<Configuration> _ConfigurationReferral = g_UnitOfWork.Configurations.GetFiltered(
                    _Rec => _Rec.ConfigurationID == (int)EnumConfiguration.referral_record_count
                );
                Configuration _ConfigReferral = _ConfigurationReferral.FirstOrDefault();
                if (_ConfigReferral != null && _ConfigReferral.ConfigurationID != 0) {
                    _ConfigReferral.Value = _FormCollection[((int)EnumConfiguration.referral_record_count).ToString()];
                } else {
                    g_UnitOfWork.Configurations.Insert(new Configuration {
                        ConfigurationID = (int)EnumConfiguration.referral_record_count,
                        Value = _FormCollection[((int)EnumConfiguration.referral_record_count).ToString()],
                        Name = "Referral Record Count"
                    });
                }
                //Practise Name
                IEnumerable<Configuration> _ConfigurationPractise = g_UnitOfWork.Configurations.GetFiltered(
                    _Rec => _Rec.ConfigurationID == (int)EnumConfiguration.practice_name
                );
                Configuration _ConfigPractise = _ConfigurationPractise.FirstOrDefault();
                if (_ConfigPractise != null && _ConfigPractise.ConfigurationID != 0) {
                    _ConfigPractise.Value = _FormCollection[((int)EnumConfiguration.practice_name).ToString()];
                } else {
                    g_UnitOfWork.Configurations.Insert(new Configuration {
                        ConfigurationID = (int)EnumConfiguration.practice_name,
                        Value = _FormCollection[((int)EnumConfiguration.practice_name).ToString()],
                        Name = "Practise Name"
                    });
                }
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                    _sDescription = "Settings successfully changed.";
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription });
        }
    }
}