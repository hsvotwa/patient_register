﻿using PatientRegister.Classes;
using PatientRegister.Models;
using PatientRegister.Repositories;
using PatientRegister.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace PatientRegister.Controllers {
    public class MedicalAidCompanyController : Controller {
        UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());

        public ActionResult Create() {
            UserSession.checkLogIn();
            return View("Edit", new MedicalAidCompany());
        }

        public ActionResult Edit(Guid _sGuid) {
            UserSession.checkLogIn();
            MedicalAidCompany _MedicalAidCompany = g_UnitOfWork.MedicalAidCompanies.GetById(_sGuid);
            return View(_MedicalAidCompany);
        }

        [HttpPost]
        public JsonResult Save(MedicalAidCompany _MedicalAidCompany) {
            UserSession.checkLogIn();
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                MedicalAidCompany _MedicalAidCompanyCurr = g_UnitOfWork.MedicalAidCompanies.GetById(_MedicalAidCompany.MedicalAidCompanyID);
                if (_MedicalAidCompanyCurr == null) {
                    g_UnitOfWork.MedicalAidCompanies.Insert(_MedicalAidCompany);
                } else {
                    _MedicalAidCompanyCurr.Name = _MedicalAidCompany.Name;
                    _MedicalAidCompanyCurr.StatusID = _MedicalAidCompany.StatusID;
                }
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                    _sDescription = "Medical Aid Company successfully saved.";
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription });
        }

        public ActionResult List() {
            UserSession.checkLogIn();
            return View();
        }

        public ActionResult ListView(MedicalAidCompanyFilterViewModel _MedicalAidCompanyFilterViewModel) {
            UserSession.checkLogIn();
            IEnumerable<MedicalAidCompany> _MedicalAidCompanies = g_UnitOfWork.MedicalAidCompanies.GetFiltered(_MedicalAidCompany =>
                (string.IsNullOrEmpty(_MedicalAidCompanyFilterViewModel.SearchText) || _MedicalAidCompany.Name.Contains(_MedicalAidCompanyFilterViewModel.SearchText)) &&
                (_MedicalAidCompanyFilterViewModel.StatusID == (int)EnumStatus.none || _MedicalAidCompany.StatusID == _MedicalAidCompanyFilterViewModel.StatusID)
            );
            return View(_MedicalAidCompanies);
        }

        public ActionResult Detail(Guid _sGuid) {
            UserSession.checkLogIn();
            MedicalAidCompany _MedicalAidCompany = g_UnitOfWork.MedicalAidCompanies.GetById(_sGuid);
            return View(_MedicalAidCompany);
        }

        public ActionResult DownloadExcel(MedicalAidCompanyFilterViewModel _MedicalAidCompanyFilterViewModel) {
            try {
                UserSession.checkLogIn();
                IEnumerable<Symptom> _MedicalAidCompanies = g_UnitOfWork.Symptoms.GetFiltered(_Branch =>
                    (string.IsNullOrEmpty(_MedicalAidCompanyFilterViewModel.SearchText) || _Branch.Name.Contains(_MedicalAidCompanyFilterViewModel.SearchText)) &&
                    (_MedicalAidCompanyFilterViewModel.StatusID == (int)EnumStatus.none || _Branch.StatusID == _MedicalAidCompanyFilterViewModel.StatusID)
                );
                DataSet _Ds = new DataSet("Data");
                DataTable _Dt = new DataTable("DataTable");
                _Dt.Columns.Add("name", typeof(string));
                _Dt.Columns.Add("status", typeof(string));
                foreach (Symptom _Branch in _MedicalAidCompanies) {
                    _Dt.Rows.Add(_Branch.Name, _Branch.Status.Name);
                }
                _Ds.Tables.Add(_Dt);
                string _sFilePath = ExcelExports.getExcelDocument(_Ds, "Symptoms");
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //ErrorLog.handleError("Class", "", "downloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }

        public ActionResult DownloadPdf(MedicalAidCompanyFilterViewModel MedicalAidCompanyFilterViewModel) {
            try {
                UserSession.checkLogIn();
                IEnumerable<MedicalAidCompany> _MedicalAidCompanies = g_UnitOfWork.MedicalAidCompanies.GetFiltered(_Branch =>
                    (string.IsNullOrEmpty(MedicalAidCompanyFilterViewModel.SearchText) || _Branch.Name.Contains(MedicalAidCompanyFilterViewModel.SearchText)) &&
                    (MedicalAidCompanyFilterViewModel.StatusID == (int)EnumStatus.none || _Branch.StatusID == MedicalAidCompanyFilterViewModel.StatusID)
                );
                DataSet _Ds = new DataSet("Data");
                DataTable _Dt = new DataTable("DataTable");
                _Dt.Columns.Add("name", typeof(string));
                _Dt.Columns.Add("status", typeof(string));
                foreach (MedicalAidCompany _MedicalAidCompany in _MedicalAidCompanies) {
                    _Dt.Rows.Add(_MedicalAidCompany.Name, _MedicalAidCompany.Status.Name);
                }
                _Ds.Tables.Add(_Dt);
                float[] _fColumnDefinitionSize = new float[] { 100F, 100F };
                string _sFilePath = pdfExports.generatePdfReport(_Ds, new string[] { }, _fColumnDefinitionSize, "Symptoms", "Symptoms");
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //  ErrorLog.handleError("Class", "", "DownloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }
    }
}