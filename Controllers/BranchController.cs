﻿using PatientRegister.Classes;
using PatientRegister.Models;
using PatientRegister.Repositories;
using PatientRegister.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Web.Mvc;

namespace PatientRegister.Controllers {
    public class BranchController : Controller {
        UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());

        public ActionResult Create() {
            UserSession.checkLogIn();
            return View("Edit", new Branch());
        }

        public ActionResult Edit(Guid _sGuid) {
            UserSession.checkLogIn();
            Branch _Branch = g_UnitOfWork.Branches.GetById(_sGuid);
            return View(_Branch);
        }

        [HttpPost]
        public JsonResult Save(Branch _Branch) {
            UserSession.checkLogIn();
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                Branch _BranchCurr = g_UnitOfWork.Branches.GetById(_Branch.BranchID);
                if (_BranchCurr == null) {
                    g_UnitOfWork.Branches.Insert(_Branch);
                } else {
                    _BranchCurr.Name = _Branch.Name;
                    _BranchCurr.StatusID = _Branch.StatusID;
                }
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                    _sDescription = "Branch successfully saved.";
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription });
        }

        public ActionResult List() {
            UserSession.checkLogIn();
            return View();
        }

        public ActionResult ListView(BranchFilterViewModel _BranchFilterViewModel) {
            UserSession.checkLogIn();
            IEnumerable<Branch> _Branches = g_UnitOfWork.Branches.GetFiltered(_Branch =>
                (string.IsNullOrEmpty(_BranchFilterViewModel.SearchText) || _Branch.Name.Contains(_BranchFilterViewModel.SearchText)) &&
                (_BranchFilterViewModel.StatusID == (int)EnumStatus.none || _Branch.StatusID == _BranchFilterViewModel.StatusID)
            );
            return View(_Branches);
        }

        public ActionResult Detail(Guid _sGuid) {
            UserSession.checkLogIn();
            Branch _Branch = g_UnitOfWork.Branches.GetById(_sGuid);
            return View(_Branch);
        }

        [HttpPost]
        public ActionResult DownloadExcel(BranchFilterViewModel _BranchFilterViewModel) {
            try {
                UserSession.checkLogIn();
                IEnumerable<Branch> _Branches = g_UnitOfWork.Branches.GetFiltered(_Branch =>
                    (string.IsNullOrEmpty(_BranchFilterViewModel.SearchText) || _Branch.Name.Contains(_BranchFilterViewModel.SearchText)) &&
                    (_BranchFilterViewModel.StatusID == (int)EnumStatus.none || _Branch.StatusID == _BranchFilterViewModel.StatusID)
                );
                DataSet _Ds = new DataSet("Data");
                DataTable _Dt = new DataTable("DataTable");
                _Dt.Columns.Add("name", typeof(string));
                _Dt.Columns.Add("status", typeof(string));
                foreach (Branch _Branch in _Branches) {
                    _Dt.Rows.Add(_Branch.Name, _Branch.Status.Name);
                }
                _Ds.Tables.Add(_Dt);
                string _sFilePath = ExcelExports.getExcelDocument(_Ds, "Branches");
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //ErrorLog.handleError("Class", "", "downloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }

        [HttpPost]
        public ActionResult DownloadPdf(BranchFilterViewModel _BranchFilterViewModel) {
            try {
                UserSession.checkLogIn();
                IEnumerable<Branch> _Branches = g_UnitOfWork.Branches.GetFiltered(_Branch =>
                    (string.IsNullOrEmpty(_BranchFilterViewModel.SearchText) || _Branch.Name.Contains(_BranchFilterViewModel.SearchText)) &&
                    (_BranchFilterViewModel.StatusID == (int)EnumStatus.none || _Branch.StatusID == _BranchFilterViewModel.StatusID)
                );
                DataSet _Ds = new DataSet("Data");
                DataTable _Dt = new DataTable("DataTable");
                _Dt.Columns.Add("name", typeof(string));
                _Dt.Columns.Add("status", typeof(string));
                foreach (Branch _Branch in _Branches) {
                    _Dt.Rows.Add(_Branch.Name, _Branch.Status.Name);
                }
                _Ds.Tables.Add(_Dt);
                float[] _fColumnDefinitionSize = new float[] { 100F, 100F };
                string _sFilePath = pdfExports.generatePdfReport(_Ds, new string[] { }, _fColumnDefinitionSize, "Branches", "Branches");
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //  ErrorLog.handleError("Class", "", "DownloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }
    }
}