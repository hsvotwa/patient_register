﻿using System.Web.Mvc;

namespace PatientRegister.Controllers {
    public class AdminActionsController : Controller {
        public ActionResult InitLookupData() {
            Data.InitLookupData.clearAll();
            return Content( Data.InitLookupData.initAll() ? "Done..." : "Something went wrong..." );
        }
    }
}