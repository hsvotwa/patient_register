﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PatientRegister.Classes;
using PatientRegister.Models;
using PatientRegister.Repositories;
using PatientRegister.ViewModels;

namespace PatientRegister.Controllers {
    public class UserController : Controller {
        UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());

        public ActionResult Create() {
            UserSession.checkLogIn();
            return View("Edit", new UserClass());
        }

        public ActionResult Edit(Guid _sGuid) {
            UserSession.checkLogIn();
            UserClass _UserClass = g_UnitOfWork.UserClasses.GetById(_sGuid);
            return View(_UserClass);
        }

        [HttpPost]
        public JsonResult Save(UserClass _UserClass, FormCollection _FormCollection) {
            UserSession.checkLogIn();
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                UserClass _UserClassCurr = g_UnitOfWork.UserClasses.GetById(_UserClass.UserID);
                if (_UserClassCurr == null) {
                    _UserClass.Password = UserClass.encryptPwd(Constant.DEFAULT_USER_PWD_RESET);//Random Password and Email it
                    _UserClass.BranchID = Guid.Parse("AB0ED56F-6C76-4088-ACE4-9A1032E6215D");
                    g_UnitOfWork.UserClasses.Insert(_UserClass);
                    string _sMail = "Hi <br><br>";
                    _sMail += "Your password on " + Constant.APP_NAME + ", has been reset to your company's default.";
                    _sMail += "<br><br>";
                    _sMail += "Log in and make sure you change it.";
                    _sMail += "<br><br>";
                    Mailer.send(_UserClass.Email, "Password successful reset", _sMail);
                } else {
                    _UserClassCurr.Name = _UserClass.Name;
                    _UserClassCurr.Surname = _UserClass.Surname;
                    _UserClassCurr.StatusID = _UserClass.StatusID;
                    _UserClassCurr.StaffTypeID = _UserClass.StaffTypeID;
                    _UserClassCurr.Email = _UserClass.Email;
                    _UserClassCurr.BranchID = Guid.Parse("AB0ED56F-6C76-4088-ACE4-9A1032E6215D");
                }
                IEnumerable<UserRole> _AllAccessRoles = g_UnitOfWork.UserRoles.GetAll();
                foreach (UserRole _UserRole in _AllAccessRoles) {
                    IEnumerable<UserRoleAccess> _UserRoleAccessRecs = g_UnitOfWork.UserRoleAccesses.GetFiltered(_Rec =>
                        _Rec.UserID == _UserClass.UserID && _Rec.UserRoleID == _UserRole.UserRoleID
                     );
                    if (_UserRoleAccessRecs != null && _UserRoleAccessRecs.Count() != 0) {
                        _UserRoleAccessRecs.FirstOrDefault().AccessLevelID = int.Parse(_FormCollection[_UserRole.UserRoleID.ToString()]);
                        continue;
                    }
                    g_UnitOfWork.UserRoleAccesses.Insert(new UserRoleAccess {
                        UserRoleAccessID = Guid.NewGuid(),
                        UserID = _UserClass.UserID,
                        UserRoleID = _UserRole.UserRoleID,
                        AccessLevelID = int.Parse(_FormCollection[_UserRole.UserRoleID.ToString()])
                    });
                }
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                    _sDescription = "User successfully saved."; //Send an email if new user
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription });
        }

        [HttpPost]
        public JsonResult ResetPassword(Guid _sGuid) {
            UserSession.checkLogIn();
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                UserClass _UserClassCurr = g_UnitOfWork.UserClasses.GetById(_sGuid);
                if (_UserClassCurr == null) {
                    return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = "Data could not be saved. User Account not found." });
                }
                _UserClassCurr.Password = UserClass.encryptPwd(Constant.DEFAULT_USER_PWD_RESET); //Random Password and Email it
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                    _sDescription = "User's Password successfully reset."; //Send an email if new user
                    string _sMail = "Hi <br><br>";
                    _sMail += "Your password on " + Constant.APP_NAME + ", has been reset to your company's default.";
                    _sMail += "<br><br>";
                    _sMail += "Log in and make sure you change it.";
                    _sMail += "<br><br>";
                    Mailer.send(_UserClassCurr.Email, "Password successful reset", _sMail);
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription });
        }

        public ActionResult List() {
            UserSession.checkLogIn();
            return View();
        }

        public ActionResult ListView(UserClassFilterViewModel _UserClassFilterViewModel) {
            UserSession.checkLogIn();
            IEnumerable<UserClass> _UserClasses = g_UnitOfWork.UserClasses.GetFiltered(_UserClass =>
                (string.IsNullOrEmpty(_UserClassFilterViewModel.SearchText) || _UserClass.Name.Contains(_UserClassFilterViewModel.SearchText)
                || _UserClass.Email.Contains(_UserClassFilterViewModel.SearchText) || _UserClass.Surname.Contains(_UserClassFilterViewModel.SearchText)) &&
                (_UserClassFilterViewModel.StatusID == (int)EnumStatus.none || _UserClass.StatusID == _UserClassFilterViewModel.StatusID) &&
                (_UserClassFilterViewModel.StaffTypeID == (int)EnumStaffType.none || _UserClass.StaffTypeID == _UserClassFilterViewModel.StaffTypeID)
            );
            return View(_UserClasses);
        }

        public ActionResult Detail(Guid _sGuid) {
            UserSession.checkLogIn();
            UserClass _UserClass = g_UnitOfWork.UserClasses.GetById(_sGuid);
            return View(_UserClass);
        }

        public ActionResult DownloadExcel(UserClassFilterViewModel _UserClassFilterViewModel) {
            try {
                UserSession.checkLogIn();
                IEnumerable<UserClass> _UserClasses = g_UnitOfWork.UserClasses.GetFiltered(_UserClass =>
                (string.IsNullOrEmpty(_UserClassFilterViewModel.SearchText) || _UserClass.Name.Contains(_UserClassFilterViewModel.SearchText)
                || _UserClass.Email.Contains(_UserClassFilterViewModel.SearchText) || _UserClass.Surname.Contains(_UserClassFilterViewModel.SearchText)) &&
                (_UserClassFilterViewModel.StatusID == (int)EnumStatus.none || _UserClass.StatusID == _UserClassFilterViewModel.StatusID) &&
                (_UserClassFilterViewModel.StaffTypeID == (int)EnumStaffType.none || _UserClass.StaffTypeID == _UserClassFilterViewModel.StaffTypeID)
            );
                DataSet _Ds = new DataSet("Data");
                DataTable _Dt = new DataTable("DataTable");
                _Dt.Columns.Add("name", typeof(string));
                _Dt.Columns.Add("status", typeof(string));
                foreach (UserClass _UserClass in _UserClasses) {
                    _Dt.Rows.Add(string.Concat(_UserClass.Surname, " ", _UserClass.Name), _UserClass.Email, _UserClass.StaffType.Name, _UserClass.Status.Name);
                }
                _Ds.Tables.Add(_Dt);
                string _sFilePath = ExcelExports.getExcelDocument(_Ds, "Branches");
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //ErrorLog.handleError("Class", "", "downloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }

        public ActionResult DownloadPdf(UserClassFilterViewModel _UserClassFilterViewModel) {
            try {
                UserSession.checkLogIn();
                IEnumerable<UserClass> _UserClasses = g_UnitOfWork.UserClasses.GetFiltered(_UserClass =>
               (string.IsNullOrEmpty(_UserClassFilterViewModel.SearchText) || _UserClass.Name.Contains(_UserClassFilterViewModel.SearchText)
               || _UserClass.Email.Contains(_UserClassFilterViewModel.SearchText) || _UserClass.Surname.Contains(_UserClassFilterViewModel.SearchText)) &&
               (_UserClassFilterViewModel.StatusID == (int)EnumStatus.none || _UserClass.StatusID == _UserClassFilterViewModel.StatusID) &&
               (_UserClassFilterViewModel.StaffTypeID == (int)EnumStaffType.none || _UserClass.StaffTypeID == _UserClassFilterViewModel.StaffTypeID)
           );
                DataSet _Ds = new DataSet("Data");
                DataTable _Dt = new DataTable("DataTable");
                _Dt.Columns.Add("name", typeof(string));
                _Dt.Columns.Add("staff_type", typeof(string));
                _Dt.Columns.Add("status", typeof(string));
                foreach (UserClass _UserClass in _UserClasses) {
                    _Dt.Rows.Add(string.Concat(_UserClass.Surname, " ", _UserClass.Name), _UserClass.Email, _UserClass.StaffType.Name, _UserClass.Status.Name);
                }
                _Ds.Tables.Add(_Dt);
                float[] _fColumnDefinitionSize = new float[] { 100F, 60F, 60F };
                string _sFilePath = pdfExports.generatePdfReport(_Ds, new string[] { }, _fColumnDefinitionSize, "Users", "Users");
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //  ErrorLog.handleError("Class", "", "DownloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }
    }
}