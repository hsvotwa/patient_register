﻿using PatientRegister.Models;
using PatientRegister.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PatientRegister.Controllers {
    public class DashboardController : Controller {
        UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());

        public ActionResult Index() {
            UserSession.checkLogIn();
            int _iLastMonthMonth = DateTime.Today.AddMonths(-1).Month;
            int _iLastMonthYear = DateTime.Today.AddMonths(-1).Year;
            //New Patients
            IEnumerable<Patient> _PatientsThisMonth = g_UnitOfWork.Patients.GetFiltered(_Rec =>
                _Rec.Created.Month == DateTime.Today.Month &&
                _Rec.Created.Year == DateTime.Today.Year
            );
            int _iPatientsThisMonth = _PatientsThisMonth == null ? 0 : _PatientsThisMonth.Count();
            IEnumerable<Patient> _PatientsTodayLastMonth = g_UnitOfWork.Patients.GetFiltered(_Rec =>
                _Rec.Created.Month == _iLastMonthMonth &&
                _Rec.Created.Year == _iLastMonthYear &&
                _Rec.Created.Day <= DateTime.Today.Day
            );
            int _iPatientsTodayLastMonth = _PatientsTodayLastMonth == null ? 0 : _PatientsTodayLastMonth.Count();
            //Consultations
            IEnumerable<Consultation> _ConsultationsThisMonth = g_UnitOfWork.Consultations.GetFiltered(_Rec =>
               _Rec.Created.Month == DateTime.Today.Month &&
               _Rec.Created.Year == DateTime.Today.Year
           );
            int _iConsultationsThisMonth = _ConsultationsThisMonth == null ? 0 : _ConsultationsThisMonth.Count();
            IEnumerable<Consultation> _ConsultationsTodayLastMonth = g_UnitOfWork.Consultations.GetFiltered(_Rec =>
                _Rec.Created.Month == _iLastMonthMonth &&
                _Rec.Created.Year == _iLastMonthYear &&
                _Rec.Created.Day <= DateTime.Today.Day
            );
            int _iConsultationsTodayLastMonth = _ConsultationsTodayLastMonth == null ? 0 : _ConsultationsTodayLastMonth.Count();
            //Operations
            IEnumerable<Operation> _OperationsThisMonth = g_UnitOfWork.Operations.GetFiltered(_Rec =>
               _Rec.Created.Month == DateTime.Today.Month &&
               _Rec.Created.Year == DateTime.Today.Year
           );
            int _iOperationsThisMonth = _OperationsThisMonth == null ? 0 : _OperationsThisMonth.Count();
            IEnumerable<Operation> _OperationsTodayLastMonth = g_UnitOfWork.Operations.GetFiltered(_Rec =>
                _Rec.Created.Month == _iLastMonthMonth &&
                _Rec.Created.Year == _iLastMonthYear &&
                _Rec.Created.Day <= DateTime.Today.Day
            );
            int _iOperationsTodayLastMonth = _OperationsTodayLastMonth == null ? 0 : _OperationsTodayLastMonth.Count();
            //Consultations by gender
            int _i6MonthsAgoMonth = DateTime.Today.AddMonths(-6).Month;
            int _i6MonthsAgoYear = DateTime.Today.AddMonths(-6).Year;
            List<string> _sMonthLabels = new List<string>();
            List<int> _iMaleData = new List<int>();
            List<int> _iFemaleData = new List<int>();
            List<int> _iTotalData = new List<int>();
            DateTime _DateTmp = DateTime.Today.AddMonths(-5);
            while (_DateTmp <= DateTime.Today) {
                _sMonthLabels.Add(_DateTmp.ToString("MMM").ToUpper());
                IEnumerable<Consultation> _ConsultationMale = g_UnitOfWork.Consultations.GetFiltered(_Rec =>
                    _Rec.Created.Month == _DateTmp.Month &&
                    _Rec.Created.Year == _DateTmp.Year &&
                    _Rec.Patient.GenderID == (int)EnumGender.male
                );
                _iMaleData.Add(_ConsultationMale == null ? 0 : _ConsultationMale.Count());
                IEnumerable<Consultation> _ConsultationFemale = g_UnitOfWork.Consultations.GetFiltered(_Rec =>
                    _Rec.Created.Month == _DateTmp.Month &&
                    _Rec.Created.Year == _DateTmp.Year &&
                    _Rec.Patient.GenderID == (int)EnumGender.female
                );
                _iFemaleData.Add(_ConsultationFemale == null ? 0 : _ConsultationFemale.Count());
                _iTotalData.Add((_ConsultationMale == null ? 0 : _ConsultationMale.Count()) + (_ConsultationFemale == null ? 0 : _ConsultationFemale.Count()));
                _DateTmp = _DateTmp.AddMonths(1);
            }
            //Medical Aid Popularity
            IEnumerable<Patient> _Patients = g_UnitOfWork.Patients.GetFiltered(_Rec =>
                   _Rec.StatusID == (int)EnumStatus.active
               );
            //var _MedicalAidData = _Patients.GroupBy(_Rec => _Rec.MedicalAidCompanyID).Select(_S => _S.Count());
            var _MedicalAidData = from p in _Patients.GroupBy(_Rec => _Rec.MedicalAidCompanyID)
                                  select new {
                                      Count = p.Count(),
                                      Name = p.FirstOrDefault().MedicalAidCompanyID//p.FirstOrDefault() != null && p.FirstOrDefault().MedicalAidCompany.Name != null ? p.FirstOrDefault().MedicalAidCompany.Name : "N/A"
                                  };
            List<string> _sMedicalAidNameList = new List<string>();
            List<int> _iMedicalAidCountList = new List<int>();
            int _iTotalCountAll = _MedicalAidData.Sum(_Rec => _Rec.Count);
            foreach (var _MedicalAidDataRec in _MedicalAidData) {
                _sMedicalAidNameList.Add(string.IsNullOrEmpty(_MedicalAidDataRec.Name.ToString()) ? "N/A" : g_UnitOfWork.MedicalAidCompanies.GetById((Guid)_MedicalAidDataRec.Name).Name);
                _iMedicalAidCountList.Add(_MedicalAidDataRec.Count * 100 / _iTotalCountAll);
            }
            //ViewBag
            ViewBag.PatientsThisMonth = _iPatientsThisMonth;
            ViewBag.ConsultationsThisMonth = _iConsultationsThisMonth;
            ViewBag.OperationsThisMonth = _iOperationsThisMonth;
            ViewBag.PatientsChange = Common.getStatisticsChangeDescription(_iPatientsTodayLastMonth, _iPatientsThisMonth, _sPeriodDescription: "month");
            ViewBag.ConsultationsChange = Common.getStatisticsChangeDescription(_iConsultationsTodayLastMonth, _iConsultationsThisMonth, _sPeriodDescription: "month");
            ViewBag.OperationsChange = Common.getStatisticsChangeDescription(_iOperationsTodayLastMonth, _iOperationsThisMonth, _sPeriodDescription: "month");
            ViewBag.MonthLabels = _sMonthLabels;// string.Join("\',\'", _sMonthLabels.ToArray());
            ViewBag.MaleData = _iMaleData;
            ViewBag.FemaleData = _iFemaleData;
            ViewBag.TotalData = _iTotalData;
            ViewBag.MedicalAidNameList = _sMedicalAidNameList;
            ViewBag.MedicalAidCountList = _iMedicalAidCountList;
            return View();
        }
    }
}