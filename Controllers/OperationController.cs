﻿using PatientRegister.Classes;
using PatientRegister.Models;
using PatientRegister.Repositories;
using PatientRegister.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Web.Mvc;

namespace PatientRegister.Controllers {
    public class OperationController : Controller {
        UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());

        public ActionResult Create() {
            UserSession.checkLogIn();
            return View("Edit", new Operation());
        }

        public ActionResult Edit(Guid _sGuid) {
            UserSession.checkLogIn();
            Operation _Operation = g_UnitOfWork.Operations.GetById(_sGuid);
            return View(_Operation);
        }

        [HttpPost]
        public JsonResult Save(Operation _Operation) {
            UserSession.checkLogIn();
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                Operation _OperationCurr = g_UnitOfWork.Operations.GetById(_Operation.OperationID);
                if (_OperationCurr == null) {
                    g_UnitOfWork.Operations.Insert(_Operation);
                } else {
                    _OperationCurr.Date = _Operation.Date;
                    _OperationCurr.Anaesthetic = _Operation.Anaesthetic;
                    _OperationCurr.AnaesthetistID = _Operation.AnaesthetistID;
                    _OperationCurr.AssistantSurgeonID = _Operation.AssistantSurgeonID;
                    _OperationCurr.OperationID = _Operation.OperationID;
                    _OperationCurr.PatientID = _Operation.PatientID;
                    _OperationCurr.DoctorID = _Operation.DoctorID;
                    _OperationCurr.EndTime = _Operation.EndTime;
                    _OperationCurr.StartTime = _Operation.StartTime;
                    _OperationCurr.SurgeonID = _Operation.SurgeonID;
                    _OperationCurr.Remarks = _Operation.Remarks;
                    _OperationCurr.Premedication = _Operation.Premedication;
                    _OperationCurr.OperationName = _Operation.OperationName;
                    _OperationCurr.OperationID = _Operation.OperationID;
                }
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                    _sDescription = "Operation successfully saved.";
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription });
        }

        public ActionResult List() {
            UserSession.checkLogIn();
            return View();
        }

        public ActionResult ListView(OperationFilterViewModel _OperationFilterViewModel) {
            UserSession.checkLogIn();
            return View(getFilteredData(_OperationFilterViewModel));
        }

        public ActionResult Detail(Guid _sGuid) {
            UserSession.checkLogIn();
            return View(g_UnitOfWork.Operations.GetById(_sGuid));
        }

        private IEnumerable<Operation> getFilteredData(OperationFilterViewModel _OperationFilterViewModel) {
            return g_UnitOfWork.Operations.GetFiltered(_Operation =>
                (string.IsNullOrEmpty(_OperationFilterViewModel.SearchText) || _Operation.Patient.Name.Contains(_OperationFilterViewModel.SearchText)
                || _Operation.Patient.PatientNo.ToString().Contains(_OperationFilterViewModel.SearchText) || _Operation.Patient.Surname.Contains(_OperationFilterViewModel.SearchText)) &&
                (_Operation.Date >= _OperationFilterViewModel.DateFrom) &&
                (_Operation.Date <= _OperationFilterViewModel.DateTo) &&
                (string.IsNullOrEmpty(_OperationFilterViewModel.PatientID) || _Operation.PatientID.ToString() == _OperationFilterViewModel.PatientID) &&
                (string.IsNullOrEmpty(_OperationFilterViewModel.SurgeonID) || _Operation.SurgeonID.ToString() == _OperationFilterViewModel.SurgeonID)
            );
        }

        private DataSet getExportDataSet(OperationFilterViewModel _OperationFilterViewModel) {
            UserSession.checkLogIn();
            IEnumerable<Operation> _Operations = getFilteredData(_OperationFilterViewModel);
            DataSet _Ds = new DataSet("Data");
            DataTable _Dt = new DataTable("DataTable");
            _Dt.Columns.Add("Patient Name", typeof(string));
            _Dt.Columns.Add("Patient No.", typeof(string));
            _Dt.Columns.Add("Medical Aid Company", typeof(string));
            _Dt.Columns.Add("Date", typeof(string));
            _Dt.Columns.Add("Operation Name", typeof(string));
            _Dt.Columns.Add("Duration", typeof(string));
            foreach (Operation _Operation in _Operations) {
                _Dt.Rows.Add(
                      string.Concat(_Operation.Patient.Surname, " ", _Operation.Patient.Name),
                    _Operation.Patient.PatientNo,
                    (_Operation.Patient.MedicalAidCompany != null && _Operation.Patient.MedicalAidCompany.MedicalAidCompanyID != Guid.Empty ? _Operation.Patient.MedicalAidCompany.Name : "N/A"),
                    Conv.toDate(_Operation.Date.ToString(), false, true),
                   _Operation.OperationName,
                    Conv.toNum(_Operation.EndTime.Subtract(_Operation.StartTime).TotalMinutes.ToString(), _iDec: 0) + "minutes"
                    );
            }
            _Ds.Tables.Add(_Dt);
            return _Ds;
        }

        public ActionResult DownloadExcel(OperationFilterViewModel _OperationFilterViewModel) {
            try {
                string _sFilePath = ExcelExports.getExcelDocument(getExportDataSet(_OperationFilterViewModel), "Operationes");
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //ErrorLog.handleError("Class", "", "downloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }

        public ActionResult DownloadPdf(OperationFilterViewModel _OperationFilterViewModel) {
            try {
                UserSession.checkLogIn();
                float[] _fColumnDefinitionSize = new float[] { 20F, 10F, 20F, 10F, 15F, 10F };
                string _sFilePath = pdfExports.generatePdfReport(getExportDataSet(_OperationFilterViewModel), new string[] { }, _fColumnDefinitionSize, "Operations", "Operations", _bLandscape: true);
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //  ErrorLog.handleError("Class", "", "DownloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }

        public ActionResult DownloadCard(Guid _Guid) {
            try {
                UserSession.checkLogIn();
                string _sFilePath = pdfExports.generateOperationCardDownload(_Guid);
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //  ErrorLog.handleError("Class", "", "DownloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }
    }
}