﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using PatientRegister.Classes;
using PatientRegister.Models;
using PatientRegister.Repositories;
using PatientRegister.ViewModels;

namespace PatientRegister.Controllers {
    public class FileController : Controller {
        UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());

        [HttpPost]
        public ActionResult Create(string id, string type_id) {
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                foreach (string _sFile in Request.Files) {
                    var _sFileName = Path.GetFileName(Request.Files[_sFile].FileName);
                    string _sFileOriginalName = _sFileName;
                    var _vFileExt = Path.GetExtension(Request.Files[_sFile].FileName);
                    string _sName = Path.GetRandomFileName();
                    _sName = _sName.Replace(".", "");
                    var _vFileSavePath = System.Web.HttpContext.Current.Server.MapPath("~/" + Constant.UPLOADS_FOLDER + "/" + _sName + _vFileExt);
                    if (System.IO.File.Exists(_vFileSavePath)) {
                        _sName = Path.GetRandomFileName();
                        _sName = _sName.Replace(".", "");
                        _vFileSavePath = System.Web.HttpContext.Current.Server.MapPath("~/" + Constant.UPLOADS_FOLDER + "/" + _sName + _vFileExt);
                        Request.Files[_sFile].SaveAs(_vFileSavePath);
                    } else {
                        Request.Files[_sFile].SaveAs(_vFileSavePath);
                    }
                    FileUpload _File = new FileUpload {
                        FileUploadID = Guid.NewGuid(),
                        LinkID = Guid.Parse(id),
                        Deleted = (int)EnumYesNo.no,
                        ServerName = _sName + _vFileExt,
                        DateUploaded = DateTime.Now,
                        GivenName = _sFileOriginalName,
                        FileTypeID = int.Parse(type_id),
                        StatusID = (int)EnumStatus.active,
                        UserId = Guid.Parse(UserSession.getCurrentUserKey())
                    };
                    g_UnitOfWork.FileUploads.Insert(_File);
                }
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                    _sDescription = "You're set! File(s) uploaded successfully.";
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse {
                ResponseStatus = _ResponseStatus,
                Description = _sDescription
            });
        }

        [HttpPost]
        public ActionResult Delete(Guid _Guid) {
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                FileUpload _FileUpload = g_UnitOfWork.FileUploads.GetById(_Guid);
                if (_FileUpload == null || _FileUpload.FileUploadID == Guid.Empty) {
                    return Json(new RequestResponse {
                        ResponseStatus = _ResponseStatus,
                        Description = "Error. The file was not found."
                    });
                }
                _FileUpload.Deleted = (int)EnumYesNo.yes;
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                    _sDescription = "File deleted successfully.";
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse {
                ResponseStatus = _ResponseStatus,
                Description = _sDescription
            });
        }

        public ActionResult ListView(Guid LinkGuid, int EntityTypeID) {
            FileFilterViewModel _FileFilterViewModel = new FileFilterViewModel {
                EntityTypeID = EntityTypeID,
                LinkGuid = LinkGuid
            };
            UserSession.checkLogIn();
            IEnumerable<FileUpload> _FileUploads = g_UnitOfWork.FileUploads.GetFiltered(_Rec =>
                _Rec.LinkID == _FileFilterViewModel.LinkGuid &&
                _Rec.FileTypeID == _FileFilterViewModel.EntityTypeID &&
                _Rec.Deleted == (int)EnumYesNo.no
            );
            return View(_FileUploads);
        }
    }
}