﻿using PatientRegister.Classes;
using PatientRegister.Models;
using PatientRegister.Repositories;
using PatientRegister.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Web.Mvc;

namespace PatientRegister.Controllers {
    public class PrescriptionController : Controller {
        UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());

        public ActionResult Create() {
            UserSession.checkLogIn();
            return View("Edit", new Prescription());
        }

        public ActionResult Edit(Guid _sGuid) {
            UserSession.checkLogIn();
            Prescription _Prescription = g_UnitOfWork.Prescriptions.GetById(_sGuid);
            return View(_Prescription);
        }

        [HttpPost]
        public JsonResult Save(Prescription _Prescription) {
            UserSession.checkLogIn();
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                Prescription _PrescriptionCurr = g_UnitOfWork.Prescriptions.GetById(_Prescription.PrescriptionID);
                if (_PrescriptionCurr == null) {
                    g_UnitOfWork.Prescriptions.Insert(_Prescription);
                } else {
                    _PrescriptionCurr.Date = _Prescription.Date;
                    _PrescriptionCurr.DoctorID = _Prescription.DoctorID;
                    _PrescriptionCurr.PatientID = _Prescription.PatientID;
                    _PrescriptionCurr.ConsultationID = _Prescription.ConsultationID;
                }
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                    _sDescription = "Prescription successfully saved.";
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription });
        }

        [HttpPost]
        public JsonResult SaveMedication(PrescriptionMedication _PrescriptionMedication) {
            UserSession.checkLogIn();
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                PrescriptionMedication _ConsultationMedicationCurr = g_UnitOfWork.PrescriptionMedications.GetById(_PrescriptionMedication.PrescriptionMedicationID);
                if (_ConsultationMedicationCurr != null && _ConsultationMedicationCurr.PrescriptionMedicationID != Guid.Empty) { //Already exists
                    _ConsultationMedicationCurr.Deleted = _PrescriptionMedication.Deleted;
                    _sDescription = "Medical item successfully " + (_PrescriptionMedication.Deleted == (int)EnumYesNo.yes ? " removed" : "added back") + ".";
                } else {
                    _PrescriptionMedication.PrescriptionMedicationID = Guid.NewGuid();
                    _PrescriptionMedication.Deleted = (int)EnumYesNo.no; //Always save as not deleted when creating
                    g_UnitOfWork.PrescriptionMedications.Insert(_PrescriptionMedication);
                    _sDescription = "Medical item successfully added.";
                }
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                } else {
                    _sDescription = "Data could not be saved. Please retry.";
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription });
        }

        public ActionResult MedicationList(Guid _PrescriptionGuid) {
            return View(g_UnitOfWork.Prescriptions.GetById(_PrescriptionGuid).getMedications());
        }

        public ActionResult List() {
            UserSession.checkLogIn();
            return View();
        }

        public ActionResult ListView(PrescriptionFilterViewModel _PrescriptionFilterViewModel) {
            UserSession.checkLogIn();
            return View(getFilteredData(_PrescriptionFilterViewModel));
        }

        public ActionResult Detail(Guid _sGuid) {
            UserSession.checkLogIn();
            return View(g_UnitOfWork.Prescriptions.GetById(_sGuid));
        }

        private IEnumerable<Prescription> getFilteredData(PrescriptionFilterViewModel _PrescriptionFilterViewModel) {
            return g_UnitOfWork.Prescriptions.GetFiltered(_Prescription =>
                (string.IsNullOrEmpty(_PrescriptionFilterViewModel.SearchText) || _Prescription.Patient.Name.Contains(_PrescriptionFilterViewModel.SearchText)
                || _Prescription.Patient.PatientNo.ToString().Contains(_PrescriptionFilterViewModel.SearchText) || _Prescription.Patient.Surname.Contains(_PrescriptionFilterViewModel.SearchText)) &&
                (_Prescription.Date >= _PrescriptionFilterViewModel.DateFrom) &&
                (_Prescription.Date <= _PrescriptionFilterViewModel.DateTo) &&
                (string.IsNullOrEmpty(_PrescriptionFilterViewModel.PatientID) || _Prescription.Patient.MedicalAidCompanyID.ToString() == _PrescriptionFilterViewModel.PatientID) &&
                (string.IsNullOrEmpty(_PrescriptionFilterViewModel.DoctorID) || _Prescription.DoctorID.ToString() == _PrescriptionFilterViewModel.DoctorID)
            );
        }

        private DataSet getExportDataSet(PrescriptionFilterViewModel _PrescriptionFilterViewModel) {
            UserSession.checkLogIn();
            IEnumerable<Prescription> _Prescriptions = getFilteredData(_PrescriptionFilterViewModel);
            DataSet _Ds = new DataSet("Data");
            DataTable _Dt = new DataTable("DataTable");
            _Dt.Columns.Add("Patient Name", typeof(string));
            _Dt.Columns.Add("Patient No", typeof(string));
            _Dt.Columns.Add("Medical Aid Company", typeof(string));
            _Dt.Columns.Add("Date", typeof(string));
            _Dt.Columns.Add("Referred To Doctor", typeof(string));
            foreach (Prescription _Prescription in _Prescriptions) {
                _Dt.Rows.Add(
                      string.Concat(_Prescription.Patient.Surname, " ", _Prescription.Patient.Name),
                    _Prescription.Patient.PatientNo,
                    (_Prescription.Patient.MedicalAidCompany != null && _Prescription.Patient.MedicalAidCompany.MedicalAidCompanyID != Guid.Empty ? _Prescription.Patient.MedicalAidCompany.Name : "N/A"),
                    Conv.toDate(_Prescription.Date.ToString(), false, true),
                    string.Concat(_Prescription.Doctor.Surname, " ", _Prescription.Doctor.Name)
                    );
            }
            _Ds.Tables.Add(_Dt);
            return _Ds;
        }

        public ActionResult DownloadExcel(PrescriptionFilterViewModel _PrescriptionFilterViewModel) {
            try {
                string _sFilePath = ExcelExports.getExcelDocument(getExportDataSet(_PrescriptionFilterViewModel), "Prescriptions");
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //ErrorLog.handleError("Class", "", "downloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }

        public ActionResult DownloadPdf(PrescriptionFilterViewModel _PrescriptionFilterViewModel) {
            try {
                UserSession.checkLogIn();
                float[] _fColumnDefinitionSize = new float[] { 20F, 10F, 20F, 10F, 15F };
                string _sFilePath = pdfExports.generatePdfReport(getExportDataSet(_PrescriptionFilterViewModel), new string[] { }, _fColumnDefinitionSize, "Prescriptions", "Prescriptions", _bLandscape: true);
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //  ErrorLog.handleError("Class", "", "DownloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }

        public ActionResult Download(Guid _Guid) {
            try {
                UserSession.checkLogIn();
                string _sFilePath = pdfExports.generatePrescriptionDownload( _Guid);
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //  ErrorLog.handleError("Class", "", "DownloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }
    }
}