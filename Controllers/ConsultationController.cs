﻿using PatientRegister.Classes;
using PatientRegister.Models;
using PatientRegister.Repositories;
using PatientRegister.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace PatientRegister.Controllers {
    public class ConsultationController : Controller {
        UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());

        public ActionResult Create() {
            UserSession.checkLogIn();
            return View("Edit", new Consultation());
        }

        public ActionResult Edit(Guid _sGuid) {
            UserSession.checkLogIn();
            Consultation _Consultation = g_UnitOfWork.Consultations.GetById(_sGuid);
            return View(_Consultation);
        }

        [HttpPost]
        public JsonResult Save(Consultation _Consultation) {
            UserSession.checkLogIn();
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                Consultation _ConsultationCurr = g_UnitOfWork.Consultations.GetById(_Consultation.ConsultationID);
                if (_ConsultationCurr == null) {
                    g_UnitOfWork.Consultations.Insert(_Consultation);
                } else {
                    _ConsultationCurr.Date = _Consultation.Date;
                    _ConsultationCurr.BloodPressure = _Consultation.BloodPressure;
                    _ConsultationCurr.DetailedComment = _Consultation.DetailedComment;
                    _ConsultationCurr.DoctorID = _Consultation.DoctorID;
                    _ConsultationCurr.NextVisitDate = _Consultation.NextVisitDate;
                    _ConsultationCurr.PatientID = _Consultation.PatientID;
                    _ConsultationCurr.SummaryComment = _Consultation.SummaryComment;
                    _ConsultationCurr.Temperature = _Consultation.Temperature;
                    _ConsultationCurr.Weight = _Consultation.Weight;
                }
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                    _sDescription = "Consultation successfully saved.";
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription });
        }

        [HttpPost]
        public JsonResult SaveSymptom(ConsultationSymptom _ConsultationSymptom) {
            UserSession.checkLogIn();
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                ConsultationSymptom _ConsultationSymptomCurr = g_UnitOfWork.ConsultationSymptoms.GetById(_ConsultationSymptom.ConsultationSymptomID);
                if (_ConsultationSymptomCurr != null && _ConsultationSymptomCurr.ConsultationSymptomID != Guid.Empty) { //Already exists
                    _ConsultationSymptomCurr.Deleted = _ConsultationSymptom.Deleted;
                    _sDescription = "Consultation Diagnosis successfully " + (_ConsultationSymptom.Deleted == (int)EnumYesNo.yes ? " removed" : "added back") + ".";
                } else {
                    _ConsultationSymptom.ConsultationSymptomID = Guid.NewGuid();
                    _ConsultationSymptom.Deleted = (int)EnumYesNo.no; //Always save as not deleted when creating
                    g_UnitOfWork.ConsultationSymptoms.Insert(_ConsultationSymptom);
                    _sDescription = "Consultation Symptom successfully saved.";
                }
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                } else {
                    _sDescription = "Data could not be saved. Please retry.";
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription });
        }

        [HttpPost]
        public JsonResult SaveDiagnosis(ConsultationDiagnosis _ConsultationDiagnosis) {
            UserSession.checkLogIn();
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                ConsultationDiagnosis _ConsultationDiagnosisCurr = g_UnitOfWork.ConsultationDiagnoses.GetById(_ConsultationDiagnosis.ConsultationDiagnosisID);
                if (_ConsultationDiagnosisCurr != null && _ConsultationDiagnosisCurr.ConsultationDiagnosisID != Guid.Empty) { //Already exists
                    _ConsultationDiagnosisCurr.Deleted = _ConsultationDiagnosis.Deleted;
                    _sDescription = "Consultation Diagnosis successfully " + (_ConsultationDiagnosis.Deleted == (int)EnumYesNo.yes ? " removed" : "added back") + ".";
                } else {
                    _ConsultationDiagnosis.ConsultationDiagnosisID = Guid.NewGuid();
                    _ConsultationDiagnosis.Deleted = (int)EnumYesNo.no; //Always save as not deleted when creating
                    g_UnitOfWork.ConsultationDiagnoses.Insert(_ConsultationDiagnosis);
                    _sDescription = "Consultation Diagnosis successfully added.";
                }
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                } else {
                    _sDescription = "Data could not be saved. Please retry.";
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription });
        }

        [HttpPost]
        public JsonResult SaveMedication(ConsultationMedication _ConsultationMedication) {
            UserSession.checkLogIn();
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                ConsultationMedication _ConsultationMedicationCurr = g_UnitOfWork.ConsultationMedications.GetById(_ConsultationMedication.ConsultationMedicationID);
                if (_ConsultationMedicationCurr != null && _ConsultationMedicationCurr.ConsultationMedicationID != Guid.Empty) { //Already exists
                    _ConsultationMedicationCurr.Deleted = _ConsultationMedication.Deleted;
                    _sDescription = "Medical item successfully " + (_ConsultationMedication.Deleted == (int)EnumYesNo.yes ? " removed" : "added back") + ".";
                } else {
                    _ConsultationMedication.ConsultationMedicationID = Guid.NewGuid();
                    _ConsultationMedication.Deleted = (int)EnumYesNo.no; //Always save as not deleted when creating
                    g_UnitOfWork.ConsultationMedications.Insert(_ConsultationMedication);
                    _sDescription = "Medical item successfully added.";
                }
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                } else {
                    _sDescription = "Data could not be saved. Please retry.";
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription });
        }

        public ActionResult SymptomList(Guid _ConsultationGuid) {
            return View(g_UnitOfWork.Consultations.GetById(_ConsultationGuid).getSymptoms());
        }

        public ActionResult MedicationList(Guid _ConsultationGuid) {
            return View(g_UnitOfWork.Consultations.GetById(_ConsultationGuid).getMedications());
        }

        public ActionResult DiagnosisList(Guid _ConsultationGuid) {
            return View(g_UnitOfWork.Consultations.GetById(_ConsultationGuid).getDiagnses());
        }

        public ActionResult List() {
            UserSession.checkLogIn();
            return View();
        }

        public ActionResult ListView(ConsultationFilterViewModel _ConsultationFilterViewModel) {
            UserSession.checkLogIn();
            return View(getFilteredData(_ConsultationFilterViewModel));
        }

        public ActionResult Detail(Guid _sGuid) {
            UserSession.checkLogIn();
            return View(g_UnitOfWork.Consultations.GetById(_sGuid));
        }

        private IEnumerable<Consultation> getFilteredData(ConsultationFilterViewModel _ConsultationFilterViewModel) {
            return g_UnitOfWork.Consultations.GetFiltered(_Consultation =>
                (string.IsNullOrEmpty(_ConsultationFilterViewModel.SearchText) || _Consultation.Patient.Name.Contains(_ConsultationFilterViewModel.SearchText)
                || _Consultation.Patient.PatientNo.ToString().Contains(_ConsultationFilterViewModel.SearchText) || _Consultation.Patient.Surname.Contains(_ConsultationFilterViewModel.SearchText)) &&
                (_Consultation.Date >= _ConsultationFilterViewModel.DateFrom) &&
                (_Consultation.Date <= _ConsultationFilterViewModel.DateTo) &&
                (string.IsNullOrEmpty(_ConsultationFilterViewModel.MedicalAidCompanyID) || _Consultation.Patient.MedicalAidCompanyID.ToString() == _ConsultationFilterViewModel.MedicalAidCompanyID) &&
                (string.IsNullOrEmpty(_ConsultationFilterViewModel.PatientID) || _Consultation.PatientID.ToString() == _ConsultationFilterViewModel.PatientID) &&
                (string.IsNullOrEmpty(_ConsultationFilterViewModel.DoctorID) || _Consultation.DoctorID.ToString() == _ConsultationFilterViewModel.DoctorID)
            );
        }

        private DataSet getExportDataSet(ConsultationFilterViewModel _ConsultationFilterViewModel) {
            UserSession.checkLogIn();
            IEnumerable<Consultation> _Consultations = getFilteredData(_ConsultationFilterViewModel);
            DataSet _Ds = new DataSet("Data");
            DataTable _Dt = new DataTable("DataTable");
            _Dt.Columns.Add("Patient Name", typeof(string));
            _Dt.Columns.Add("Patient No.", typeof(string));
            _Dt.Columns.Add("Medical Aid Company", typeof(string));
            _Dt.Columns.Add("Date", typeof(string));
            _Dt.Columns.Add("Doctor", typeof(string));
            _Dt.Columns.Add("Next Visit", typeof(string));
            _Dt.Columns.Add("Comment", typeof(string));
            foreach (Consultation _Consultation in _Consultations) {
                _Dt.Rows.Add(
                      string.Concat(_Consultation.Patient.Surname, " ", _Consultation.Patient.Name),
                    _Consultation.Patient.PatientNo,
                    (_Consultation.Patient.MedicalAidCompany != null && _Consultation.Patient.MedicalAidCompany.MedicalAidCompanyID != Guid.Empty ? _Consultation.Patient.MedicalAidCompany.Name : "N/A"),
                    Conv.toDate(_Consultation.Date.ToString(), false, true),
                   string.Concat(_Consultation.Doctor.Surname, " ", _Consultation.Doctor.Name),
                    _Consultation.NextVisitDate == null ? "" : Conv.toDate(_Consultation.NextVisitDate.ToString(), false, true),
                    _Consultation.SummaryComment
                    );
            }
            _Ds.Tables.Add(_Dt);
            return _Ds;
        }

        public ActionResult DownloadExcel(ConsultationFilterViewModel _ConsultationFilterViewModel) {
            try {
                string _sFilePath = ExcelExports.getExcelDocument(getExportDataSet(_ConsultationFilterViewModel), "Consultations");
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //ErrorLog.handleError("Class", "", "downloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }

        public ActionResult DownloadPdf(ConsultationFilterViewModel _ConsultationFilterViewModel) {
            try {
                UserSession.checkLogIn();
                float[] _fColumnDefinitionSize = new float[] { 20F, 10F, 20F, 10F, 20F, 10F, 15F };
                string _sFilePath = pdfExports.generatePdfReport(getExportDataSet(_ConsultationFilterViewModel), new string[] { }, _fColumnDefinitionSize, "Consultations", "Consultations", _bLandscape: true);
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //  ErrorLog.handleError("Class", "", "DownloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }
    }
}