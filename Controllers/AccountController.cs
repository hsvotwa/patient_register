﻿using PatientRegister.Classes;
using PatientRegister.Models;
using PatientRegister.Repositories;
using PatientRegister.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PatientRegister.Controllers {
    public class AccountController : Controller {
        UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());

        public ActionResult Login() {
            return View();
        }

        public ActionResult ForgotPassword() {
            return View();
        }

        [HttpPost]
        public ActionResult ForgotPassword(LoginViewModel _Login) {
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "";
            string _sJsFunc = string.Empty;
            try {
                IEnumerable<UserClass> _MatchedUsers = g_UnitOfWork.UserClasses.GetFiltered(_User => _User.Email == _Login.Email);
                if (_MatchedUsers.Count() == 0) {
                    return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = "Ooops! We couldn't find an account with that username.", JsFunc = _sJsFunc });
                }
                UserClass _UserClassCurr = g_UnitOfWork.UserClasses.GetById(_MatchedUsers.FirstOrDefault().UserID); 
                if (_UserClassCurr == null) {
                    return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = "Something went seriously wrong. Please make sure you're logged in." });
                }
                _UserClassCurr.Password = UserClass.encryptPwd(Constant.DEFAULT_USER_PWD_RESET);
                if (!g_UnitOfWork.Commit()) {
                    _sDescription = "Something went wrong. Please retry."; //Send an email if new user
                    return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription, JsFunc = _sJsFunc });
                }
                _ResponseStatus = EnumResponseStatus.success;
                _sDescription = "Password successful reset. An email has been sent to: " + _Login.Email;
                _sJsFunc = "window.location='/Account/Login/';";
                string _sMail = "Hi <br><br>";
                _sMail += "Your password on " + Constant.APP_NAME + ", has been reset to your company's default.";
                _sMail += "<br><br>";
                _sMail += "Log in and make sure you change it.";
                _sMail += "<br><br>";
                Mailer.send(_Login.Email, "Password successfully reset", _sMail);
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription, JsFunc = _sJsFunc });
        }

        public ActionResult ChangePassword() {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordViewModel _ChangePasswordViewModel) {
            UserSession.checkLogIn();
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                if (UserClass.encryptPwd(_ChangePasswordViewModel.NewPassword) != UserClass.encryptPwd(_ChangePasswordViewModel.ConfirmNewPassword)) {
                    return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = "The two new password fields should have similar values." });
                }
                if (!UserClass.authenticateUser(UserSession.getUserEmail(), UserClass.encryptPwd(_ChangePasswordViewModel.CurrentPassword), _bLogIn: false)) {
                    return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = "Invalid current password." });
                }
                UserClass _UserClassCurr = g_UnitOfWork.UserClasses.GetById(Guid.Parse(UserSession.getCurrentUserKey()));
                if (_UserClassCurr == null) {
                    return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = "Something went seriously wrong. Please make sure you're logged in." });
                }
                _UserClassCurr.Password = UserClass.encryptPwd(_ChangePasswordViewModel.NewPassword);
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                    _sDescription = "Password successfully changed."; //Send an email if new user
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription });
        }

        [HttpPost]
        public ActionResult ResetPassword(UserClass _UserClass) {
            UserSession.checkLogIn();
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                UserClass _UserClassCurr = g_UnitOfWork.UserClasses.GetById(_UserClass.UserID);
                if (_UserClassCurr == null) {
                    return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = "Something went seriously wrong. Please make sure you're logged in." });
                }
                _UserClassCurr.Password = UserClass.encryptPwd(Constant.DEFAULT_USER_PWD_RESET);
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                    _sDescription = "Password successfully reset."; //Send an email if new user
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription });
        }

        public void Logout() {
            UserSession.killUserSession();
            UserSession.redirect("/Account/Login");
            return;
        }
    }
}