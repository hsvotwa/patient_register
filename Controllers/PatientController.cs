﻿using PatientRegister.Classes;
using PatientRegister.Models;
using PatientRegister.Repositories;
using PatientRegister.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Web.Mvc;

namespace PatientRegister.Controllers {
    public class PatientController : Controller {
        UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());

        public ActionResult Create() {
            UserSession.checkLogIn();
            return View("Edit", new Patient());
        }

        public ActionResult Edit(Guid _sGuid) {
            UserSession.checkLogIn();
            Patient _Patient = g_UnitOfWork.Patients.GetById(_sGuid);
            return View(_Patient);
        }

        [HttpPost]
        public JsonResult Save(Patient _Patient) {
            UserSession.checkLogIn();
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                Patient _PatientCurr = g_UnitOfWork.Patients.GetById(_Patient.PatientID);
                if (_PatientCurr == null) {
                    _Patient.PatientNo = _Patient.getNextPatientNumber();
                    _Patient.DateOfBirth = DateTime.Parse(Conv.toDate(_Patient.DateOfBirth.ToString(), true, true));
                    g_UnitOfWork.Patients.Insert(_Patient);
                } else {
                    if (_PatientCurr.PatientNo == 0) {
                        _PatientCurr.PatientNo = _PatientCurr.getNextPatientNumber();
                    }
                    _PatientCurr.Name = _Patient.Name;
                    _PatientCurr.Surname = _Patient.Surname;
                    _PatientCurr.DateOfBirth = _Patient.DateOfBirth;
                    _PatientCurr.IDNumber = _Patient.IDNumber;
                    _PatientCurr.PostalAddress = _Patient.PostalAddress;
                    _PatientCurr.ResidentialAddress = _Patient.ResidentialAddress;
                    _PatientCurr.HomeTel = _Patient.HomeTel;
                    _PatientCurr.WorkTel = _Patient.WorkTel;
                    _PatientCurr.CellNo = _Patient.CellNo;
                    _PatientCurr.Email = _Patient.Email;
                    _PatientCurr.Occupation = _Patient.Occupation;
                    _PatientCurr.Employer = _Patient.Employer;
                    _PatientCurr.MedicalAidName = _Patient.MedicalAidName;
                    _PatientCurr.MedicalAidMainMemberName = _Patient.MedicalAidMainMemberName;
                    _PatientCurr.MedicalAidMainMemberIDNo = _Patient.MedicalAidMainMemberIDNo;
                    _PatientCurr.MedicalAidNo = _Patient.MedicalAidNo;
                    _PatientCurr.MedicalAidMainMemberOccupation = _Patient.MedicalAidMainMemberOccupation;
                    _PatientCurr.MedicalAidMainMemberPhone = _Patient.MedicalAidMainMemberPhone;
                    _PatientCurr.MedicalAidMainMemberRelationshipID = _Patient.MedicalAidMainMemberRelationshipID;
                    _PatientCurr.EmergencyContactName = _Patient.EmergencyContactName;
                    _PatientCurr.EmergencyContactPhone = _Patient.EmergencyContactPhone;
                    _PatientCurr.EmergencyContactRelationshipID = _Patient.EmergencyContactRelationshipID;
                    _PatientCurr.CommunicationOptionID = _Patient.CommunicationOptionID;
                    _PatientCurr.StatusID = _Patient.StatusID;
                    _PatientCurr.GenderID = _Patient.GenderID;
                    _PatientCurr.MedicalAidCompanyID = _Patient.MedicalAidCompanyID;
                }
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                    _sDescription = "Patient successfully saved."; //Send an email if new user
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription });
        }

        public ActionResult List() {
            UserSession.checkLogIn();
            return View();
        }

        public ActionResult ListView(PatientFilterViewModel _PatientFilterViewModel) {
            UserSession.checkLogIn();
            return View(getFilteredData(_PatientFilterViewModel));
        }

        public ActionResult Detail(Guid _sGuid) {
            UserSession.checkLogIn();
            return View(g_UnitOfWork.Patients.GetById(_sGuid));
        }

        private IEnumerable<Patient> getFilteredData(PatientFilterViewModel _PatientFilterViewModel) {
            return g_UnitOfWork.Patients.GetFiltered(_Patient =>
               (string.IsNullOrEmpty(_PatientFilterViewModel.SearchText) || _Patient.Name.Contains(_PatientFilterViewModel.SearchText)
               || _Patient.Email.Contains(_PatientFilterViewModel.SearchText) || _Patient.Surname.Contains(_PatientFilterViewModel.SearchText)
               || _Patient.PatientNo.ToString().Contains(_PatientFilterViewModel.SearchText)) &&
               (_PatientFilterViewModel.StatusID == (int)EnumStatus.none || _Patient.StatusID == _PatientFilterViewModel.StatusID) &&
               (_PatientFilterViewModel.GenderID == (int)EnumStaffType.none || _Patient.GenderID == _PatientFilterViewModel.GenderID) &&
               (string.IsNullOrEmpty(_PatientFilterViewModel.MedicalAidCompanyID) || _Patient.MedicalAidCompanyID.ToString() == _PatientFilterViewModel.MedicalAidCompanyID)
            );
        }

        private DataSet getExportDataSet(PatientFilterViewModel _PatientFilterViewModel) {
            UserSession.checkLogIn();
            IEnumerable<Patient> _Patients = getFilteredData(_PatientFilterViewModel);
            DataSet _Ds = new DataSet("Data");
            DataTable _Dt = new DataTable("DataTable");
            _Dt.Columns.Add("Name", typeof(string));
            _Dt.Columns.Add("Patient No", typeof(string));
            _Dt.Columns.Add("Gender", typeof(string));
            _Dt.Columns.Add("Age", typeof(string));
            _Dt.Columns.Add("Medical Aid Company", typeof(string));
            _Dt.Columns.Add("Cell No", typeof(string));
            _Dt.Columns.Add("Emergency Contact", typeof(string));
            _Dt.Columns.Add("Status", typeof(string));
            foreach (Patient _Patient in _Patients) {
                _Dt.Rows.Add(
                    string.Concat(_Patient.Surname, " ", _Patient.Name),
                   _Patient.PatientNo,
                   _Patient.Gender.Name,
                   (int)(DateTime.Today.Subtract(_Patient.DateOfBirth).TotalDays / 365),
                    (_Patient.MedicalAidCompany != null && _Patient.MedicalAidCompany.MedicalAidCompanyID != Guid.Empty ? _Patient.MedicalAidCompany.Name : "N/A"),
                    _Patient.CellNo,
                    _Patient.EmergencyContactPhone,
                   _Patient.Status.Name
                    );
            }
            _Ds.Tables.Add(_Dt);
            return _Ds;
        }

        [HttpPost]
        public ActionResult DownloadExcel(PatientFilterViewModel _PatientFilterViewModel) {
            try {
                string _sFilePath = ExcelExports.getExcelDocument(getExportDataSet(_PatientFilterViewModel), "Patients");
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //ErrorLog.handleError("Class", "", "downloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }

        [HttpPost]
        public ActionResult DownloadPdf(PatientFilterViewModel _PatientFilterViewModel) {
            try {
                UserSession.checkLogIn();
                float[] _fColumnDefinitionSize = new float[] { 20F, 10F, 10F, 7f, 20F, 10F, 15F, 10F };
                string _sFilePath = pdfExports.generatePdfReport(getExportDataSet(_PatientFilterViewModel), new string[] { }, _fColumnDefinitionSize, "Patients", "Patients", _bLandscape: true);
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //  ErrorLog.handleError("Class", "", "DownloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }
    }
}