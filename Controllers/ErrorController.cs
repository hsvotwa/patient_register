﻿using System.Web.Mvc;

namespace PatientRegister.Controllers {
    public class ErrorController : Controller {
        public ActionResult NotFound() {
            return View();
        }

        public ActionResult InternalError() {
            return View();
        }

        public ActionResult NoAccess() {
            return View();
        }
    }
}