﻿using PatientRegister.Classes;
using PatientRegister.Models;
using PatientRegister.Repositories;
using PatientRegister.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace PatientRegister.Controllers {
    public class SymptomController : Controller {
        UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());

        public ActionResult Create() {
            UserSession.checkLogIn();
            return View("Edit", new Symptom());
        }

        public ActionResult Edit(Guid _sGuid) {
            UserSession.checkLogIn();
            Symptom _Symptom = g_UnitOfWork.Symptoms.GetById(_sGuid);
            return View(_Symptom);
        }

        [HttpPost]
        public JsonResult Save(Symptom _Symptom) {
            UserSession.checkLogIn();
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                Symptom _SymptomCurr = g_UnitOfWork.Symptoms.GetById(_Symptom.SymptomID);
                if (_SymptomCurr == null) {
                    g_UnitOfWork.Symptoms.Insert(_Symptom);
                } else {
                    _SymptomCurr.Name = _Symptom.Name;
                    _SymptomCurr.StatusID = _Symptom.StatusID;
                }
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                    _sDescription = "Symptom successfully saved.";
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription });
        }

        public ActionResult List() {
            UserSession.checkLogIn();
            return View();
        }

        public ActionResult ListView(SymptomFilterViewModel _SymptomFilterViewModel) {
            UserSession.checkLogIn();
            IEnumerable<Symptom> _Symptoms = g_UnitOfWork.Symptoms.GetFiltered(_Symptom =>
                (string.IsNullOrEmpty(_SymptomFilterViewModel.SearchText) || _Symptom.Name.Contains(_SymptomFilterViewModel.SearchText)) &&
                (_SymptomFilterViewModel.StatusID == (int)EnumStatus.none || _Symptom.StatusID == _SymptomFilterViewModel.StatusID)
            );
            return View(_Symptoms);
        }

        public ActionResult Detail(Guid _sGuid) {
            UserSession.checkLogIn();
            Symptom _Symptom = g_UnitOfWork.Symptoms.GetById(_sGuid);
            return View(_Symptom);
        }

        public ActionResult DownloadExcel(BranchFilterViewModel _BranchFilterViewModel) {
            try {
                UserSession.checkLogIn();
                IEnumerable<Symptom> _Branches = g_UnitOfWork.Symptoms.GetFiltered(_Branch =>
                    (string.IsNullOrEmpty(_BranchFilterViewModel.SearchText) || _Branch.Name.Contains(_BranchFilterViewModel.SearchText)) &&
                    (_BranchFilterViewModel.StatusID == (int)EnumStatus.none || _Branch.StatusID == _BranchFilterViewModel.StatusID)
                );
                DataSet _Ds = new DataSet("Data");
                DataTable _Dt = new DataTable("DataTable");
                _Dt.Columns.Add("name", typeof(string));
                _Dt.Columns.Add("status", typeof(string));
                foreach (Symptom _Branch in _Branches) {
                    _Dt.Rows.Add(_Branch.Name, _Branch.Status.Name);
                }
                _Ds.Tables.Add(_Dt);
                string _sFilePath = ExcelExports.getExcelDocument(_Ds, "Symptoms");
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //ErrorLog.handleError("Class", "", "downloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }

        public ActionResult DownloadPdf(BranchFilterViewModel _BranchFilterViewModel) {
            try {
                UserSession.checkLogIn();
                IEnumerable<Symptom> _Symptoms = g_UnitOfWork.Symptoms.GetFiltered(_Branch =>
                    (string.IsNullOrEmpty(_BranchFilterViewModel.SearchText) || _Branch.Name.Contains(_BranchFilterViewModel.SearchText)) &&
                    (_BranchFilterViewModel.StatusID == (int)EnumStatus.none || _Branch.StatusID == _BranchFilterViewModel.StatusID)
                );
                DataSet _Ds = new DataSet("Data");
                DataTable _Dt = new DataTable("DataTable");
                _Dt.Columns.Add("name", typeof(string));
                _Dt.Columns.Add("status", typeof(string));
                foreach (Symptom _Branch in _Symptoms) {
                    _Dt.Rows.Add(_Branch.Name, _Branch.Status.Name);
                }
                _Ds.Tables.Add(_Dt);
                float[] _fColumnDefinitionSize = new float[] { 100F, 100F };
                string _sFilePath = pdfExports.generatePdfReport(_Ds, new string[] { }, _fColumnDefinitionSize, "Symptoms", "Symptoms");
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //  ErrorLog.handleError("Class", "", "DownloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }
    }
}