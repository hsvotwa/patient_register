﻿using PatientRegister.Classes;
using PatientRegister.Models;
using PatientRegister.Repositories;
using PatientRegister.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Web.Mvc;

namespace PatientRegister.Controllers {
    public class ReferralController : Controller {
        UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());

        public ActionResult Create() {
            UserSession.checkLogIn();
            return View("Edit", new Referral());
        }

        public ActionResult Edit(Guid _sGuid) {
            UserSession.checkLogIn();
            Referral _Referral = g_UnitOfWork.Referrals.GetById(_sGuid);
            return View(_Referral);
        }

        [HttpPost]
        public JsonResult Save(Referral _Referral) {
            UserSession.checkLogIn();
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                Referral _ReferralCurr = g_UnitOfWork.Referrals.GetById(_Referral.ReferralID);
                if (_ReferralCurr == null) {
                    g_UnitOfWork.Referrals.Insert(_Referral);
                } else {
                    _ReferralCurr.Date = _Referral.Date;
                    _ReferralCurr.ConsultationID = _Referral.ConsultationID;
                    _ReferralCurr.CurrentMedication = _Referral.CurrentMedication;
                    _ReferralCurr.DoctorID = _Referral.DoctorID;
                    _ReferralCurr.ExaminationAndInvestigation = _Referral.ExaminationAndInvestigation;
                    _ReferralCurr.History = _Referral.History;
                    _ReferralCurr.SummaryComment = _Referral.SummaryComment;
                    _ReferralCurr.PatientID = _Referral.PatientID;
                    _ReferralCurr.ReferredToDoctor = _Referral.ReferredToDoctor;
                }
                if (g_UnitOfWork.Commit()) {
                    _ResponseStatus = EnumResponseStatus.success;
                    _sDescription = "Referral successfully saved.";
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            return Json(new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription });
        }

        public ActionResult List() {
            UserSession.checkLogIn();
            return View();
        }

        public ActionResult Detail(Guid _sGuid) {
            UserSession.checkLogIn();
            return View(g_UnitOfWork.Referrals.GetById(_sGuid));
        }

        public ActionResult ListView(ReferralFilterViewModel _ReferralFilterViewModel) {
            UserSession.checkLogIn();
            return View(getFilteredData(_ReferralFilterViewModel));
        }

        private IEnumerable<Referral> getFilteredData(ReferralFilterViewModel _ReferralFilterViewModel) {
            return g_UnitOfWork.Referrals.GetFiltered(_Referral =>
              (string.IsNullOrEmpty(_ReferralFilterViewModel.SearchText) || _Referral.Patient.Name.Contains(_ReferralFilterViewModel.SearchText)
              || _Referral.Patient.PatientNo.ToString().Contains(_ReferralFilterViewModel.SearchText) || _Referral.Patient.Surname.Contains(_ReferralFilterViewModel.SearchText)) &&
              (_Referral.Date >= _ReferralFilterViewModel.DateFrom) &&
              (_Referral.Date <= _ReferralFilterViewModel.DateTo) &&
              (string.IsNullOrEmpty(_ReferralFilterViewModel.PatientID) || _Referral.PatientID.ToString() == _ReferralFilterViewModel.PatientID) &&
              (string.IsNullOrEmpty(_ReferralFilterViewModel.DoctorID) || _Referral.DoctorID.ToString() == _ReferralFilterViewModel.DoctorID)
          );
        }

        private DataSet getExportDataSet(ReferralFilterViewModel _ReferralFilterViewModel) {
            UserSession.checkLogIn();
            IEnumerable<Referral> _Referrals = getFilteredData(_ReferralFilterViewModel);
            DataSet _Ds = new DataSet("Data");
            DataTable _Dt = new DataTable("DataTable");
            _Dt.Columns.Add("Patient Name", typeof(string));
            _Dt.Columns.Add("Patient No.", typeof(string));
            _Dt.Columns.Add("Medical Aid Company", typeof(string));
            _Dt.Columns.Add("Date", typeof(string));
            _Dt.Columns.Add("Doctor", typeof(string));
            _Dt.Columns.Add("Referred to Doctor", typeof(string));
            foreach (Referral _Referral in _Referrals) {
                _Dt.Rows.Add(
                    string.Concat(_Referral.Patient.Surname, " ", _Referral.Patient.Name),
                    _Referral.Patient.PatientNo,
                    (_Referral.Patient.MedicalAidCompany != null && _Referral.Patient.MedicalAidCompany.MedicalAidCompanyID != Guid.Empty ? _Referral.Patient.MedicalAidCompany.Name : "N/A"),
                    Conv.toDate(_Referral.Date.ToString(), false, true),
                    string.Concat(_Referral.Doctor.Surname, " ", _Referral.Doctor.Name),
                    _Referral.ReferredToDoctor
                    );
            }
            _Ds.Tables.Add(_Dt);
            return _Ds;
        }

        public ActionResult DownloadExcel(ReferralFilterViewModel _ReferralFilterViewModel) {
            try {
                string _sFilePath = ExcelExports.getExcelDocument(getExportDataSet(_ReferralFilterViewModel), "Referrals");
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //ErrorLog.handleError("Class", "", "downloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }

        public ActionResult DownloadPdf(ReferralFilterViewModel _ReferralFilterViewModel) {
            try {
                UserSession.checkLogIn();
                float[] _fColumnDefinitionSize = new float[] { 20F, 10F, 20F, 10F, 15F, 15F };
                string _sFilePath = pdfExports.generatePdfReport(getExportDataSet(_ReferralFilterViewModel), new string[] { }, _fColumnDefinitionSize, "Referrals", "Referrals", _bLandscape: true);
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //  ErrorLog.handleError("Class", "", "DownloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }

        public ActionResult DownloadLetter(Guid _Guid) {
            try {
                UserSession.checkLogIn();
                string _sFilePath = pdfExports.generateReferralLetter(_Guid);
                FileModel _FileModel = Common.getFileDetail(_sFilePath);
                if (_FileModel == null) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
                }
                return Json(new { Errors = false, _FileModel.FilePath, _FileModel.FileName, Url = "" }, JsonRequestBehavior.AllowGet);
            } catch (Exception _Ex) {
                //  ErrorLog.handleError("Class", "", "DownloadPdf", _Ex.Message);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Failed to download file");
        }
    }
}