﻿using System;
using System.Web.Mvc;
using PatientRegister.Classes;
using PatientRegister.Models;
using PatientRegister.ViewModels;

namespace PatientRegister.Controllers {
    public class LoginController : Controller {
        [HttpPost]
        public ActionResult Try(LoginViewModel _Login) {
            EnumResponseStatus _ResponseStatus = EnumResponseStatus.failed;
            string _sDescription = "Invalid email and/or password. Please retry.";
            string _sJsFunc = string.Empty;
            try {
                if (UserClass.authenticateUser(_Login.Email, UserClass.encryptPwd(_Login.Password))) {
                    _ResponseStatus = EnumResponseStatus.success;
                    _sDescription = "Log in successful. Let us take you through.";
                    _sJsFunc = "window.location='" + (System.Web.HttpContext.Current.Session["nextpage"] ?? "/Dashboard/") + "';";
                    string _sMail = "Hi <br><br>";
                    _sMail += "You have successfully logged in to " + Constant.APP_NAME + ".";
                    _sMail += "<br><br>";
                    _sMail += "If this wasn't you, please contact your administrator immediately.";
                    _sMail += "<br><br>";
                    Mailer.send(_Login.Email, "Log in successful", _sMail);
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = EnumResponseStatus.error;
            }
            RequestResponse _RequestResponse = new RequestResponse { ResponseStatus = _ResponseStatus, Description = _sDescription, JsFunc = _sJsFunc };
            return Json(_RequestResponse);
        }
    }
}