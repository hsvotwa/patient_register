﻿namespace PatientRegister.ViewModels {
    public class LoginViewModel {
        #region Public Properties
        public string Email { get; set; }
        public string Password { get; set; }
        public int RememberMe { get; set; }
        #endregion
    }
}