﻿namespace PatientRegister.ViewModels {
    public class BranchFilterViewModel {
        public int StatusID { get; set; }
        public string SearchText { get; set; }
    }
}