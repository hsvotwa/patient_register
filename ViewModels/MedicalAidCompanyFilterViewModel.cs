﻿namespace PatientRegister.ViewModels {
    public class MedicalAidCompanyFilterViewModel {
        public int StatusID { get; set; }
        public string SearchText { get; set; }
    }
} 