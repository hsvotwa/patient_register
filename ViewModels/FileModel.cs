﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PatientRegister.ViewModels {
    public class FileModel {
        public string FilePath { get; set; }
        public string FileName { get; set; }
    }
}