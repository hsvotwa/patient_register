﻿using System;

namespace PatientRegister.ViewModels {
    public class OperationFilterViewModel {
        public string PatientID { get; set; }
        public string SurgeonID { get; set; }
        public string SearchText { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}