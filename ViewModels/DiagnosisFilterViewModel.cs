﻿namespace PatientRegister.ViewModels {
    public class DiagnosisFilterViewModel {
        public int StatusID { get; set; }
        public string SearchText { get; set; }
    }
}