﻿using System;

namespace PatientRegister.ViewModels {
    public class ReferralFilterViewModel {
        public string PatientID { get; set; }
        public string DoctorID { get; set; }
        public string SearchText { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}