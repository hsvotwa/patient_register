﻿namespace PatientRegister.ViewModels {
    public class MedicalStockItemFilterViewModel {
        public int StatusID { get; set; }
        public string SearchText { get; set; }
    }
}