﻿using System;

namespace PatientRegister.ViewModels {
    public class FileFilterViewModel {
        public int EntityTypeID { get; set; }
        public Guid LinkGuid { get; set; }
    }
}