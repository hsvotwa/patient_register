﻿namespace PatientRegister.ViewModels {
    public class UserClassFilterViewModel {
        public int StatusID { get; set; }
        public int StaffTypeID { get; set; }
        public string SearchText { get; set; }
    }
}