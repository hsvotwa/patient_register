﻿namespace PatientRegister.ViewModels {
    public class SymptomFilterViewModel {
        public int StatusID { get; set; }
        public string SearchText { get; set; }
    }
}