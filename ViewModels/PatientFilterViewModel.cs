﻿namespace PatientRegister.ViewModels {
    public class PatientFilterViewModel {
        public int StatusID { get; set; }
        public int GenderID { get; set; }
        public string MedicalAidCompanyID { get; set; }
        public string SearchText { get; set; }
    }
}