(function ($) {
    'use strict';
    $(function () {
        // Remove pro banner on close
        //document.querySelector('#bannerClose').addEventListener('click',function() {
        //  document.querySelector('#proBanner').classList.add('d-none');
        //});
        Chart.defaults.global.legend.labels.usePointStyle = true;
        if ($("#consultations-by-gender").length) {
            Chart.defaults.global.legend.labels.usePointStyle = true;
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: var_labels,
                    datasets: var_gender_data
                },
                options: {
                    responsive: true,
                    legend: false,
                    legendCallback: function (chart) {
                        var text = [];
                        text.push('<ul>');
                        for (var i = 0; i < chart.data.datasets.length; i++) {
                            text.push('<li><span class="legend-dots" style="background:' +
                                chart.data.datasets[i].legendColor +
                                '"></span>');
                            if (chart.data.datasets[i].label) {
                                text.push(chart.data.datasets[i].label);
                            }
                            text.push('</li>');
                        }
                        text.push('</ul>');
                        return text.join('');
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                display: false,
                                min: 0,
                                stepSize: 20,
                                max: 80
                            },
                            gridLines: {
                                drawBorder: false,
                                color: 'rgba(235,237,242,1)',
                                zeroLineColor: 'rgba(235,237,242,1)'
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false,
                                drawBorder: false,
                                color: 'rgba(0,0,0,1)',
                                zeroLineColor: 'rgba(235,237,242,1)'
                            },
                            ticks: {
                                padding: 20,
                                fontColor: "#9c9fa6",
                                autoSkip: true,
                            },
                            categoryPercentage: 0.5,
                            barPercentage: 0.5
                        }]
                    }
                },
                elements: {
                    point: {
                        radius: 0
                    }
                }
            });
            $("#consultations-by-gender-legend").html(myChart.generateLegend());
        }
        if ($("#medical-aid-popularity").length) {
            //var gradientStrokeBlue = ctx.createLinearGradient(0, 0, 0, 181);
            //gradientStrokeBlue.addColorStop(0, 'rgba(54, 215, 232, 1)');
            //gradientStrokeBlue.addColorStop(1, 'rgba(177, 148, 250, 1)');
            //var gradientLegendBlue = 'linear-gradient(to right, rgba(54, 215, 232, 1), rgba(177, 148, 250, 1))';

            //var gradientStrokeRed = ctx.createLinearGradient(0, 0, 0, 50);
            //gradientStrokeRed.addColorStop(0, 'rgba(255, 191, 150, 1)');
            //gradientStrokeRed.addColorStop(1, 'rgba(254, 112, 150, 1)');
            //var gradientLegendRed = 'linear-gradient(to right, rgba(255, 191, 150, 1), rgba(254, 112, 150, 1))';

            //var gradientStrokeGreen = ctx.createLinearGradient(0, 0, 0, 300);
            //gradientStrokeGreen.addColorStop(0, 'rgba(6, 185, 157, 1)');
            //gradientStrokeGreen.addColorStop(1, 'rgba(132, 217, 210, 1)');
            //var gradientLegendGreen = 'linear-gradient(to right, rgba(6, 185, 157, 1), rgba(132, 217, 210, 1))';

            var medical_aid_pop_data = {
                datasets: [{
                    data: var_medical_aid_pop_data,
                    backgroundColor: [
                        gradientStrokeBlue,
                        gradientStrokeGreen,
                        gradientStrokeRed,
                        gradientStrokeRed,
                        gradientStrokeRed,
                        gradientStrokeRed
                    ],
                    hoverBackgroundColor: [
                        gradientStrokeBlue,
                        gradientStrokeGreen,
                        gradientStrokeRed,
                        gradientStrokeRed,
                        gradientStrokeRed,
                        gradientStrokeRed
                    ],
                    borderColor: [
                        gradientStrokeBlue,
                        gradientStrokeGreen,
                        gradientStrokeRed,
                        gradientStrokeRed,
                        gradientStrokeRed,
                        gradientStrokeRed
                    ],
                    legendColor: [
                        gradientLegendBlue,
                        gradientLegendGreen,
                        gradientLegendRed,
                        gradientLegendRed,
                        gradientLegendRed,
                        gradientLegendRed
                    ]
                }],

                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: var_medical_aid_company_labels
            };
            var medical_aid_pop_chart_opt = {
                responsive: true,
                animation: {
                    animateScale: true,
                    animateRotate: true
                },
                legend: false,
                legendCallback: function (chart) {
                    var text = [];
                    text.push('<ul>');
                    for (var i = 0; i < medical_aid_pop_data.datasets[0].data.length; i++) {
                        text.push('<li><span class="legend-dots" style="background:' +
                            medical_aid_pop_data.datasets[0].legendColor[i] +
                            '"></span>');
                        if (medical_aid_pop_data.labels[i]) {
                            text.push(medical_aid_pop_data.labels[i]);
                        }
                        text.push('<span class="float-right">' + medical_aid_pop_data.datasets[0].data[i] + "%" + '</span>');
                        text.push('</li>');
                    }
                    text.push('</ul>');
                    return text.join('');
                }
            };
            var trafficChartCanvas = $("#medical-aid-popularity").get(0).getContext("2d");
            var trafficChart = new Chart(trafficChartCanvas, {
                type: 'doughnut',
                data: medical_aid_pop_data,
                options: medical_aid_pop_chart_opt
            });
            $("#medical-aid-popularity-legend").html(trafficChart.generateLegend());
        }
        if ($("#inline-datepicker").length) {
            $('#inline-datepicker').datepicker({
                enableOnReadonly: true,
                todayHighlight: true
            });
        }
    });
})(jQuery);
