﻿using System;
using PatientRegister.Models;
using PatientRegister.Repositories;

namespace PatientRegister.Data {
    public class InitLookupData {
        public static bool initAll() {
            try {
                //UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());
                //bool _b = AccessLevel.initData(g_UnitOfWork);
                //_b = Configuration.initData(g_UnitOfWork);
                //_b = Gender.initData(g_UnitOfWork);
                //_b = CommunicationOption.initData(g_UnitOfWork);
                //_b = MeasurementUnit.initData(g_UnitOfWork);
                //_b = StaffType.initData(g_UnitOfWork);
                //_b = NotificationType.initData(g_UnitOfWork);
                //_b = NotificationSide.initData(g_UnitOfWork);
                //_b = PersonRelationship.initData(g_UnitOfWork);
                //_b = UserRole.initData(g_UnitOfWork);
                //_b = ResponseStatus.initData(g_UnitOfWork);
                //_b = Status.initData(g_UnitOfWork);
                //_b = TimeUnit.initData(g_UnitOfWork);
                //_b = YesNo.initData(g_UnitOfWork);
                //_b = FileEntityType.initData(g_UnitOfWork);
                UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());
                return AccessLevel.initData(g_UnitOfWork) &&
                Configuration.initData(g_UnitOfWork) &&
                Gender.initData(g_UnitOfWork) &&
                CommunicationOption.initData(g_UnitOfWork) &&
                MeasurementUnit.initData(g_UnitOfWork) &&
                StaffType.initData(g_UnitOfWork) &&
                NotificationType.initData(g_UnitOfWork) &&
                NotificationSide.initData(g_UnitOfWork) &&
                PersonRelationship.initData(g_UnitOfWork) &&
                UserRole.initData(g_UnitOfWork) &&
                ResponseStatus.initData(g_UnitOfWork) &&
                Status.initData(g_UnitOfWork) &&
                TimeUnit.initData(g_UnitOfWork) &&
                YesNo.initData(g_UnitOfWork) &&
                FileEntityType.initData(g_UnitOfWork);
            } catch {
            }
            return false;
        }

        public static void clearAll() {
            try {
                UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());
                AccessLevel.clearData(g_UnitOfWork);
                Configuration.clearData(g_UnitOfWork);
                Gender.clearData(g_UnitOfWork);
                CommunicationOption.clearData(g_UnitOfWork);
                MeasurementUnit.clearData(g_UnitOfWork);
                StaffType.clearData(g_UnitOfWork);
                NotificationType.clearData(g_UnitOfWork);
                NotificationSide.clearData(g_UnitOfWork);
                PersonRelationship.clearData(g_UnitOfWork);
                UserRole.clearData(g_UnitOfWork);
                ResponseStatus.clearData(g_UnitOfWork);
                Status.clearData(g_UnitOfWork);
                TimeUnit.clearData(g_UnitOfWork);
                YesNo.clearData(g_UnitOfWork);
                FileEntityType.clearData(g_UnitOfWork);
            } catch {
            }
        }
    }
}