﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using PatientRegister.Models;

namespace PatientRegister.Repositories {
    public class PatientRegisterContext : DbContext {
        public PatientRegisterContext() : base(Constant.CON_STRING) {
        }
        public DbSet<UserRoleAccess> UserRoleAccesses { get; }
        public DbSet<Branch> Branches { get; }
        public DbSet<MedicalAidCompany> MedicalAidCompanies { get; }
        public DbSet<MedicalStockItem> MedicalStockItems { get; }
        public DbSet<Symptom> Symptoms { get; }
        public DbSet<Diagnosis> Diagnoses { get; }
        public DbSet<UserRole> UserRoles { get; }
        public DbSet<FileUpload> FileUploads { get; }
        public DbSet<AccessLevel> AccessLevels { get; }
        public DbSet<MeasurementUnit> MeasurementUnits { get; }
        public DbSet<TimeUnit> TimeUnits { get; }
        public DbSet<Consultation> Consultations { get; }
        public DbSet<ConsultationDiagnosis> ConsultationDiagnoses { get; }
        public DbSet<ConsultationMedication> ConsultationMedications { get; }
        public DbSet<ConsultationSymptom> ConsutationSymptoms { get; }
        public DbSet<PrescriptionMedication> PrescriptionMedication { get; }
        public DbSet<Referral> Referrals { get; }
        public DbSet<Gender> Genders { get; }
        public DbSet<PersonRelationship> PersonRelationships { get; }
        public DbSet<Status> Statuses { get; }
        public DbSet<Configuration> Configurations { get; }
        public DbSet<YesNo> YesNos { get; }
        public DbSet<NotificationType> NotificationTypes { get; }
        public DbSet<NotificationSide> NotificationSides { get; }
        public DbSet<FileEntityType> FileTypes { get; }
        public DbSet<UserClass> UserClasses { get; }
        public DbSet<ResponseStatus> ResponseStatuses { get; }
        public DbSet<StaffType> StaffTypes { get; }
        public DbSet<CommunicationOption> CommunicationOptions { get; }
        public DbSet<Patient> Patients { get; }
        public DbSet<Prescription> Prescriptions { get; }
        public DbSet<Operation> Operations { get; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public override int SaveChanges() {
            var _vEntries = ChangeTracker
                .Entries()
                .Where(_Entry => _Entry.Entity is HasCreatedAndLastModified && (
                        _Entry.State == EntityState.Added
                        || _Entry.State == EntityState.Modified));
            foreach (var _vEntityEntry in _vEntries) {
                ((HasCreatedAndLastModified)_vEntityEntry.Entity).LastModified = DateTime.Now;
                if (_vEntityEntry.State == EntityState.Added) {
                    ((HasCreatedAndLastModified)_vEntityEntry.Entity).Created = DateTime.Now;
                }
            }
            return base.SaveChanges();
        }
    }
}