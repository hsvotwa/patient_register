﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PatientRegister.Models;

namespace PatientRegister.Repositories {
    public class LookupRepo<T> where T : class {
        public static List<SelectListItem> getSelectData(string _sIdToSelect = "", string _sDefaultValue = "", List<int> _iIDsToExclude = null) {
            UnitOfWork _UnitOfWork = new UnitOfWork(new PatientRegisterContext());
            IQueryable<T> _Data = null;
            if (typeof(T) == typeof(YesNo)) {
                _Data = _UnitOfWork.YesNos.GetAll() as IQueryable<T>;
            } else if (typeof(T) == typeof(Gender)) {
                _Data = _UnitOfWork.Genders.GetAll() as IQueryable<T>;
            } else if (typeof(T) == typeof(TimeUnit)) {
                _Data = _UnitOfWork.TimeUnits.GetAll() as IQueryable<T>;
            } else if (typeof(T) == typeof(Status)) {
                _Data = _UnitOfWork.Statuses.GetAll() as IQueryable<T>;
            } else if (typeof(T) == typeof(StaffType)) {
                _Data = _UnitOfWork.StaffTypes.GetAll() as IQueryable<T>;
            } else if (typeof(T) == typeof(MedicalAidCompany)) {
                _Data = _UnitOfWork.MedicalAidCompanies.GetAll() as IQueryable<T>;
            } else if (typeof(T) == typeof(PersonRelationship)) {
                _Data = _UnitOfWork.PersonRelationships.GetAll() as IQueryable<T>;
            } else if (typeof(T) == typeof(Patient)) {
                _Data = _UnitOfWork.Patients.GetAll() as IQueryable<T>;
                //_Data = _UnitOfWork.Patients.GetFiltered(_Rec => _Rec.StatusID == (int)EnumStatus.active) as IQueryable<T>;
            } else if (typeof(T) == typeof(UserClass)) {
                _Data = _UnitOfWork.UserClasses.GetAll() as IQueryable<T>;
                //_Data = _UnitOfWork.UserClasses.GetFiltered(_Rec => _Rec.StatusID == (int)EnumStatus.active) as IQueryable<T>;
            } else if (typeof(T) == typeof(Diagnosis)) {
                _Data = _UnitOfWork.Diagnoses.GetAll() as IQueryable<T>;
                //_Data = _UnitOfWork.UserClasses.GetFiltered(_Rec => _Rec.StatusID == (int)EnumStatus.active) as IQueryable<T>;
            } else if (typeof(T) == typeof(Symptom)) {
                _Data = _UnitOfWork.Symptoms.GetAll() as IQueryable<T>;
                //_Data = _UnitOfWork.UserClasses.GetFiltered(_Rec => _Rec.StatusID == (int)EnumStatus.active) as IQueryable<T>;
            } else if (typeof(T) == typeof(MedicalStockItem)) {
                _Data = _UnitOfWork.MedicalStockItems.GetAll() as IQueryable<T>;
                //_Data = _UnitOfWork.UserClasses.GetFiltered(_Rec => _Rec.StatusID == (int)EnumStatus.active) as IQueryable<T>;
            } else if (typeof(T) == typeof(TimeUnit)) {
                _Data = _UnitOfWork.TimeUnits.GetAll() as IQueryable<T>;
            } else if (typeof(T) == typeof(MeasurementUnit)) {
                _Data = _UnitOfWork.MeasurementUnits.GetAll() as IQueryable<T>;
            }
            List<SelectListItem> _SelectItems = new List<SelectListItem>();
            //if ( _sIdToSelect == string.Empty ) {
            //    _SelectItems.Add( new SelectListItem { Text = _sDefaultValue, Value = string.Empty, Selected = true } );
            //}
            string _sDisplay = string.Empty;
            string _sValue = string.Empty;
            if (_sDefaultValue != "") {
                _SelectItems.Add(new SelectListItem { Text = _sDefaultValue, Value = "", Selected = true });
            }
            foreach (T _DataItem in _Data) {
                if (typeof(T) == typeof(YesNo)) {
                    if (_iIDsToExclude != null && _iIDsToExclude.Contains((_DataItem as Status).StatusID)) {
                        continue;
                    }
                    _sValue = (_DataItem as YesNo).YesNoID.ToString();
                    _sDisplay = (_DataItem as YesNo).Name;
                } else if (typeof(T) == typeof(Gender)) {
                    if (_iIDsToExclude != null && _iIDsToExclude.Contains((_DataItem as Status).StatusID)) {
                        continue;
                    }
                    _sValue = (_DataItem as Gender).GenderID.ToString();
                    _sDisplay = (_DataItem as Gender).Name;
                } else if (typeof(T) == typeof(TimeUnit)) {
                    if (_iIDsToExclude != null && _iIDsToExclude.Contains((_DataItem as Status).StatusID)) {
                        continue;
                    }
                    _sValue = (_DataItem as TimeUnit).TimeUnitID.ToString();
                    _sDisplay = (_DataItem as TimeUnit).Name;
                } else if (typeof(T) == typeof(Status)) {
                    if (_iIDsToExclude != null && _iIDsToExclude.Contains((_DataItem as Status).StatusID)) {
                        continue;
                    }
                    _sValue = (_DataItem as Status).StatusID.ToString();
                    _sDisplay = (_DataItem as Status).Name;
                } else if (typeof(T) == typeof(StaffType)) {
                    if (_iIDsToExclude != null && _iIDsToExclude.Contains((_DataItem as StaffType).StaffTypeID)) {
                        continue;
                    }
                    _sValue = (_DataItem as StaffType).StaffTypeID.ToString();
                    _sDisplay = (_DataItem as StaffType).Name;
                } else if (typeof(T) == typeof(MedicalAidCompany)) {
                    _sValue = (_DataItem as MedicalAidCompany).MedicalAidCompanyID.ToString();
                    _sDisplay = (_DataItem as MedicalAidCompany).Name;
                } else if (typeof(T) == typeof(PersonRelationship)) {
                    if (_iIDsToExclude != null && _iIDsToExclude.Contains((_DataItem as PersonRelationship).PersonRelationshipID)) {
                        continue;
                    }
                    _sValue = (_DataItem as PersonRelationship).PersonRelationshipID.ToString();
                    _sDisplay = (_DataItem as PersonRelationship).Name;
                } else if (typeof(T) == typeof(Patient)) {
                    _sValue = (_DataItem as Patient).PatientID.ToString();
                    _sDisplay = string.Concat((_DataItem as Patient).Surname, " ", (_DataItem as Patient).Name, " - ", (_DataItem as Patient).PatientNo);
                } else if (typeof(T) == typeof(UserClass)) {
                    _sValue = (_DataItem as UserClass).UserID.ToString();
                    _sDisplay = string.Concat((_DataItem as UserClass).Surname, " ", (_DataItem as UserClass).Name);
                } else if (typeof(T) == typeof(Symptom)) {
                    _sValue = (_DataItem as Symptom).SymptomID.ToString();
                    _sDisplay = (_DataItem as Symptom).Name;
                } else if (typeof(T) == typeof(Diagnosis)) {
                    _sValue = (_DataItem as Diagnosis).DiagnosisID.ToString();
                    _sDisplay = (_DataItem as Diagnosis).Name;
                } else if (typeof(T) == typeof(MedicalStockItem)) {
                    _sValue = (_DataItem as MedicalStockItem).MedicalStockItemID.ToString();
                    _sDisplay = (_DataItem as MedicalStockItem).Name;
                } else if (typeof(T) == typeof(TimeUnit)) {
                    _sValue = (_DataItem as TimeUnit).TimeUnitID.ToString();
                    _sDisplay = (_DataItem as TimeUnit).Name;
                } else if (typeof(T) == typeof(MeasurementUnit)) {
                    _sValue = (_DataItem as MeasurementUnit).MeasurementID.ToString();
                    _sDisplay = (_DataItem as MeasurementUnit).Name;
                }
                _SelectItems.Add(new SelectListItem { Text = _sDisplay, Value = _sValue, Selected = _sValue.Equals(_sIdToSelect) });
            }
            return _SelectItems;
        }
    }
}