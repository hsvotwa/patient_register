﻿using System;
using System.Data.Entity.Validation;
using PatientRegister.Models;

namespace PatientRegister.Repositories {
    public class UnitOfWork : IUnitOfWork {
        private readonly PatientRegisterContext g_dbContext;
        
        private Repository<UserRoleAccess> _UserRoleAccesses;
        private Repository<Branch> _Branches;
        private Repository<MedicalStockItem> _MedicalStockItems;
        private Repository<MedicalAidCompany> _MedicalAidCompanies;
        private Repository<UserRole> _UserRoles;
        private Repository<FileUpload> _FileUploads;
        private Repository<AccessLevel> g_AccessLevels;
        private Repository<MeasurementUnit> _MeasurementUnits;
        private Repository<TimeUnit> _TimeUnits;
        private Repository<Consultation> _Consultations;
        private Repository<ConsultationSymptom> _ConsultationSymptoms;
        private Repository<ConsultationDiagnosis> _ConsultationDiagnoses;
        private Repository<ConsultationMedication> _ConsultationMedications;
        private Repository<PrescriptionMedication> _PrescriptionMedications;
        private Repository<Referral> _Referrals;
        private Repository<Gender> _Genders;
        private Repository<PersonRelationship> _PersonRelationships;
        private Repository<Status> _Statuses;
        private Repository<Configuration> _Configurations;
        private Repository<YesNo> _YesNos;
        private Repository<NotificationType> _NotificationTypes;
        private Repository<NotificationSide> _NotificationSides;
        private Repository<FileEntityType> _FileTypes;
        private Repository<UserClass> _UserClasses;
        private Repository<ResponseStatus> _ResponseStatuses;
        private Repository<StaffType> _StaffTypes;
        private Repository<CommunicationOption> _CommunicationOptions;
        private Repository<Patient> _Patients;
        private Repository<Prescription> _Prescriptions;
        private Repository<Diagnosis> _Diagnoses;
        private Repository<Symptom> _Symptoms;
        private Repository<Operation> _Operations;

        public UnitOfWork(PatientRegisterContext _dbContext) {
            g_dbContext = _dbContext;
        }

        public IRepository<UserRoleAccess> UserRoleAccesses {
            get {
                return _UserRoleAccesses ?? (_UserRoleAccesses = new Repository<UserRoleAccess>(g_dbContext));
            }
        }

        public IRepository<Branch> Branches {
            get {
                return _Branches ?? (_Branches = new Repository<Branch>(g_dbContext));
            }
        }

        public IRepository<MedicalAidCompany> MedicalAidCompanies {
            get {
                return _MedicalAidCompanies ?? (_MedicalAidCompanies = new Repository<MedicalAidCompany>(g_dbContext));
            }
        }

        public IRepository<MedicalStockItem> MedicalStockItems {
            get {
                return _MedicalStockItems ?? (_MedicalStockItems = new Repository<MedicalStockItem>(g_dbContext));
            }
        }

        public IRepository<Symptom> Symptoms {
            get {
                return _Symptoms ?? (_Symptoms = new Repository<Symptom>(g_dbContext));
            }
        }

        public IRepository<Diagnosis> Diagnoses {
            get {
                return _Diagnoses ?? (_Diagnoses = new Repository<Diagnosis>(g_dbContext));
            }
        }

        public IRepository<FileUpload> FileUploads {
            get {
                return _FileUploads ?? (_FileUploads = new Repository<FileUpload>(g_dbContext));
            }
        }

        public IRepository<Patient> Patients {
            get {
                return _Patients ?? (_Patients = new Repository<Patient>(g_dbContext));
            }
        }

        public IRepository<Consultation> Consultations {
            get {
                return _Consultations ?? (_Consultations = new Repository<Consultation>(g_dbContext));
            }
        }

        public IRepository<ConsultationDiagnosis> ConsultationDiagnoses {
            get {
                return _ConsultationDiagnoses ?? (_ConsultationDiagnoses = new Repository<ConsultationDiagnosis>(g_dbContext));
            }
        }

        public IRepository<ConsultationSymptom> ConsultationSymptoms {
            get {
                return _ConsultationSymptoms ?? (_ConsultationSymptoms = new Repository<ConsultationSymptom>(g_dbContext));
            }
        }

        public IRepository<ConsultationMedication> ConsultationMedications {
            get {
                return _ConsultationMedications ?? (_ConsultationMedications = new Repository<ConsultationMedication>(g_dbContext));
            }
        }

        public IRepository<PrescriptionMedication> PrescriptionMedications {
            get {
                return _PrescriptionMedications ?? (_PrescriptionMedications = new Repository<PrescriptionMedication>(g_dbContext));
            }
        }

        public IRepository<Referral> Referrals {
            get {
                return _Referrals ?? (_Referrals = new Repository<Referral>(g_dbContext));
            }
        }

        public IRepository<Prescription> Prescriptions {
            get {
                return _Prescriptions ?? (_Prescriptions = new Repository<Prescription>(g_dbContext));
            }
        }

        public IRepository<Operation> Operations {
            get {
                return _Operations ?? (_Operations = new Repository<Operation>(g_dbContext));
            }
        }

        public IRepository<UserClass> UserClasses {
            get {
                return _UserClasses ?? (_UserClasses = new Repository<UserClass>(g_dbContext));
            }
        }

        public IRepository<StaffType> StaffTypes {
            get {
                return _StaffTypes ?? (_StaffTypes = new Repository<StaffType>(g_dbContext));
            }
        }

        public IRepository<Status> Statuses {
            get {
                return _Statuses ?? (_Statuses = new Repository<Status>(g_dbContext));
            }
        }

        public IRepository<Gender> Genders {
            get {
                return _Genders ?? (_Genders = new Repository<Gender>(g_dbContext));
            }
        }

        public IRepository<PersonRelationship> PersonRelationships {
            get {
                return _PersonRelationships ?? (_PersonRelationships = new Repository<PersonRelationship>(g_dbContext));
            }
        }

        public IRepository<AccessLevel> AccessLevels {
            get {
                return g_AccessLevels ?? (g_AccessLevels = new Repository<AccessLevel>(g_dbContext));
            }
        }

        public IRepository<ResponseStatus> ResponseStatuses {
            get {
                return _ResponseStatuses ?? (_ResponseStatuses = new Repository<ResponseStatus>(g_dbContext));
            }
        }

        public IRepository<FileEntityType> FileTypes {
            get {
                return _FileTypes ?? (_FileTypes = new Repository<FileEntityType>(g_dbContext));
            }
        }

        public IRepository<Configuration> Configurations {
            get {
                return _Configurations ?? (_Configurations = new Repository<Configuration>(g_dbContext));
            }
        }

        public IRepository<YesNo> YesNos {
            get {
                return _YesNos ?? (_YesNos = new Repository<YesNo>(g_dbContext));
            }
        }

        public IRepository<NotificationType> NotificationTypes {
            get {
                return _NotificationTypes ?? (_NotificationTypes = new Repository<NotificationType>(g_dbContext));
            }
        }

        public IRepository<NotificationSide> NotificationSides {
            get {
                return _NotificationSides ?? (_NotificationSides = new Repository<NotificationSide>(g_dbContext));
            }
        }

        public IRepository<MeasurementUnit> MeasurementUnits {
            get {
                return _MeasurementUnits ?? (_MeasurementUnits = new Repository<MeasurementUnit>(g_dbContext));
            }
        }

        public IRepository<TimeUnit> TimeUnits {
            get {
                return _TimeUnits ?? (_TimeUnits = new Repository<TimeUnit>(g_dbContext));
            }
        }

        public IRepository<CommunicationOption> CommunicationOptions {
            get {
                return _CommunicationOptions ?? (_CommunicationOptions = new Repository<CommunicationOption>(g_dbContext));
            }
        }

        public IRepository<UserRole> UserRoles {
            get {
                return _UserRoles ?? (_UserRoles = new Repository<UserRole>(g_dbContext));
            }
        }

        public bool Commit() {
            try {
                g_dbContext.SaveChanges();
                return true;
            } catch (Exception _Ex) {
                if (_Ex.GetType() == typeof(DbEntityValidationException)) {
                    string _sErrors = string.Empty;
                    DbEntityValidationException _DbEntityValidationException = _Ex as DbEntityValidationException;
                    foreach (var _vEve in _DbEntityValidationException.EntityValidationErrors) {
                        _sErrors += string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", _vEve.Entry.Entity.GetType().Name, _vEve.Entry.State);
                        foreach (var _vEveProp in _vEve.ValidationErrors) {
                            _sErrors += string.Format("- Property: \"{0}\", Error: \"{1}\"", _vEveProp.PropertyName, _vEveProp.ErrorMessage);
                        }
                    }
                    throw;
                }
            }
            return false;
        }
    }
}