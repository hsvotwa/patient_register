﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace PatientRegister.Repositories {
    public interface IRepository<T> {
        void Insert(T _Entity);
        void Delete(T _Entity);
        IQueryable<T> SearchFor(Expression<Func<T, bool>> _Predicate);
        IQueryable<T> GetAll();
        void DeleteAll();
        IEnumerable<T> GetFiltered(Expression<Func<T, bool>> _Filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> _OrderBy = null, string _sIncludeProperties = "");
        IEnumerable<T> GetWithRawSql(string _sQuery, params object[] _oParameters);
        T GetById(Guid ID);
    }
}