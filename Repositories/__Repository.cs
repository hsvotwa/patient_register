﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace PatientRegister.Repositories {
    public class Repository<T> : IRepository<T> where T : class {
        protected DbSet<T> g_DbSet;
        private PatientRegisterContext g_DataContext;

        public Repository(PatientRegisterContext _PatientRegisterContext) {
            g_DataContext = _PatientRegisterContext;
            g_DbSet = g_DataContext.Set<T>();
        }

        #region IRepository<T> Members
        public void Insert(T _Entity) {
            g_DbSet.Add(_Entity);
        }

        public void Delete(T _Entity) {
            if (g_DataContext.Entry(_Entity).State == EntityState.Detached) {
                g_DbSet.Attach(_Entity);
            }
            g_DbSet.Remove(_Entity);
        }

        public void DeleteAll() { //For lookups
            g_DbSet.RemoveRange(GetAll());
        }

        public IQueryable<T> SearchFor(Expression<Func<T, bool>> _Predicate) {
            return g_DbSet.Where(_Predicate);
        }

        public IQueryable<T> GetAll() {
            return g_DbSet;
        }

        public virtual IEnumerable<T> GetFiltered(Expression<Func<T, bool>> _Filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> _OrderBy = null, string _sIncludeProperties = "") {
            try {
                IQueryable<T> _Query = g_DbSet;
                if (_Filter != null) {
                    _Query = _Query.Where(_Filter);
                }
                if (_sIncludeProperties != null) {
                    foreach (var _vIncludeProperty in _sIncludeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)) {
                        _Query = _Query.Include(_vIncludeProperty);
                    }
                }
                if (_OrderBy != null) {
                    return _OrderBy(_Query).ToList();
                } else {
                    return _Query.ToList();
                }
            } catch (Exception _Ex) {
                string _S = _Ex.InnerException.Message;
            }
            return  null;
        }

        public T GetById(Guid _gID) {
            return g_DbSet.Find(_gID);
        }

        public virtual IEnumerable<T> GetWithRawSql(string _sQuery, params object[] _oParameters) {
            return g_DbSet.SqlQuery(_sQuery, _oParameters).ToList();
        }
        #endregion
    }
}
