﻿using PatientRegister.Models;

namespace PatientRegister.Repositories {
    public interface IUnitOfWork {
        IRepository<UserRoleAccess> UserRoleAccesses { get; }
        IRepository<Branch> Branches { get; }
        IRepository<MedicalAidCompany> MedicalAidCompanies { get; }
        IRepository<MedicalStockItem> MedicalStockItems { get; }
        IRepository<Symptom> Symptoms { get; }
        IRepository<Diagnosis> Diagnoses { get; }
        IRepository<UserRole> UserRoles { get; }
        IRepository<FileUpload> FileUploads { get; }
        IRepository<AccessLevel> AccessLevels { get; }
        IRepository<MeasurementUnit> MeasurementUnits { get; }
        IRepository<TimeUnit> TimeUnits { get; }
        IRepository<Consultation> Consultations { get; }
        IRepository<ConsultationSymptom> ConsultationSymptoms { get; }
        IRepository<ConsultationDiagnosis> ConsultationDiagnoses { get; }
        IRepository<ConsultationMedication> ConsultationMedications { get; }
        IRepository<PrescriptionMedication> PrescriptionMedications { get; }
        IRepository<Referral> Referrals { get; }
        IRepository<Gender> Genders { get; }
        IRepository<PersonRelationship> PersonRelationships { get; }
        IRepository<Status> Statuses { get; }
        IRepository<Configuration> Configurations { get; }
        IRepository<YesNo> YesNos { get; }
        IRepository<NotificationType> NotificationTypes { get; }
        IRepository<NotificationSide> NotificationSides { get; }
        IRepository<FileEntityType> FileTypes { get; }
        IRepository<UserClass> UserClasses { get; }
        IRepository<ResponseStatus> ResponseStatuses { get; }
        IRepository<StaffType> StaffTypes { get; }
        IRepository<CommunicationOption> CommunicationOptions { get; }
        IRepository<Patient> Patients { get; }
        IRepository<Prescription> Prescriptions { get; }
        IRepository<Operation> Operations { get; }
        bool Commit();
    }
}