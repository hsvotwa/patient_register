﻿using System;
namespace PatientRegister.Repositories {
    public interface IEntity {
        Guid ID { get; }
    }
}