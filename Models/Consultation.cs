﻿using PatientRegister.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace PatientRegister.Models {
    [Table(SqlTable.CONSULTATION)]
    public class Consultation: HasCreatedAndLastModified {
        #region Public Properties
        [Key] public Guid ConsultationID { get; set; }

        [ForeignKey("Patient")]
        [Required] public Guid PatientID { get; set; }

        [ForeignKey("Doctor")]
        [Required] public Guid DoctorID { get; set; }

        [Column(TypeName = "DateTime2")]
        [Required] public DateTime Date { get; set; }
        [Required] public decimal Temperature { get; set; }
        [Required] public decimal BloodPressure { get; set; }
        public decimal Weight { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime? NextVisitDate { get; set; }
        [Required] public string SummaryComment { get; set; }
        [Required] public string DetailedComment { get; set; }

        public virtual Patient Patient { get; set; }
        public virtual UserClass Doctor { get; set; }

        public IEnumerable<ConsultationSymptom> getSymptoms() {
            if (this.ConsultationID == Guid.Empty) {
                return Enumerable.Empty<ConsultationSymptom>();
            }
            UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());
            return g_UnitOfWork.ConsultationSymptoms.GetFiltered(_Rec => _Rec.ConsultationID == this.ConsultationID && _Rec.Deleted == (int)EnumYesNo.no);
        }

        public IEnumerable<ConsultationDiagnosis> getDiagnses() {
            if (this.ConsultationID == Guid.Empty) {
                return Enumerable.Empty<ConsultationDiagnosis>();
            }
            UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());
            return g_UnitOfWork.ConsultationDiagnoses.GetFiltered(_Rec => _Rec.ConsultationID == this.ConsultationID && _Rec.Deleted == (int)EnumYesNo.no);
        }

        public IEnumerable<ConsultationMedication> getMedications() {
            if (this.ConsultationID == Guid.Empty) {
                return Enumerable.Empty<ConsultationMedication>();
            }
            UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());
            return g_UnitOfWork.ConsultationMedications.GetFiltered(_Rec => _Rec.ConsultationID == this.ConsultationID && _Rec.Deleted == (int)EnumYesNo.no);
        }
        #endregion
    }
}