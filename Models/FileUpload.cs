﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientRegister.Models {
    [Table( SqlTable.FILE )]
    public class FileUpload : HasCreatedAndLastModified {
        #region Member Variables
        [Key] public Guid FileUploadID { get; set; }
        [Required] public Guid LinkID { get; set; }
        [Required] public string ServerName { get; set; }
        [Required] public string GivenName { get; set; }

        [ForeignKey( "FileType" )]
        [Required] public int FileTypeID { get; set; }

        [ForeignKey( "Status" )]
        [Required] public int StatusID { get; set; }

        [ForeignKey( "User" )]
        [Required] public Guid UserId { get; set; }

        [Required] public int Deleted { get; set; }
        [Required] public DateTime DateUploaded { get; set; }

        public virtual FileEntityType FileType { get; set; }
        public virtual Status Status { get; set; }
        public virtual UserClass User { get; set; }
        #endregion

        #region Public Properties
        public static List<FileUpload> getAllLinkedFile( string _sUuid ) {
            return null;
        }
        #endregion
    }
}