﻿using PatientRegister.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace PatientRegister.Models {
    [Table(SqlTable.PATIENT)]
    public class Patient : HasCreatedAndLastModified {
        #region Public Properties
        [Key] public Guid PatientID { get; set; }
        [Required] public int PatientNo { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime DateOfBirth { get; set; }
        public string IDNumber { get; set; }
        public string PostalAddress { get; set; }
        public string ResidentialAddress { get; set; }
        public string HomeTel { get; set; }
        public string WorkTel { get; set; }
        public string CellNo { get; set; }
        public string Email { get; set; }
        public string Occupation { get; set; }
        public string Employer { get; set; }
        public string MedicalAidName { get; set; }
        public string MedicalAidMainMemberName { get; set; }
        public string MedicalAidMainMemberIDNo { get; set; }
        public string MedicalAidNo { get; set; }
        public string MedicalAidMainMemberOccupation { get; set; }
        public string MedicalAidMainMemberPhone { get; set; }

        [ForeignKey("MedicalAidMainRelationship")]
        public int MedicalAidMainMemberRelationshipID { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyContactPhone { get; set; }

        [ForeignKey("EmergencyContactRelationship")]
        public int EmergencyContactRelationshipID { get; set; }

        [ForeignKey("CommunicationOption")]
        public int CommunicationOptionID { get; set; }

        [ForeignKey("Status")]
        [Required] public int StatusID { get; set; }

        [ForeignKey("Gender")]
        public int GenderID { get; set; }

        [ForeignKey("MedicalAidCompany")]
        public Guid? MedicalAidCompanyID { get; set; }

        public virtual CommunicationOption CommunicationOption { get; set; }
        public virtual Status Status { get; set; }
        public virtual Gender Gender { get; set; }
        public virtual MedicalAidCompany MedicalAidCompany { get; set; }
        public virtual PersonRelationship MedicalAidMainRelationship { get; set; }
        public virtual PersonRelationship EmergencyContactRelationship { get; set; }

        public int getNextPatientNumber() {
            try {
                UnitOfWork _UnitOfWork = new UnitOfWork(new PatientRegisterContext());
                return Constant.PATIENT_NO_START_PREFIX + _UnitOfWork.Patients.GetAll().Count() + 1;
            } catch (Exception) {
            }
            return 0;
        }
        #endregion
    }
}