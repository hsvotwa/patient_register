﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PatientRegister.Repositories;

namespace PatientRegister.Models {
    [Table( SqlTable.LU_STATUS )]
    public class Status : HasCreatedAndLastModified {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.None )]
        public int StatusID { get; set; }

        [Required, MaxLength( 100 )]
        public string Name { get; set; }

        public static bool initData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.Statuses.Insert( new Status {
                    StatusID = ( int )EnumStatus.active,
                    Name = "Active",
                } );
                g_UnitOfWork.Statuses.Insert( new Status {
                    StatusID = ( int )EnumStatus.inactive,
                    Name = "Inactive",
                } );
                g_UnitOfWork.Statuses.Insert( new Status {
                    StatusID = ( int )EnumStatus.deleted,
                    Name = "Deleted",
                } );
                return g_UnitOfWork.Commit();
            } catch (Exception){
            }
            return false;
        }

        public static bool clearData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.Statuses.DeleteAll();
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }
    }
}