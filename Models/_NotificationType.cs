﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PatientRegister.Repositories;

namespace PatientRegister.Models {
    [Table( SqlTable.LU_NOTIFICATION_TYPE )]
    public class NotificationType : HasCreatedAndLastModified {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.None )]
        public int NotificationTypeID { get; set; }

        [Required, MaxLength( 100 )]
        public string Name { get; set; }

        public static bool initData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.NotificationTypes.Insert( new NotificationType {
                    NotificationTypeID = ( int )EnumNotificationType.error,
                    Name = "Error",
                } );
                g_UnitOfWork.NotificationTypes.Insert( new NotificationType {
                    NotificationTypeID = ( int )EnumNotificationType.warning,
                    Name = "Warning",
                } );
                g_UnitOfWork.NotificationTypes.Insert( new NotificationType {
                    NotificationTypeID = ( int )EnumNotificationType.info,
                    Name = "Info",
                } );
                g_UnitOfWork.NotificationTypes.Insert( new NotificationType {
                    NotificationTypeID = ( int )EnumNotificationType.success,
                    Name = "Success",
                } );
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }

        public static bool clearData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.NotificationTypes.DeleteAll();
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }
    }
}