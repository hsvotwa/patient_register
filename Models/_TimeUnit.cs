﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PatientRegister.Repositories;

namespace PatientRegister.Models {
    [Table(SqlTable.LU_TIME_UNIT)]
    public class TimeUnit : HasCreatedAndLastModified {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TimeUnitID { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        public static bool initData(UnitOfWork g_UnitOfWork) {
            try {
                g_UnitOfWork.TimeUnits.Insert(new TimeUnit {
                    TimeUnitID = (int)EnumTimeUnit.day,
                    Name = "day(s)",
                });
                g_UnitOfWork.TimeUnits.Insert(new TimeUnit {
                    TimeUnitID = (int)EnumTimeUnit.week,
                    Name = "week(s)",
                });
                g_UnitOfWork.TimeUnits.Insert(new TimeUnit {
                    TimeUnitID = (int)EnumTimeUnit.month,
                    Name = "month(s)",
                });
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }

        public static bool clearData(UnitOfWork g_UnitOfWork) {
            try {
                g_UnitOfWork.TimeUnits.DeleteAll();
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }
    }
}