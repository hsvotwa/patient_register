﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PatientRegister.Repositories;

namespace PatientRegister.Models {
    [Table(SqlTable.LU_ACCESS_LEVEL)]
    public partial class AccessLevel : HasCreatedAndLastModified {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AccessLevelID { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        public static bool initData(UnitOfWork g_UnitOfWork) {
            try {
                g_UnitOfWork.AccessLevels.Insert(new AccessLevel {
                    AccessLevelID = (int)EnumAccessLevel.none,
                    Name = "None",
                });
                g_UnitOfWork.AccessLevels.Insert(new AccessLevel {
                    AccessLevelID = (int)EnumAccessLevel.read,
                    Name = "Read",
                });
                g_UnitOfWork.AccessLevels.Insert(new AccessLevel {
                    AccessLevelID = (int)EnumAccessLevel.write,
                    Name = "Write",
                });
                return g_UnitOfWork.Commit();
            } catch (Exception _Ex){
            }
            return false;
        }

        public static bool clearData(UnitOfWork g_UnitOfWork) {
            try {
                g_UnitOfWork.AccessLevels.DeleteAll();
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }
    }
}