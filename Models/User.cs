﻿using PatientRegister.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace PatientRegister.Models {
    [Table(SqlTable.USER)]
    public class UserClass : HasCreatedAndLastModified {
        #region Public Properties
        [Key] public Guid UserID { get; set; }

        [ForeignKey("Branch")]
        public Guid BranchID { get; set; }

        [ForeignKey("Status")]
        public int StatusID { get; set; }
        [Required] public string Name { get; set; }
        [Required] public string Surname { get; set; }
        [Required] public string Email { get; set; }
        [Required] public string Password { get; set; }

        [ForeignKey("StaffType")]
        [Required] public int StaffTypeID { get; set; }

        public virtual Status Status { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual StaffType StaffType { get; set; }
        #endregion

        #region Public Properties
        public static bool authenticateUser(string _sUsername, string _sEncrPassword, bool _bKeepSession = false, bool _bLogIn = true) {
            try {
                UnitOfWork _UnitOfWork = new UnitOfWork(new PatientRegisterContext());
                IEnumerable<UserClass> _MatchedUsers = _UnitOfWork.UserClasses.GetFiltered(_User => _User.Email == _sUsername && _User.Password == _sEncrPassword);
                if (_MatchedUsers.Count() == 0) {
                    return false;
                }
                if (!_bLogIn) { //Just checking if it's valid
                    return true;
                }
                UserSession.setUser(_MatchedUsers.First(), _bKeepSession);
                return true;
            } catch (Exception _Ex) {
                ErrorHandler.handleError("Class", "UserClass", "authenticateUser", _Ex.Message);
            }
            return false;
        }

        public static string encryptPwd(string clearText, string _sEncryptionKey = "Zvibaba88Svotwa94Zvacho17162") {
            try {
                byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
                using (Aes encryptor = Aes.Create()) {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(_sEncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream()) {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)) {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        clearText = Convert.ToBase64String(ms.ToArray());
                    }
                }
                return clearText;
            } catch (Exception _Ex) {
                ErrorHandler.handleError("Class", "UserClass", "encryptPwd", _Ex.Message);
                return "";
            }
        }

        public static string getProfPic(string _sGuid) {
            string _sPath = "/Images/user-avatar-placeholder.png";
            try {
                UnitOfWork _UnitOfWork = new UnitOfWork(new PatientRegisterContext());
                IEnumerable<FileUpload> _MatchedUsers = _UnitOfWork.FileUploads.GetFiltered(_File => _File.LinkID.ToString() == _sGuid && _File.FileTypeID == (int)EnumFileEntityType.prescription);
                if (_MatchedUsers.Count() > 0) {
                    _sPath = "/" + Constant.UPLOADS_FOLDER + "/" + _MatchedUsers.First().ServerName;
                }
            } catch (Exception _Ex) {
                ErrorHandler.handleError("Class", "File", "get", _Ex.Message);
            }
            return _sPath;
        }

        public string decryptPwd(string cipherText, string _sEncryptionKey = "Zvibaba88Svotwa94Zvacho17162") {
            try {
                byte[] cipherBytes = Convert.FromBase64String(cipherText);
                using (Aes encryptor = Aes.Create()) {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(_sEncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream()) {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)) {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        cipherText = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
                return cipherText;
            } catch (Exception _Ex) {
                ErrorHandler.handleError("Class", "UserClass", "decryptPwd", _Ex.Message);
                return "";
            }
        }

        public static bool emailTaken(string _sEmail, string _sUserUuid = "") {
            try {
                string _sQuery = @"Select * 
                                    from tbl_user
                                    where email ='" + _sEmail.Trim() + @"'"
                                       + (_sUserUuid.Trim() == string.Empty ? ";" : " and uuid <> '" + _sUserUuid + "';");
                return false;// To do: Implement logic
            } catch (Exception _Ex) {
                ErrorHandler.handleError("Class", "UserClass", "emailTaken", _Ex.Message);
            }
            return false;
        }

        public static bool UsernameNotInUse(string _sUsername, string _sUserUuid = "", bool _bCheckPilot = false) {
            try {
                string _sQuery = @"Select * 
                                    from tbl_user
                                    where username ='" + _sUsername.Trim() + @"'"
                                       + (_sUserUuid.Trim() == string.Empty ? ";" : " and " + (_bCheckPilot ? "pilot_uuid" : "uuid") + " <> '" + _sUserUuid + "';");
                return false;// To do: Implement logic
            } catch (Exception _Ex) {
                ErrorHandler.handleError("Class", "UserClass", "UsernameNotInUse", _Ex.Message);
            }
            return false;
        }

        public static List<UserRoleAccess> getAccessRoleList(string _sCurrentUserGuid = "") {
            List<UserRoleAccess> _RoleList = new List<UserRoleAccess>();
            UnitOfWork _UnitOfWork = new UnitOfWork(new PatientRegisterContext());
            try {
                if (string.IsNullOrEmpty(_sCurrentUserGuid)) {
                    _sCurrentUserGuid = UserSession.getCurrentUserKey();
                }
                Guid _CurrentUserGuid = Guid.Parse(_sCurrentUserGuid);
                IEnumerable<UserRole> _AllAccessRoles = _UnitOfWork.UserRoles.GetAll();
                if (_AllAccessRoles == null ) {
                    return new List<UserRoleAccess>();
                }
                foreach (UserRole _UserRole in _AllAccessRoles) {
                    UserRoleAccess _UserRoleAccess = _UnitOfWork.UserRoleAccesses.GetFiltered(_User => _User.UserID == _CurrentUserGuid && _User.UserRoleID == _UserRole.UserRoleID).FirstOrDefault();
                    if (_UserRoleAccess == null || _UserRoleAccess.UserRoleAccessID == Guid.Empty) {
                        _UserRoleAccess = new UserRoleAccess();
                        _UserRoleAccess.UserID = _CurrentUserGuid;
                        _UserRoleAccess.UserRoleAccessID = Guid.NewGuid();
                        _UserRoleAccess.UserRoleID = _UserRole.UserRoleID;
                        _UserRoleAccess.AccessLevelID = (int)EnumAccessLevel.none;
                        _UserRoleAccess.UserRole = _UserRole;
                    }
                    _RoleList.Add(_UserRoleAccess);
                }
            } catch (Exception _Ex) {
                ErrorHandler.handleError("Class", "UserClass", "getAccess", _Ex.Message);
            }
            return _RoleList;
        }
        #endregion
    }
}