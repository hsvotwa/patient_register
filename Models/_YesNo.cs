﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PatientRegister.Repositories;

namespace PatientRegister.Models {
    [Table( SqlTable.LU_YES_NO )]
    public class YesNo : HasCreatedAndLastModified {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.None )]
        public int YesNoID { get; set; }

        [Required, MaxLength( 100 )]
        public string Name { get; set; }

        public static bool initData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.YesNos.Insert( new YesNo {
                    YesNoID = ( int )EnumYesNo.yes,
                    Name = "Yes",
                } );
                g_UnitOfWork.YesNos.Insert( new YesNo {
                    YesNoID = ( int )EnumYesNo.no,
                    Name = "No",
                } );
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }

        public static bool clearData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.YesNos.DeleteAll();
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }
    }
}