﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientRegister.Models {
    [Table(SqlTable.CONSULTATION_DIAGNOSIS)]
    public class ConsultationDiagnosis : HasCreatedAndLastModified {
        #region Member Variables
        [Key]
        public Guid ConsultationDiagnosisID { get; set; }

        [ForeignKey("Consultation")]
        public Guid ConsultationID { get; set; }

        [ForeignKey("Diagnosis")]
        public Guid DiagnosisID { get; set; }

        [Required] public int Deleted { get; set; }

        public virtual Consultation Consultation { get; set; }
        public virtual Diagnosis Diagnosis { get; set; }
        #endregion
    }
}