﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PatientRegister.Repositories;

namespace PatientRegister.Models {
    [Table(SqlTable.LU_FILE_TYPE)]
    public class FileEntityType : HasCreatedAndLastModified {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FileTypeID { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        public static bool initData(UnitOfWork g_UnitOfWork) {
            try {
                g_UnitOfWork.FileTypes.Insert(new FileEntityType {
                    FileTypeID = (int)EnumFileEntityType.consultation,
                    Name = "Consultation",
                });
                g_UnitOfWork.FileTypes.Insert(new FileEntityType {
                    FileTypeID = (int)EnumFileEntityType.prescription,
                    Name = "Prescription",
                });
                g_UnitOfWork.FileTypes.Insert(new FileEntityType {
                    FileTypeID = (int)EnumFileEntityType.operation,
                    Name = "Operation",
                });
                g_UnitOfWork.FileTypes.Insert(new FileEntityType {
                    FileTypeID = (int)EnumFileEntityType.referral,
                    Name = "Referral",
                });
                g_UnitOfWork.FileTypes.Insert(new FileEntityType {
                    FileTypeID = (int)EnumFileEntityType.user,
                    Name = "User",
                });
                g_UnitOfWork.FileTypes.Insert(new FileEntityType {
                    FileTypeID = (int)EnumFileEntityType.patient,
                    Name = "Patient",
                });
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }

        public static bool clearData(UnitOfWork g_UnitOfWork) {
            try {
                g_UnitOfWork.FileTypes.DeleteAll();
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }
    }
}
