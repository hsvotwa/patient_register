﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PatientRegister.Repositories;

namespace PatientRegister.Models {
    [Table(SqlTable.LU_MEASUREMENT_UNIT)]
    public class MeasurementUnit : HasCreatedAndLastModified {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MeasurementID { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        public static bool initData(UnitOfWork g_UnitOfWork) {
            try {
                g_UnitOfWork.MeasurementUnits.Insert(new MeasurementUnit {
                    MeasurementID = (int)EnumMeasurementUnit.miligrams,
                    Name = "miligram(s)",
                });
                g_UnitOfWork.MeasurementUnits.Insert(new MeasurementUnit {
                    MeasurementID = (int)EnumMeasurementUnit.units,
                    Name = "unit(s)",
                });
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }

        public static bool clearData(UnitOfWork g_UnitOfWork) {
            try {
                g_UnitOfWork.MeasurementUnits.DeleteAll();
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }
    }
}