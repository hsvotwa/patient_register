﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using PatientRegister.Repositories;

namespace PatientRegister.Models {
    [Table(SqlTable.LU_CONFIGURATION)]
    public partial class Configuration : HasCreatedAndLastModified {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ConfigurationID { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        [Required]
        public string Value { get; set; }

        public static bool initData(UnitOfWork g_UnitOfWork) {
            try {
                g_UnitOfWork.Configurations.Insert(new Configuration {
                    ConfigurationID = (int)EnumConfiguration.logo_path,
                    Name = "Logo Path",
                    Value = "/assets/images/logo_gt.png"
                });
                g_UnitOfWork.Configurations.Insert(new Configuration {
                    ConfigurationID = (int)EnumConfiguration.practice_name,
                    Name = "Practice Name",
                    Value = Constant.COMPANY_NAME
                });
                g_UnitOfWork.Configurations.Insert(new Configuration {
                    ConfigurationID = (int)EnumConfiguration.referral_record_count,
                    Name = "Referral Record Count",
                    Value = "5"
                });
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }

        public static bool clearData(UnitOfWork g_UnitOfWork) {
            try {
                g_UnitOfWork.Configurations.DeleteAll();
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }

        public static string getValue(EnumConfiguration _Configuration) {
            try {
                UnitOfWork _UnitOfWork = new UnitOfWork(new PatientRegisterContext());
                IEnumerable<Configuration> _Configurations = _UnitOfWork.Configurations.GetFiltered(
                     _Rec => _Rec.ConfigurationID == (int)_Configuration
                 );
                Configuration _Config = _Configurations.FirstOrDefault();
                if (_Config == null || string.IsNullOrEmpty(_Config.Value)) {
                    switch (_Configuration) {
                        case EnumConfiguration.practice_name:
                            return Constant.COMPANY_NAME;
                        case EnumConfiguration.referral_record_count:
                            return Constant.REFERRAL_HISTORY_RECORD_COUNT.ToString();
                    }
                }
                return _Config.Value;
            } catch (Exception _Ex) {
            }
            return string.Empty;
        }
    }
}