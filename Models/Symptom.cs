﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientRegister.Models {
    [Table(SqlTable.SYMPTOM)]
    public class Symptom : HasCreatedAndLastModified {
        #region Member Variables
        [Key] public Guid SymptomID { get; set; }
        [Required] public string Name { get; set; }

        [ForeignKey("Status")]
        [Required] public int StatusID { get; set; }

        public virtual Status Status { get; set; }
        #endregion
    }
}