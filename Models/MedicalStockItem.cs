﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientRegister.Models {
    [Table(SqlTable.MEDICAL_STOCK_ITEM)]
    public class MedicalStockItem : HasCreatedAndLastModified {
        #region Member Variables
        [Key] public Guid MedicalStockItemID { get; set; }
        [Required] public string Name { get; set; }

        [ForeignKey("Status")]
        [Required] public int StatusID { get; set; }

        public virtual Status Status { get; set; }
        #endregion
    }
}