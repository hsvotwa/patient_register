﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PatientRegister.Repositories;

namespace PatientRegister.Models {
    [Table( SqlTable.LU_RESPONSE_STATUS )]
    public class ResponseStatus : HasCreatedAndLastModified {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.None )]
        public int ResponseStatusID { get; set; }

        [Required, MaxLength( 100 )]
        public string Name { get; set; }

        public static bool initData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.ResponseStatuses.Insert( new ResponseStatus {
                    ResponseStatusID = ( int )EnumResponseStatus.success,
                    Name = "Success",
                } );
                g_UnitOfWork.ResponseStatuses.Insert( new ResponseStatus {
                    ResponseStatusID = ( int )EnumResponseStatus.failed,
                    Name = "Failed",
                } );
                g_UnitOfWork.ResponseStatuses.Insert( new ResponseStatus {
                    ResponseStatusID = ( int )EnumResponseStatus.error,
                    Name = "Error",
                } );
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }

        public static bool clearData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.ResponseStatuses.DeleteAll();
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }
    }
}