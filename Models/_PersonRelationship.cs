﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PatientRegister.Repositories;

namespace PatientRegister.Models {
    [Table(SqlTable.LU_PERSON_RELATIONSHSHIP)]
    public class PersonRelationship : HasCreatedAndLastModified {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PersonRelationshipID { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        public static bool initData(UnitOfWork g_UnitOfWork) {
            try {
                g_UnitOfWork.PersonRelationships.Insert(new PersonRelationship {
                    PersonRelationshipID = (int)EnumPersonRelationship.other,
                    Name = "Other"
                });
                g_UnitOfWork.PersonRelationships.Insert(new PersonRelationship {
                    PersonRelationshipID = (int)EnumPersonRelationship.spouse,
                    Name = "Spouse"
                });
                g_UnitOfWork.PersonRelationships.Insert(new PersonRelationship {
                    PersonRelationshipID = (int)EnumPersonRelationship.sibling,
                    Name = "Sibling"
                });
                g_UnitOfWork.PersonRelationships.Insert(new PersonRelationship {
                    PersonRelationshipID = (int)EnumPersonRelationship.parent,
                    Name = "Parent"
                });
                g_UnitOfWork.PersonRelationships.Insert(new PersonRelationship {
                    PersonRelationshipID = (int)EnumPersonRelationship.grandchild,
                    Name = "Gran Child"
                });
                g_UnitOfWork.PersonRelationships.Insert(new PersonRelationship {
                    PersonRelationshipID = (int)EnumPersonRelationship.grandparent,
                    Name = "Grand Parent"
                });
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }

        public static bool clearData(UnitOfWork g_UnitOfWork) {
            try {
                g_UnitOfWork.PersonRelationships.DeleteAll();
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }
    }
}