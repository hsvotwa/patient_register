﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PatientRegister.Repositories;

namespace PatientRegister.Models {
    [Table( SqlTable.LU_NOTIFICATION_SIDE )]
    public class NotificationSide : HasCreatedAndLastModified {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.None )]
        public int NotificationSideID { get; set; }

        [Required, MaxLength( 100 )]
        public string Name { get; set; }

        public static bool initData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.NotificationSides.Insert( new NotificationSide {
                    NotificationSideID = ( int )EnumNotificationSide.bottom,
                    Name = "Bottom",
                } );
                g_UnitOfWork.NotificationSides.Insert( new NotificationSide {
                    NotificationSideID = ( int )EnumNotificationSide.left,
                    Name = "Left",
                } );
                g_UnitOfWork.NotificationSides.Insert( new NotificationSide {
                    NotificationSideID = ( int )EnumNotificationSide.right,
                    Name = "Right",
                } );
                g_UnitOfWork.NotificationSides.Insert( new NotificationSide {
                    NotificationSideID = ( int )EnumNotificationSide.top,
                    Name = "Top"
                } );
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }

        public static bool clearData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.NotificationSides.DeleteAll();
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }
    }
}