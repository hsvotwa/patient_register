﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientRegister.Models {
    [Table(SqlTable.OPERATION)]
    public class Operation : HasCreatedAndLastModified {
        #region Public Properties
        [Key] public Guid OperationID { get; set; }

        [ForeignKey("Consultation")]
        public Guid? ConsultationID { get; set; }

        [ForeignKey("Patient")]
        [Required] public Guid PatientID { get; set; }

        [ForeignKey("Doctor")]
        [Required] public Guid DoctorID { get; set; }

        [Column(TypeName = "DateTime2")]
        [Required] public DateTime Date { get; set; }
        [Required] public string Premedication { get; set; }
        [Required] public string OperationName { get; set; }
        
        [Column(TypeName = "DateTime2")]
        [Required] public DateTime StartTime { get; set; }

        [Column(TypeName = "DateTime2")]
        [Required] public DateTime EndTime { get; set; }
        public string Anaesthetic { get; set; }
        [Required] public string Remarks { get; set; }

        [ForeignKey("Surgeon")]
        [Required] public Guid SurgeonID { get; set; }

        [ForeignKey("AssistantSurgeon")]
        //[Column("ass")]
        public Guid AssistantSurgeonID { get; set; }

        [ForeignKey("Anaesthetist")]
        public Guid AnaesthetistID { get; set; }


        public virtual Consultation Consultation { get; set; }
        public virtual Patient Patient { get; set; }
        public virtual UserClass Doctor { get; set; }
        public virtual UserClass Surgeon { get; set; }
        public virtual UserClass AssistantSurgeon { get; set; }
        public virtual UserClass Anaesthetist { get; set; }
        #endregion
    }
}