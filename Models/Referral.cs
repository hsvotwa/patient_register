﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientRegister.Models {
    [Table(SqlTable.REFERRAL)]
    public class Referral : HasCreatedAndLastModified {
        #region Public Properties
        [Key] public Guid ReferralID { get; set; }

        [ForeignKey("Consultation")]
        public Guid? ConsultationID { get; set; }

        [ForeignKey("Patient")]
        [Required] public Guid PatientID { get; set; }

        [ForeignKey("Doctor")]
        [Required] public Guid DoctorID { get; set; }

        [Column(TypeName = "DateTime2")]
        [Required] public DateTime Date { get; set; }
        [Required] public string ReferredToDoctor { get; set; }
        public string CurrentMedication { get; set; }
        [Required] public string History { get; set; }
        [Required] public string ExaminationAndInvestigation { get; set; }
        [Required] public string SummaryComment { get; set; }

        public virtual Consultation Consultation { get; set; }
        public virtual Patient Patient { get; set; }
        public virtual UserClass Doctor { get; set; }
        #endregion
    }
}