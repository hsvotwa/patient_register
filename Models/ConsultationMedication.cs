﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientRegister.Models {
    [Table(SqlTable.CONSULTATION_MEDICATION)]
    public class ConsultationMedication : HasCreatedAndLastModified {
        #region Member Variables
        [Key]
        public Guid ConsultationMedicationID { get; set; }

        [ForeignKey("Consultation")]
        public Guid ConsultationID { get; set; }

        [ForeignKey("MedicalStockItem")]
        public Guid MedicalStockItemID { get; set; }

        public decimal Quantity { get; set; }

        [ForeignKey("MeasurementUnit")]
        public int MeasurementUnitID { get; set; }

        public decimal TimeLength { get; set; }

        [ForeignKey("TimeUnit")]
        public int TimeUnitID { get; set; }

        [Required] public int Deleted { get; set; }

        public virtual Consultation Consultation { get; set; }
        public virtual MeasurementUnit MeasurementUnit { get; set; }
        public virtual TimeUnit TimeUnit { get; set; }
        public virtual MedicalStockItem MedicalStockItem { get; set; }
        #endregion
    }
}