﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PatientRegister.Repositories;

namespace PatientRegister.Models {
    [Table(SqlTable.LU_USER_ROLE)]
    public class UserRole : HasCreatedAndLastModified {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserRoleID { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        public static bool initData(UnitOfWork g_UnitOfWork) {
            try {
                g_UnitOfWork.UserRoles.Insert(new UserRole {
                    UserRoleID = (int)EnumUserRole.branches,
                    Name = "Branches",
                });
                g_UnitOfWork.UserRoles.Insert(new UserRole {
                    UserRoleID = (int)EnumUserRole.consultations,
                    Name = "Consultations",
                });
                g_UnitOfWork.UserRoles.Insert(new UserRole {
                    UserRoleID = (int)EnumUserRole.diagnoses,
                    Name = "Diagnosis",
                });
                g_UnitOfWork.UserRoles.Insert(new UserRole {
                    UserRoleID = (int)EnumUserRole.operations,
                    Name = "Operations",
                });
                g_UnitOfWork.UserRoles.Insert(new UserRole {
                    UserRoleID = (int)EnumUserRole.patients,
                    Name = "Patients",
                });
                g_UnitOfWork.UserRoles.Insert(new UserRole {
                    UserRoleID = (int)EnumUserRole.prescriptions,
                    Name = "Prescriptions",
                });
                g_UnitOfWork.UserRoles.Insert(new UserRole {
                    UserRoleID = (int)EnumUserRole.referrals,
                    Name = "Referrals",
                });
                g_UnitOfWork.UserRoles.Insert(new UserRole {
                    UserRoleID = (int)EnumUserRole.settings,
                    Name = "Settings",
                });
                g_UnitOfWork.UserRoles.Insert(new UserRole {
                    UserRoleID = (int)EnumUserRole.symptoms,
                    Name = "Symptoms",
                });
                g_UnitOfWork.UserRoles.Insert(new UserRole {
                    UserRoleID = (int)EnumUserRole.users,
                    Name = "Users",
                });
                g_UnitOfWork.UserRoles.Insert(new UserRole {
                    UserRoleID = (int)EnumUserRole.medical_stock_item,
                    Name = "Medical Stock Items",
                });
                g_UnitOfWork.UserRoles.Insert(new UserRole {
                    UserRoleID = (int)EnumUserRole.medical_aid_company,
                    Name = "Medical Aid Companies",
                });
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }

        public static bool clearData(UnitOfWork g_UnitOfWork) {
            try {
                g_UnitOfWork.UserRoles.DeleteAll();
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }
    }
}