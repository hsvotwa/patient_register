﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientRegister.Models {
    [Table(SqlTable.CONSULTATION_SYMPTOM)]
    public class ConsultationSymptom : HasCreatedAndLastModified {
        #region Member Variables
        [Key]
        public Guid ConsultationSymptomID { get; set; }

        [ForeignKey("Consultation")]
        public Guid ConsultationID { get; set; }

        [ForeignKey("Symptom")]
        public Guid SymptomID { get; set; }
       [Required] public int Deleted { get; set; }

        public virtual Consultation Consultation { get; set; }
        public virtual Symptom Symptom { get; set; }
        #endregion
    }
}