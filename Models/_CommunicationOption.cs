﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PatientRegister.Repositories;

namespace PatientRegister.Models {
    [Table( SqlTable.LU_COMMUNICATION_OPTION)]
    public class CommunicationOption : HasCreatedAndLastModified {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.None )]
        public int CommunicationOptionID { get; set; }

        [Required, MaxLength( 100 )]
        public string Name { get; set; }

        public static bool initData(UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.CommunicationOptions.Insert( new CommunicationOption {
                    CommunicationOptionID = ( int )EnumCommunicationOption.none,
                    Name = "None"
                } );
                g_UnitOfWork.CommunicationOptions.Insert( new CommunicationOption {
                    CommunicationOptionID = ( int )EnumCommunicationOption.sms,
                    Name = "SMS"
                } );
                g_UnitOfWork.CommunicationOptions.Insert( new CommunicationOption {
                    CommunicationOptionID = ( int )EnumCommunicationOption.email,
                    Name = "Email"
                } );
                g_UnitOfWork.CommunicationOptions.Insert(new CommunicationOption {
                    CommunicationOptionID = (int)EnumCommunicationOption.sms_and_email,
                    Name = "SMS and Email"
                });
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }

        public static bool clearData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.CommunicationOptions.DeleteAll();
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }
    }
}