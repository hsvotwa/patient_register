﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientRegister.Models {
    [Table(SqlTable.PRESCRIPTION_MEDICATION)]
    public class PrescriptionMedication : HasCreatedAndLastModified {
        #region Member Variables
        [Key]
        public Guid PrescriptionMedicationID { get; set; }

        [ForeignKey("Prescription")]
        public Guid PrescriptionID { get; set; }

        [ForeignKey("MedicalStockItem")]
        public Guid MedicalStockItemID { get; set; }

        public decimal Quantity { get; set; }

        [ForeignKey("MeasurementUnit")]
        public int MeasurementUnitID { get; set; }

        public decimal TimeLength { get; set; }

        [ForeignKey("TimeUnit")]
        public int TimeUnitID { get; set; }

        [Required] public int Deleted { get; set; }

        public virtual Prescription Prescription { get; set; }
        public virtual MeasurementUnit MeasurementUnit { get; set; }
        public virtual TimeUnit TimeUnit { get; set; }
        public virtual MedicalStockItem MedicalStockItem { get; set; }
        #endregion
    }
}