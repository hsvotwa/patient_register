﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PatientRegister.Repositories;

namespace PatientRegister.Models {
    [Table( SqlTable.LU_STAFF_TYPE )]
    public class StaffType : HasCreatedAndLastModified {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.None )]
        public int StaffTypeID { get; set; }

        [Required, MaxLength( 100 )]
        public string Name { get; set; }

        public static bool initData(UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.StaffTypes.Insert( new StaffType {
                    StaffTypeID = (int)EnumStaffType.administrator,
                    Name = "Administrator",
                } );
                g_UnitOfWork.StaffTypes.Insert( new StaffType {
                    StaffTypeID = (int)EnumStaffType.doctor,
                    Name = "Doctor",
                } );
                g_UnitOfWork.StaffTypes.Insert( new StaffType {
                    StaffTypeID = (int)EnumStaffType.doctor_and_surgeon,
                    Name = "Doctor/Surgeon",
                } );
                g_UnitOfWork.StaffTypes.Insert(new StaffType {
                    StaffTypeID = (int)EnumStaffType.anaesthetist,
                    Name = "Anaesthetist",
                });
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }

        public static bool clearData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.StaffTypes.DeleteAll();
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }
    }
}