﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientRegister.Models {
    [Table( SqlTable.DIAGNOSIS )]
    public class Diagnosis : HasCreatedAndLastModified {
        #region Member Variables
        [Key] public Guid DiagnosisID { get; set; }
        [Required] public string Name { get; set; }

        [ForeignKey("Status")]
        [Required] public int StatusID { get; set; }

        public virtual Status Status { get; set; }
        #endregion
    }
}