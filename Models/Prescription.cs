﻿using PatientRegister.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace PatientRegister.Models {
    [Table(SqlTable.PRESCRIPTION)]
    public class Prescription : HasCreatedAndLastModified {
        #region Public Properties
        [Key] public Guid PrescriptionID { get; set; }

        [ForeignKey("Consultation")]
        public Guid? ConsultationID { get; set; }

        [ForeignKey("Patient")]
        [Required] public Guid PatientID { get; set; }

        [ForeignKey("Doctor")]
        [Required] public Guid DoctorID { get; set; }

        [Column(TypeName = "DateTime2")]
        [Required] public DateTime Date { get; set; }
        public virtual Consultation Consultation { get; set; }
        public virtual Patient Patient { get; set; }
        public virtual UserClass Doctor { get; set; }
        #endregion

        public IEnumerable<PrescriptionMedication> getMedications() {
            if (this.ConsultationID == Guid.Empty) {
                return Enumerable.Empty<PrescriptionMedication>();
            }
            UnitOfWork g_UnitOfWork = new UnitOfWork(new PatientRegisterContext());
            return g_UnitOfWork.PrescriptionMedications.GetFiltered(_Rec => _Rec.PrescriptionID == this.PrescriptionID && _Rec.Deleted == (int)EnumYesNo.no);
        }
    }
}