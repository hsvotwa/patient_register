﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientRegister.Models {
    [Table(SqlTable.USER_ROLE_ACCESS)]
    public class UserRoleAccess : HasCreatedAndLastModified {
        #region Member Variables
        [Key]
        public Guid UserRoleAccessID { get; set; }

        [ForeignKey("User")]
        public Guid UserID { get; set; }

        [ForeignKey("UserRole")]
        public int UserRoleID { get; set; }

        [ForeignKey("AccessLevel")]
        public int AccessLevelID { get; set; }

        public virtual UserClass User { get; set; }
        public virtual UserRole UserRole { get; set; }
        public virtual AccessLevel AccessLevel { get; set; }
        #endregion
    }
}