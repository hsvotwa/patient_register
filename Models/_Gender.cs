﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PatientRegister.Repositories;

namespace PatientRegister.Models {
    [Table( SqlTable.LU_GENDER )]
    public class Gender : HasCreatedAndLastModified {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.None )]
        public int GenderID { get; set; }

        [Required, MaxLength( 100 )]
        public string Name { get; set; }

        public static bool initData(UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.Genders.Insert( new Gender {
                    GenderID = ( int )EnumGender.not_set,
                    Name = "Not set"
                } );
                g_UnitOfWork.Genders.Insert( new Gender {
                    GenderID = ( int )EnumGender.male,
                    Name = "Male"
                } );
                g_UnitOfWork.Genders.Insert( new Gender {
                    GenderID = ( int )EnumGender.female,
                    Name = "Female"
                } );
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }

        public static bool clearData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.Genders.DeleteAll();
                return g_UnitOfWork.Commit();
            } catch {
            }
            return false;
        }
    }
}