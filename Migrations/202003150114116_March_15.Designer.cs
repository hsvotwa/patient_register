﻿// <auto-generated />
namespace PatientRegister.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class March_15 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(March_15));
        
        string IMigrationMetadata.Id
        {
            get { return "202003150114116_March_15"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
