﻿namespace PatientRegister.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _15_March_2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.tbl_prescription_nedication", "TimeLength", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.tbl_prescription_nedication", "Deleted", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.tbl_prescription_nedication", "Deleted");
            DropColumn("dbo.tbl_prescription_nedication", "TimeLength");
        }
    }
}
