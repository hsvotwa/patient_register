﻿namespace PatientRegister.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DemoFixes_2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.tbl_lu_configuration", "Value", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.tbl_lu_configuration", "Value");
        }
    }
}
