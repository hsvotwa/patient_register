﻿namespace PatientRegister.Migrations {
    using System;
    using System.Data.Entity.Migrations;

    public partial class Initial : DbMigration {
        public override void Up() {
            CreateTable(
                "dbo.tbl_lu_access_level",
                c => new {
                    AccessLevelID = c.Int(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                })
                .PrimaryKey(t => t.AccessLevelID);

            CreateTable(
                "dbo.tbl_branch",
                c => new {
                    BranchID = c.Guid(nullable: false),
                    Name = c.String(nullable: false),
                    StatusID = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.BranchID)
                .ForeignKey("dbo.tbl_lu_status", t => t.StatusID)
                .Index(t => t.StatusID);

            CreateTable(
                "dbo.tbl_lu_status",
                c => new {
                    StatusID = c.Int(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                })
                .PrimaryKey(t => t.StatusID);

            CreateTable(
                "dbo.tbl_lu_communication_option",
                c => new {
                    GenderID = c.Int(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                })
                .PrimaryKey(t => t.GenderID);

            CreateTable(
                "dbo.tbl_lu_configuration",
                c => new {
                    ConfigurationID = c.Int(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                })
                .PrimaryKey(t => t.ConfigurationID);

            CreateTable(
                "dbo.tbl_consultation_diagnosis",
                c => new {
                    ConsultationMedicationID = c.Guid(nullable: false),
                    ConsultationID = c.Guid(nullable: false),
                    Diagnosis_DiagnosisID = c.Guid(),
                })
                .PrimaryKey(t => t.ConsultationMedicationID)
                .ForeignKey("dbo.tbl_consultation", t => t.ConsultationID)
                .ForeignKey("dbo.tbl_diagnosis", t => t.Diagnosis_DiagnosisID)
                .Index(t => t.ConsultationID)
                .Index(t => t.Diagnosis_DiagnosisID);

            CreateTable(
                "dbo.tbl_consultation",
                c => new {
                    ConsultationID = c.Guid(nullable: false),
                    PatientID = c.Guid(nullable: false),
                    DoctorID = c.Guid(nullable: false),
                    Date = c.DateTime(nullable: false),
                    Temperature = c.Decimal(nullable: false, precision: 18, scale: 2),
                    BloodPressure = c.Decimal(nullable: false, precision: 18, scale: 2),
                    Weight = c.Decimal(nullable: false, precision: 18, scale: 2),
                    NextVisitDate = c.DateTime(nullable: false),
                    SummaryComment = c.String(nullable: false),
                    DetailedComment = c.String(nullable: false),
                })
                .PrimaryKey(t => t.ConsultationID)
                .ForeignKey("dbo.tbl_user", t => t.DoctorID)
                .ForeignKey("dbo.tbl_patient", t => t.PatientID)
                .Index(t => t.PatientID)
                .Index(t => t.DoctorID);

            CreateTable(
                "dbo.tbl_user",
                c => new {
                    UserID = c.Guid(nullable: false),
                    BranchID = c.Guid(nullable: false),
                    StatusID = c.Int(nullable: false),
                    Name = c.String(nullable: false),
                    Surname = c.String(nullable: false),
                    Email = c.String(nullable: false),
                    Password = c.String(nullable: false),
                    StaffTypeID = c.Int(nullable: false),
                    Discriminator = c.String(nullable: false, maxLength: 128),
                })
                .PrimaryKey(t => t.UserID)
                .ForeignKey("dbo.tbl_branch", t => t.BranchID)
                .ForeignKey("dbo.tbl_lu_staff_type", t => t.StaffTypeID)
                .ForeignKey("dbo.tbl_lu_status", t => t.StatusID)
                .Index(t => t.BranchID)
                .Index(t => t.StatusID)
                .Index(t => t.StaffTypeID);

            CreateTable(
                "dbo.tbl_lu_staff_type",
                c => new {
                    StaffTypeID = c.Int(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                })
                .PrimaryKey(t => t.StaffTypeID);

            CreateTable(
                "dbo.tbl_patient",
                c => new {
                    PatientEmpId = c.Guid(nullable: false),
                    Name = c.String(),
                    Surname = c.String(),
                    DateOfBirth = c.DateTime(nullable: false),
                    IDNumber = c.String(),
                    PostalAddress = c.String(),
                    ResidentialAddress = c.String(),
                    HomeTel = c.String(),
                    WorkTel = c.String(),
                    CellNo = c.String(),
                    Email = c.String(),
                    Occupation = c.String(),
                    Employer = c.String(),
                    MedicalAidName = c.String(),
                    MedicalAidMainMemberName = c.String(),
                    MedicalAidMainMemberIDNo = c.String(),
                    MedicalAidNo = c.String(),
                    MedicalAidMainMemberOccupation = c.String(),
                    MedicalAidMainMemberPhone = c.String(),
                    MedicalAidMainMemberRelationshipID = c.Int(nullable: false),
                    EmergencyContactName = c.String(),
                    EmergencyContactPhone = c.String(),
                    EmergencyContactRelationship = c.Int(nullable: false),
                    CommunicationOptionID = c.Int(nullable: false),
                    StatusID = c.Int(nullable: false),
                    GenderID = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.PatientEmpId)
                .ForeignKey("dbo.tbl_lu_communication_option", t => t.CommunicationOptionID)
                .ForeignKey("dbo.tbl_lu_gender", t => t.GenderID)
                .ForeignKey("dbo.tbl_lu_person_relationship", t => t.EmergencyContactRelationship)
                .ForeignKey("dbo.tbl_lu_person_relationship", t => t.MedicalAidMainMemberRelationshipID)
                .ForeignKey("dbo.tbl_lu_status", t => t.StatusID)
                .Index(t => t.MedicalAidMainMemberRelationshipID)
                .Index(t => t.EmergencyContactRelationship)
                .Index(t => t.CommunicationOptionID)
                .Index(t => t.StatusID)
                .Index(t => t.GenderID);

            CreateTable(
                "dbo.tbl_lu_gender",
                c => new {
                    GenderID = c.Int(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                })
                .PrimaryKey(t => t.GenderID);

            CreateTable(
                "dbo.tbl_lu_person_relationship",
                c => new {
                    PersonRelationshipID = c.Int(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                })
                .PrimaryKey(t => t.PersonRelationshipID);

            CreateTable(
                "dbo.tbl_diagnosis",
                c => new {
                    DiagnosisID = c.Guid(nullable: false),
                    Name = c.String(nullable: false),
                    StatusID = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.DiagnosisID)
                .ForeignKey("dbo.tbl_lu_status", t => t.StatusID)
                .Index(t => t.StatusID);

            CreateTable(
                "dbo.tbl_consultation_medication",
                c => new {
                    ConsultationMedicationID = c.Guid(nullable: false),
                    ConsultationID = c.Guid(nullable: false),
                    MedicalStockItemID = c.Guid(nullable: false),
                    MeasurementUnitID = c.Int(nullable: false),
                    TimeUnitID = c.Int(nullable: false),
                    Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                })
                .PrimaryKey(t => t.ConsultationMedicationID)
                .ForeignKey("dbo.tbl_consultation", t => t.ConsultationID)
                .ForeignKey("dbo.tbl_lu_measurement_unit", t => t.MeasurementUnitID)
                .ForeignKey("dbo.tbl_medical_stock_item", t => t.MedicalStockItemID)
                .ForeignKey("dbo.tbl_lu_time_unit", t => t.TimeUnitID)
                .Index(t => t.ConsultationID)
                .Index(t => t.MedicalStockItemID)
                .Index(t => t.MeasurementUnitID)
                .Index(t => t.TimeUnitID);

            CreateTable(
                "dbo.tbl_lu_measurement_unit",
                c => new {
                    MeasurementID = c.Int(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                })
                .PrimaryKey(t => t.MeasurementID);

            CreateTable(
                "dbo.tbl_medical_stock_item",
                c => new {
                    MedicalStockItemID = c.Guid(nullable: false),
                    Name = c.String(nullable: false),
                    StatusID = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.MedicalStockItemID)
                .ForeignKey("dbo.tbl_lu_status", t => t.StatusID)
                .Index(t => t.StatusID);

            CreateTable(
                "dbo.tbl_lu_time_unit",
                c => new {
                    TimeUnitID = c.Int(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                })
                .PrimaryKey(t => t.TimeUnitID);

            CreateTable(
                "dbo.tbl_consultation_symptom",
                c => new {
                    ConsutationSymptomID = c.Guid(nullable: false),
                    ConsultationID = c.Guid(nullable: false),
                    Diagnosis_DiagnosisID = c.Guid(),
                })
                .PrimaryKey(t => t.ConsutationSymptomID)
                .ForeignKey("dbo.tbl_consultation", t => t.ConsultationID)
                .ForeignKey("dbo.tbl_diagnosis", t => t.Diagnosis_DiagnosisID)
                .Index(t => t.ConsultationID)
                .Index(t => t.Diagnosis_DiagnosisID);

            CreateTable(
                "dbo.tbl_lu_file_type",
                c => new {
                    FileTypeID = c.Int(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                })
                .PrimaryKey(t => t.FileTypeID);

            CreateTable(
                "dbo.tbl_file",
                c => new {
                    FileUploadID = c.Guid(nullable: false),
                    LinkID = c.Guid(nullable: false),
                    ServerName = c.String(nullable: false),
                    GivenName = c.String(nullable: false),
                    FileTypeID = c.Int(nullable: false),
                    StatusID = c.Int(nullable: false),
                    UserId = c.Guid(nullable: false),
                })
                .PrimaryKey(t => t.FileUploadID)
                .ForeignKey("dbo.tbl_lu_file_type", t => t.FileTypeID)
                .ForeignKey("dbo.tbl_lu_status", t => t.StatusID)
                .ForeignKey("dbo.tbl_user", t => t.UserId)
                .Index(t => t.FileTypeID)
                .Index(t => t.StatusID)
                .Index(t => t.UserId);

            CreateTable(
                "dbo.tbl_medical_aid_company",
                c => new {
                    MedicalAidCompanyID = c.Guid(nullable: false),
                    Name = c.String(nullable: false),
                    StatusID = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.MedicalAidCompanyID)
                .ForeignKey("dbo.tbl_lu_status", t => t.StatusID)
                .Index(t => t.StatusID);

            CreateTable(
                "dbo.tbl_lu_notification_side",
                c => new {
                    NotificationSideID = c.Int(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                })
                .PrimaryKey(t => t.NotificationSideID);

            CreateTable(
                "dbo.tbl_lu_notification_type",
                c => new {
                    NotificationTypeID = c.Int(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                })
                .PrimaryKey(t => t.NotificationTypeID);

            CreateTable(
                "dbo.tbl_operation",
                c => new {
                    OperationID = c.Guid(nullable: false),
                    ConsultationID = c.Guid(nullable: false),
                    PatientID = c.Guid(nullable: false),
                    DoctorID = c.Guid(nullable: false),
                    Date = c.DateTime(nullable: false),
                    Premedication = c.String(nullable: false),
                    OperationName = c.String(nullable: false),
                    StartTime = c.DateTime(nullable: false),
                    EndTime = c.DateTime(nullable: false),
                    Anaesthetic = c.String(),
                    Remarks = c.String(nullable: false),
                    SurgeonID = c.Guid(nullable: false),
                    AssistantSurgeonID = c.Guid(nullable: false),
                    AnaesthetistID = c.Guid(nullable: false),
                })
                .PrimaryKey(t => t.OperationID)
                .ForeignKey("dbo.tbl_user", t => t.AnaesthetistID)
                .ForeignKey("dbo.tbl_user", t => t.AssistantSurgeonID)
                .ForeignKey("dbo.tbl_consultation", t => t.ConsultationID)
                .ForeignKey("dbo.tbl_user", t => t.DoctorID)
                .ForeignKey("dbo.tbl_patient", t => t.PatientID)
                .ForeignKey("dbo.tbl_user", t => t.SurgeonID)
                .Index(t => t.ConsultationID)
                .Index(t => t.PatientID)
                .Index(t => t.DoctorID)
                .Index(t => t.SurgeonID)
                .Index(t => t.AssistantSurgeonID)
                .Index(t => t.AnaesthetistID);

            CreateTable(
                "dbo.tbl_prescription_nedication",
                c => new {
                    PrescriptionMedicationID = c.Guid(nullable: false),
                    PrescriptionID = c.Guid(nullable: false),
                    MedicalStockItemID = c.Guid(nullable: false),
                    MeasurementUnitID = c.Int(nullable: false),
                    TimeUnitID = c.Int(nullable: false),
                    Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                })
                .PrimaryKey(t => t.PrescriptionMedicationID)
                .ForeignKey("dbo.tbl_lu_measurement_unit", t => t.MeasurementUnitID)
                .ForeignKey("dbo.tbl_medical_stock_item", t => t.MedicalStockItemID)
                .ForeignKey("dbo.tbl_prescription", t => t.PrescriptionID)
                .ForeignKey("dbo.tbl_lu_time_unit", t => t.TimeUnitID)
                .Index(t => t.PrescriptionID)
                .Index(t => t.MedicalStockItemID)
                .Index(t => t.MeasurementUnitID)
                .Index(t => t.TimeUnitID);

            CreateTable(
                "dbo.tbl_prescription",
                c => new {
                    PrescriptionID = c.Guid(nullable: false),
                    ConsultationID = c.Guid(nullable: false),
                    PatientID = c.Guid(nullable: false),
                    DoctorID = c.Guid(nullable: false),
                    Date = c.DateTime(nullable: false),
                })
                .PrimaryKey(t => t.PrescriptionID)
                .ForeignKey("dbo.tbl_consultation", t => t.ConsultationID)
                .ForeignKey("dbo.tbl_user", t => t.DoctorID)
                .ForeignKey("dbo.tbl_patient", t => t.PatientID)
                .Index(t => t.ConsultationID)
                .Index(t => t.PatientID)
                .Index(t => t.DoctorID);

            CreateTable(
                "dbo.tbl_referral",
                c => new {
                    ReferralID = c.Guid(nullable: false),
                    ConsultationID = c.Guid(nullable: false),
                    PatientID = c.Guid(nullable: false),
                    DoctorID = c.Guid(nullable: false),
                    Date = c.DateTime(nullable: false),
                    ReferredToDoctor = c.String(nullable: false),
                    CurrentMedication = c.String(),
                    History = c.String(nullable: false),
                    ExaminationAndInvestigation = c.String(nullable: false),
                })
                .PrimaryKey(t => t.ReferralID)
                .ForeignKey("dbo.tbl_consultation", t => t.ConsultationID)
                .ForeignKey("dbo.tbl_user", t => t.DoctorID)
                .ForeignKey("dbo.tbl_patient", t => t.PatientID)
                .Index(t => t.ConsultationID)
                .Index(t => t.PatientID)
                .Index(t => t.DoctorID);

            CreateTable(
                "dbo.tbl_lu_response_status",
                c => new {
                    ResponseStatusID = c.Int(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                })
                .PrimaryKey(t => t.ResponseStatusID);

            CreateTable(
                "dbo.tbl_symptom",
                c => new {
                    SymptomID = c.Guid(nullable: false),
                    Name = c.String(nullable: false),
                    StatusID = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.SymptomID)
                .ForeignKey("dbo.tbl_lu_status", t => t.StatusID)
                .Index(t => t.StatusID);

            CreateTable(
                "dbo.tbl_user_role",
                c => new {
                    UserRoleID = c.Guid(nullable: false),
                    UserID = c.Guid(nullable: false),
                    UserRoleAccessID = c.Int(nullable: false),
                    AccessLevelID = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.UserRoleID)
                .ForeignKey("dbo.tbl_lu_access_level", t => t.AccessLevelID)
                .ForeignKey("dbo.tbl_user", t => t.UserID)
                .ForeignKey("dbo.tbl_lu_user_role", t => t.UserRoleAccessID)
                .Index(t => t.UserID)
                .Index(t => t.UserRoleAccessID)
                .Index(t => t.AccessLevelID);

            CreateTable(
                "dbo.tbl_lu_user_role",
                c => new {
                    UserRoleID = c.Int(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                })
                .PrimaryKey(t => t.UserRoleID);

            CreateTable(
                "dbo.tbl_lu_yes_no",
                c => new {
                    YesNoID = c.Int(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                })
                .PrimaryKey(t => t.YesNoID);

        }

        public override void Down() {
            DropForeignKey("dbo.tbl_user_role", "UserRoleAccessID", "dbo.tbl_lu_user_role");
            DropForeignKey("dbo.tbl_user_role", "UserID", "dbo.tbl_user");
            DropForeignKey("dbo.tbl_user_role", "AccessLevelID", "dbo.tbl_lu_access_level");
            DropForeignKey("dbo.tbl_symptom", "StatusID", "dbo.tbl_lu_status");
            DropForeignKey("dbo.tbl_referral", "PatientID", "dbo.tbl_patient");
            DropForeignKey("dbo.tbl_referral", "DoctorID", "dbo.tbl_user");
            DropForeignKey("dbo.tbl_referral", "ConsultationID", "dbo.tbl_consultation");
            DropForeignKey("dbo.tbl_prescription_nedication", "TimeUnitID", "dbo.tbl_lu_time_unit");
            DropForeignKey("dbo.tbl_prescription_nedication", "PrescriptionID", "dbo.tbl_prescription");
            DropForeignKey("dbo.tbl_prescription", "PatientID", "dbo.tbl_patient");
            DropForeignKey("dbo.tbl_prescription", "DoctorID", "dbo.tbl_user");
            DropForeignKey("dbo.tbl_prescription", "ConsultationID", "dbo.tbl_consultation");
            DropForeignKey("dbo.tbl_prescription_nedication", "MedicalStockItemID", "dbo.tbl_medical_stock_item");
            DropForeignKey("dbo.tbl_prescription_nedication", "MeasurementUnitID", "dbo.tbl_lu_measurement_unit");
            DropForeignKey("dbo.tbl_operation", "SurgeonID", "dbo.tbl_user");
            DropForeignKey("dbo.tbl_operation", "PatientID", "dbo.tbl_patient");
            DropForeignKey("dbo.tbl_operation", "DoctorID", "dbo.tbl_user");
            DropForeignKey("dbo.tbl_operation", "ConsultationID", "dbo.tbl_consultation");
            DropForeignKey("dbo.tbl_operation", "AssistantSurgeonID", "dbo.tbl_user");
            DropForeignKey("dbo.tbl_operation", "AnaesthetistID", "dbo.tbl_user");
            DropForeignKey("dbo.tbl_medical_aid_company", "StatusID", "dbo.tbl_lu_status");
            DropForeignKey("dbo.tbl_file", "UserId", "dbo.tbl_user");
            DropForeignKey("dbo.tbl_file", "StatusID", "dbo.tbl_lu_status");
            DropForeignKey("dbo.tbl_file", "FileTypeID", "dbo.tbl_lu_file_type");
            DropForeignKey("dbo.tbl_consultation_symptom", "Diagnosis_DiagnosisID", "dbo.tbl_diagnosis");
            DropForeignKey("dbo.tbl_consultation_symptom", "ConsultationID", "dbo.tbl_consultation");
            DropForeignKey("dbo.tbl_consultation_medication", "TimeUnitID", "dbo.tbl_lu_time_unit");
            DropForeignKey("dbo.tbl_consultation_medication", "MedicalStockItemID", "dbo.tbl_medical_stock_item");
            DropForeignKey("dbo.tbl_medical_stock_item", "StatusID", "dbo.tbl_lu_status");
            DropForeignKey("dbo.tbl_consultation_medication", "MeasurementUnitID", "dbo.tbl_lu_measurement_unit");
            DropForeignKey("dbo.tbl_consultation_medication", "ConsultationID", "dbo.tbl_consultation");
            DropForeignKey("dbo.tbl_consultation_diagnosis", "Diagnosis_DiagnosisID", "dbo.tbl_diagnosis");
            DropForeignKey("dbo.tbl_diagnosis", "StatusID", "dbo.tbl_lu_status");
            DropForeignKey("dbo.tbl_consultation_diagnosis", "ConsultationID", "dbo.tbl_consultation");
            DropForeignKey("dbo.tbl_consultation", "PatientID", "dbo.tbl_patient");
            DropForeignKey("dbo.tbl_patient", "StatusID", "dbo.tbl_lu_status");
            DropForeignKey("dbo.tbl_patient", "MedicalAidMainMemberRelationshipID", "dbo.tbl_lu_person_relationship");
            DropForeignKey("dbo.tbl_patient", "EmergencyContactRelationship", "dbo.tbl_lu_person_relationship");
            DropForeignKey("dbo.tbl_patient", "GenderID", "dbo.tbl_lu_gender");
            DropForeignKey("dbo.tbl_patient", "CommunicationOptionID", "dbo.tbl_lu_communication_option");
            DropForeignKey("dbo.tbl_consultation", "DoctorID", "dbo.tbl_user");
            DropForeignKey("dbo.tbl_user", "StatusID", "dbo.tbl_lu_status");
            DropForeignKey("dbo.tbl_user", "StaffTypeID", "dbo.tbl_lu_staff_type");
            DropForeignKey("dbo.tbl_user", "BranchID", "dbo.tbl_branch");
            DropForeignKey("dbo.tbl_branch", "StatusID", "dbo.tbl_lu_status");
            DropIndex("dbo.tbl_user_role", new[] { "AccessLevelID" });
            DropIndex("dbo.tbl_user_role", new[] { "UserRoleAccessID" });
            DropIndex("dbo.tbl_user_role", new[] { "UserID" });
            DropIndex("dbo.tbl_symptom", new[] { "StatusID" });
            DropIndex("dbo.tbl_referral", new[] { "DoctorID" });
            DropIndex("dbo.tbl_referral", new[] { "PatientID" });
            DropIndex("dbo.tbl_referral", new[] { "ConsultationID" });
            DropIndex("dbo.tbl_prescription", new[] { "DoctorID" });
            DropIndex("dbo.tbl_prescription", new[] { "PatientID" });
            DropIndex("dbo.tbl_prescription", new[] { "ConsultationID" });
            DropIndex("dbo.tbl_prescription_nedication", new[] { "TimeUnitID" });
            DropIndex("dbo.tbl_prescription_nedication", new[] { "MeasurementUnitID" });
            DropIndex("dbo.tbl_prescription_nedication", new[] { "MedicalStockItemID" });
            DropIndex("dbo.tbl_prescription_nedication", new[] { "PrescriptionID" });
            DropIndex("dbo.tbl_operation", new[] { "AnaesthetistID" });
            DropIndex("dbo.tbl_operation", new[] { "AssistantSurgeonID" });
            DropIndex("dbo.tbl_operation", new[] { "SurgeonID" });
            DropIndex("dbo.tbl_operation", new[] { "DoctorID" });
            DropIndex("dbo.tbl_operation", new[] { "PatientID" });
            DropIndex("dbo.tbl_operation", new[] { "ConsultationID" });
            DropIndex("dbo.tbl_medical_aid_company", new[] { "StatusID" });
            DropIndex("dbo.tbl_file", new[] { "UserId" });
            DropIndex("dbo.tbl_file", new[] { "StatusID" });
            DropIndex("dbo.tbl_file", new[] { "FileTypeID" });
            DropIndex("dbo.tbl_consultation_symptom", new[] { "Diagnosis_DiagnosisID" });
            DropIndex("dbo.tbl_consultation_symptom", new[] { "ConsultationID" });
            DropIndex("dbo.tbl_medical_stock_item", new[] { "StatusID" });
            DropIndex("dbo.tbl_consultation_medication", new[] { "TimeUnitID" });
            DropIndex("dbo.tbl_consultation_medication", new[] { "MeasurementUnitID" });
            DropIndex("dbo.tbl_consultation_medication", new[] { "MedicalStockItemID" });
            DropIndex("dbo.tbl_consultation_medication", new[] { "ConsultationID" });
            DropIndex("dbo.tbl_diagnosis", new[] { "StatusID" });
            DropIndex("dbo.tbl_patient", new[] { "GenderID" });
            DropIndex("dbo.tbl_patient", new[] { "StatusID" });
            DropIndex("dbo.tbl_patient", new[] { "CommunicationOptionID" });
            DropIndex("dbo.tbl_patient", new[] { "EmergencyContactRelationship" });
            DropIndex("dbo.tbl_patient", new[] { "MedicalAidMainMemberRelationshipID" });
            DropIndex("dbo.tbl_user", new[] { "StaffTypeID" });
            DropIndex("dbo.tbl_user", new[] { "StatusID" });
            DropIndex("dbo.tbl_user", new[] { "BranchID" });
            DropIndex("dbo.tbl_consultation", new[] { "DoctorID" });
            DropIndex("dbo.tbl_consultation", new[] { "PatientID" });
            DropIndex("dbo.tbl_consultation_diagnosis", new[] { "Diagnosis_DiagnosisID" });
            DropIndex("dbo.tbl_consultation_diagnosis", new[] { "ConsultationID" });
            DropIndex("dbo.tbl_branch", new[] { "StatusID" });
            DropTable("dbo.tbl_lu_yes_no");
            DropTable("dbo.tbl_lu_user_role");
            DropTable("dbo.tbl_user_role");
            DropTable("dbo.tbl_symptom");
            DropTable("dbo.tbl_lu_response_status");
            DropTable("dbo.tbl_referral");
            DropTable("dbo.tbl_prescription");
            DropTable("dbo.tbl_prescription_nedication");
            DropTable("dbo.tbl_operation");
            DropTable("dbo.tbl_lu_notification_type");
            DropTable("dbo.tbl_lu_notification_side");
            DropTable("dbo.tbl_medical_aid_company");
            DropTable("dbo.tbl_file");
            DropTable("dbo.tbl_lu_file_type");
            DropTable("dbo.tbl_consultation_symptom");
            DropTable("dbo.tbl_lu_time_unit");
            DropTable("dbo.tbl_medical_stock_item");
            DropTable("dbo.tbl_lu_measurement_unit");
            DropTable("dbo.tbl_consultation_medication");
            DropTable("dbo.tbl_diagnosis");
            DropTable("dbo.tbl_lu_person_relationship");
            DropTable("dbo.tbl_lu_gender");
            DropTable("dbo.tbl_patient");
            DropTable("dbo.tbl_lu_staff_type");
            DropTable("dbo.tbl_user");
            DropTable("dbo.tbl_consultation");
            DropTable("dbo.tbl_consultation_diagnosis");
            DropTable("dbo.tbl_lu_configuration");
            DropTable("dbo.tbl_lu_communication_option");
            DropTable("dbo.tbl_lu_status");
            DropTable("dbo.tbl_branch");
            DropTable("dbo.tbl_lu_access_level");
        }
    }
}
