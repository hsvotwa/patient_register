﻿namespace PatientRegister.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _15_March_3 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.tbl_prescription", new[] { "ConsultationID" });
            AlterColumn("dbo.tbl_prescription", "ConsultationID", c => c.Guid());
            CreateIndex("dbo.tbl_prescription", "ConsultationID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.tbl_prescription", new[] { "ConsultationID" });
            AlterColumn("dbo.tbl_prescription", "ConsultationID", c => c.Guid(nullable: false));
            CreateIndex("dbo.tbl_prescription", "ConsultationID");
        }
    }
}
