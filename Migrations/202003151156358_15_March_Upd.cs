﻿namespace PatientRegister.Migrations {
    using System;
    using System.Data.Entity.Migrations;

    public partial class _15_March_Upd : DbMigration {
        public override void Up() {
            DropIndex("dbo.tbl_operation", new[] { "ConsultationID" });
            AlterColumn("dbo.tbl_operation", "ConsultationID", c => c.Guid());
            CreateIndex("dbo.tbl_operation", "ConsultationID");
        }

        public override void Down() {
            DropIndex("dbo.tbl_operation", new[] { "ConsultationID" });
            AlterColumn("dbo.tbl_operation", "ConsultationID", c => c.Guid(nullable: false));
            CreateIndex("dbo.tbl_operation", "ConsultationID");
        }
    }
}
