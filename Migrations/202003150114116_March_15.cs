﻿namespace PatientRegister.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class March_15 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.tbl_consultation_symptom", "Diagnosis_DiagnosisID", "dbo.tbl_diagnosis");
            DropIndex("dbo.tbl_consultation_diagnosis", new[] { "Diagnosis_DiagnosisID" });
            DropIndex("dbo.tbl_consultation_symptom", new[] { "Diagnosis_DiagnosisID" });
            RenameColumn(table: "dbo.tbl_consultation_diagnosis", name: "Diagnosis_DiagnosisID", newName: "DiagnosisID");
            DropPrimaryKey("dbo.tbl_consultation_diagnosis");
            AddColumn("dbo.tbl_consultation_diagnosis", "ConsultationDiagnosisID", c => c.Guid(nullable: false));
            AddColumn("dbo.tbl_consultation_diagnosis", "Deleted", c => c.Int(nullable: false));
            AddColumn("dbo.tbl_consultation_medication", "TimeLength", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.tbl_consultation_medication", "Deleted", c => c.Int(nullable: false));
            AddColumn("dbo.tbl_consultation_symptom", "SymptomID", c => c.Guid(nullable: false));
            AddColumn("dbo.tbl_consultation_symptom", "Deleted", c => c.Int(nullable: false));
            AlterColumn("dbo.tbl_consultation_diagnosis", "DiagnosisID", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.tbl_consultation_diagnosis", "ConsultationDiagnosisID");
            CreateIndex("dbo.tbl_consultation_diagnosis", "DiagnosisID");
            CreateIndex("dbo.tbl_consultation_symptom", "SymptomID");
            AddForeignKey("dbo.tbl_consultation_symptom", "SymptomID", "dbo.tbl_symptom", "SymptomID");
            DropColumn("dbo.tbl_consultation_diagnosis", "ConsultationMedicationID");
            DropColumn("dbo.tbl_consultation_symptom", "Diagnosis_DiagnosisID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.tbl_consultation_symptom", "Diagnosis_DiagnosisID", c => c.Guid());
            AddColumn("dbo.tbl_consultation_diagnosis", "ConsultationMedicationID", c => c.Guid(nullable: false));
            DropForeignKey("dbo.tbl_consultation_symptom", "SymptomID", "dbo.tbl_symptom");
            DropIndex("dbo.tbl_consultation_symptom", new[] { "SymptomID" });
            DropIndex("dbo.tbl_consultation_diagnosis", new[] { "DiagnosisID" });
            DropPrimaryKey("dbo.tbl_consultation_diagnosis");
            AlterColumn("dbo.tbl_consultation_diagnosis", "DiagnosisID", c => c.Guid());
            DropColumn("dbo.tbl_consultation_symptom", "Deleted");
            DropColumn("dbo.tbl_consultation_symptom", "SymptomID");
            DropColumn("dbo.tbl_consultation_medication", "Deleted");
            DropColumn("dbo.tbl_consultation_medication", "TimeLength");
            DropColumn("dbo.tbl_consultation_diagnosis", "Deleted");
            DropColumn("dbo.tbl_consultation_diagnosis", "ConsultationDiagnosisID");
            AddPrimaryKey("dbo.tbl_consultation_diagnosis", "ConsultationMedicationID");
            RenameColumn(table: "dbo.tbl_consultation_diagnosis", name: "DiagnosisID", newName: "Diagnosis_DiagnosisID");
            CreateIndex("dbo.tbl_consultation_symptom", "Diagnosis_DiagnosisID");
            CreateIndex("dbo.tbl_consultation_diagnosis", "Diagnosis_DiagnosisID");
            AddForeignKey("dbo.tbl_consultation_symptom", "Diagnosis_DiagnosisID", "dbo.tbl_diagnosis", "DiagnosisID");
        }
    }
}
