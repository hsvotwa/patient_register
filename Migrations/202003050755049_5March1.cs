﻿namespace PatientRegister.Migrations {
    using System;
    using System.Data.Entity.Migrations;

    public partial class _5March1 : DbMigration {
        public override void Up() {
            DropForeignKey("dbo.tbl_patient", "CommunicationOptionID", "dbo.tbl_lu_communication_option");
            DropPrimaryKey("dbo.tbl_lu_communication_option");
            AddColumn("dbo.tbl_lu_communication_option", "CommunicationOptionID", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.tbl_lu_communication_option", "CommunicationOptionID");
            AddForeignKey("dbo.tbl_patient", "CommunicationOptionID", "dbo.tbl_lu_communication_option", "CommunicationOptionID");
            DropColumn("dbo.tbl_lu_communication_option", "GenderID");
        }

        public override void Down() {
            AddColumn("dbo.tbl_lu_communication_option", "GenderID", c => c.Int(nullable: false));
            DropForeignKey("dbo.tbl_patient", "CommunicationOptionID", "dbo.tbl_lu_communication_option");
            DropPrimaryKey("dbo.tbl_lu_communication_option");
            DropColumn("dbo.tbl_lu_communication_option", "CommunicationOptionID");
            AddPrimaryKey("dbo.tbl_lu_communication_option", "GenderID");
            AddForeignKey("dbo.tbl_patient", "CommunicationOptionID", "dbo.tbl_lu_communication_option", "GenderID");
        }
    }
}
