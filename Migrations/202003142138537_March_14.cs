﻿namespace PatientRegister.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class March_14 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.tbl_consultation", "Date", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.tbl_consultation", "NextVisitDate", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.tbl_operation", "Date", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.tbl_operation", "StartTime", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.tbl_operation", "EndTime", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.tbl_prescription", "Date", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.tbl_referral", "Date", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.tbl_referral", "Date", c => c.DateTime(nullable: false));
            AlterColumn("dbo.tbl_prescription", "Date", c => c.DateTime(nullable: false));
            AlterColumn("dbo.tbl_operation", "EndTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.tbl_operation", "StartTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.tbl_operation", "Date", c => c.DateTime(nullable: false));
            AlterColumn("dbo.tbl_consultation", "NextVisitDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.tbl_consultation", "Date", c => c.DateTime(nullable: false));
        }
    }
}
