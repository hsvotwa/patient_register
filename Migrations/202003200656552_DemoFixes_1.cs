﻿namespace PatientRegister.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DemoFixes_1 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.tbl_consultation_symptom");
            AddColumn("dbo.tbl_consultation_symptom", "ConsultationSymptomID", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.tbl_consultation_symptom", "ConsultationSymptomID");
            DropColumn("dbo.tbl_consultation_symptom", "ConsutationSymptomID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.tbl_consultation_symptom", "ConsutationSymptomID", c => c.Guid(nullable: false));
            DropPrimaryKey("dbo.tbl_consultation_symptom");
            DropColumn("dbo.tbl_consultation_symptom", "ConsultationSymptomID");
            AddPrimaryKey("dbo.tbl_consultation_symptom", "ConsutationSymptomID");
        }
    }
}
