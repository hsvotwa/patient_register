﻿namespace PatientRegister.Migrations {
    using System;
    using System.Data.Entity.Migrations;

    public partial class _15_March_Upd2 : DbMigration {
        public override void Up() {
            AddColumn("dbo.tbl_referral", "SummaryComment", c => c.String(nullable: false));
        }

        public override void Down() {
            DropColumn("dbo.tbl_referral", "SummaryComment");
        }
    }
}