﻿namespace PatientRegister.Migrations {
    using System;
    using System.Data.Entity.Migrations;

    public partial class _06_March : DbMigration {
        public override void Up() {
            DropForeignKey("dbo.tbl_consultation", "PatientID", "dbo.tbl_patient");
            DropForeignKey("dbo.tbl_operation", "PatientID", "dbo.tbl_patient");
            DropForeignKey("dbo.tbl_prescription", "PatientID", "dbo.tbl_patient");
            DropForeignKey("dbo.tbl_referral", "PatientID", "dbo.tbl_patient");
           // DropIndex("dbo.tbl_user_role", new[] { "UserRoleAccessID" });
            //DropColumn("dbo.tbl_user_role", "UserRoleID");
            //RenameColumn(table: "dbo.tbl_user_role", name: "UserRoleAccessID", newName: "UserRoleID");
            RenameColumn(table: "dbo.tbl_patient", name: "EmergencyContactRelationship", newName: "EmergencyContactRelationshipID");
            RenameIndex(table: "dbo.tbl_patient", name: "IX_EmergencyContactRelationship", newName: "IX_EmergencyContactRelationshipID");
            DropPrimaryKey("dbo.tbl_patient");
            //DropPrimaryKey("dbo.tbl_user_role");
            AddColumn("dbo.tbl_patient", "PatientID", c => c.Guid(nullable: false));
            AddColumn("dbo.tbl_patient", "MedicalAidCompanyID", c => c.Guid(nullable: false));
            //AlterColumn("dbo.tbl_user_role", "UserRoleID", c => c.Int(nullable: false));
            //AlterColumn("dbo.tbl_user_role", "UserRoleAccessID", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.tbl_patient", "PatientID");
           // AddPrimaryKey("dbo.tbl_user_role", "UserRoleAccessID");
            CreateIndex("dbo.tbl_patient", "MedicalAidCompanyID");
            //CreateIndex("dbo.tbl_user_role", "UserRoleID");
            AddForeignKey("dbo.tbl_patient", "MedicalAidCompanyID", "dbo.tbl_medical_aid_company", "MedicalAidCompanyID");
            AddForeignKey("dbo.tbl_consultation", "PatientID", "dbo.tbl_patient", "PatientID");
            AddForeignKey("dbo.tbl_operation", "PatientID", "dbo.tbl_patient", "PatientID");
            AddForeignKey("dbo.tbl_prescription", "PatientID", "dbo.tbl_patient", "PatientID");
            AddForeignKey("dbo.tbl_referral", "PatientID", "dbo.tbl_patient", "PatientID");
            DropColumn("dbo.tbl_patient", "PatientEmpId");
        }

        public override void Down() {
            AddColumn("dbo.tbl_patient", "PatientEmpId", c => c.Guid(nullable: false));
            DropForeignKey("dbo.tbl_referral", "PatientID", "dbo.tbl_patient");
            DropForeignKey("dbo.tbl_prescription", "PatientID", "dbo.tbl_patient");
            DropForeignKey("dbo.tbl_operation", "PatientID", "dbo.tbl_patient");
            DropForeignKey("dbo.tbl_consultation", "PatientID", "dbo.tbl_patient");
            DropForeignKey("dbo.tbl_patient", "MedicalAidCompanyID", "dbo.tbl_medical_aid_company");
            //DropIndex("dbo.tbl_user_role", new[] { "UserRoleID" });
            DropIndex("dbo.tbl_patient", new[] { "MedicalAidCompanyID" });
            //DropPrimaryKey("dbo.tbl_user_role");
            DropPrimaryKey("dbo.tbl_patient");
            // AlterColumn("dbo.tbl_user_role", "UserRoleAccessID", c => c.Int(nullable: false));
            //AlterColumn("dbo.tbl_user_role", "UserRoleID", c => c.Guid(nullable: false));
            DropColumn("dbo.tbl_patient", "MedicalAidCompanyID");
            DropColumn("dbo.tbl_patient", "PatientID");
            //AddPrimaryKey("dbo.tbl_user_role", "UserRoleID");
            AddPrimaryKey("dbo.tbl_patient", "PatientEmpId");
            RenameIndex(table: "dbo.tbl_patient", name: "IX_EmergencyContactRelationshipID", newName: "IX_EmergencyContactRelationship");
            RenameColumn(table: "dbo.tbl_patient", name: "EmergencyContactRelationshipID", newName: "EmergencyContactRelationship");
            //RenameColumn(table: "dbo.tbl_user_role", name: "UserRoleID", newName: "UserRoleAccessID");
            // AddColumn("dbo.tbl_user_role", "UserRoleID", c => c.Guid(nullable: false));
            // CreateIndex("dbo.tbl_user_role", "UserRoleAccessID");
            AddForeignKey("dbo.tbl_referral", "PatientID", "dbo.tbl_patient", "PatientEmpId");
            AddForeignKey("dbo.tbl_prescription", "PatientID", "dbo.tbl_patient", "PatientEmpId");
            AddForeignKey("dbo.tbl_operation", "PatientID", "dbo.tbl_patient", "PatientEmpId");
            AddForeignKey("dbo.tbl_consultation", "PatientID", "dbo.tbl_patient", "PatientEmpId");
        }
    }
}
