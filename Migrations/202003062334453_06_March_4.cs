﻿namespace PatientRegister.Migrations {
    using System;
    using System.Data.Entity.Migrations;

    public partial class _06_March_4 : DbMigration {
        public override void Up() {
            AddColumn("dbo.tbl_patient", "PatientNo", c => c.Int(nullable: false));
        }

        public override void Down() {
            DropColumn("dbo.tbl_patient", "PatientNo");
        }
    }
}
