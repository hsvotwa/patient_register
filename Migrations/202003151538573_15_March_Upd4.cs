﻿namespace PatientRegister.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _15_March_Upd4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.tbl_file", "Deleted", c => c.Int(nullable: false));
            AddColumn("dbo.tbl_file", "DateUploaded", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.tbl_file", "DateUploaded");
            DropColumn("dbo.tbl_file", "Deleted");
        }
    }
}
