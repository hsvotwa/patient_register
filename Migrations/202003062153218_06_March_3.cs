﻿namespace PatientRegister.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _06_March_3 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.tbl_patient", new[] { "MedicalAidCompanyID" });
            AlterColumn("dbo.tbl_patient", "MedicalAidCompanyID", c => c.Guid());
            CreateIndex("dbo.tbl_patient", "MedicalAidCompanyID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.tbl_patient", new[] { "MedicalAidCompanyID" });
            AlterColumn("dbo.tbl_patient", "MedicalAidCompanyID", c => c.Guid(nullable: false));
            CreateIndex("dbo.tbl_patient", "MedicalAidCompanyID");
        }
    }
}
