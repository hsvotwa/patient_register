﻿namespace PatientRegister.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _06_March_2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.tbl_patient", "DateOfBirth", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.tbl_patient", "DateOfBirth", c => c.DateTime(nullable: false));
        }
    }
}
