﻿namespace PatientRegister.Migrations {
    using System;
    using System.Data.Entity.Migrations;

    public partial class _15_March_Upd1 : DbMigration {
        public override void Up() {
            DropIndex("dbo.tbl_referral", new[] { "ConsultationID" });
            AlterColumn("dbo.tbl_referral", "ConsultationID", c => c.Guid());
            CreateIndex("dbo.tbl_referral", "ConsultationID");
        }

        public override void Down() {
            DropIndex("dbo.tbl_referral", new[] { "ConsultationID" });
            AlterColumn("dbo.tbl_referral", "ConsultationID", c => c.Guid(nullable: false));
            CreateIndex("dbo.tbl_referral", "ConsultationID");
        }
    }
}
