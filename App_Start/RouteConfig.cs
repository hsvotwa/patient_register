﻿using System.Web.Mvc;
using System.Web.Routing;

namespace PatientRegister {
    public class RouteConfig {
        public static void RegisterRoutes(RouteCollection routes) {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
               name: "First",
               url: "{controller}/{action}",
                defaults: new { controller = "Dashboard", action = "Index" }
           );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{_sGuid}",
                defaults: new { controller = "Dashboard", action = "Index", _sGuid = UrlParameter.Optional }
            );
        }
    }
}