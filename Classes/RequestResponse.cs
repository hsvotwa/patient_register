﻿using System;
namespace PatientRegister.Classes {
    public class RequestResponse {
        #region Member Variables
        protected EnumResponseStatus _ResponseStatus;
        protected string _sDescription;
        protected string _sJsFunc;
        #endregion

        #region Public Properties
        public EnumResponseStatus ResponseStatus { get; set; }
        public string Description { get; set; }
        public string JsFunc { get; set; }
        #endregion
    }
}