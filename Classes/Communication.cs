﻿using System;
using PatientRegister.Models;

namespace PatientRegister.Classes {
    public class Communication {
        //public static bool sendNotification( EnumCommunicationType _CommunicationType, UserClass _ReceipientUser, string _sTwoStepAuthOrPwdResetCode = "", Project _Project = null, Consultation _Job = null ) {
        //    try {
        //        string _sBody = $"Dear {_ReceipientUser.Name}";
        //        string _sSubject = "";
        //        switch ( _CommunicationType ) {
        //            case EnumCommunicationType.new_account_preapproval:
        //                getEmailContNewAccountPreapproval( ref _sSubject, ref _sBody );
        //                break;
        //            case EnumCommunicationType.account_approved:
        //                getEmailContAccountApprovedl( ref _sSubject, ref _sBody );
        //                break;
        //            case EnumCommunicationType.two_step_auth:
        //                getEmailCont2StepAuth( ref _sSubject, ref _sBody, _sTwoStepAuthOrPwdResetCode );
        //                break;
        //            case EnumCommunicationType.password_reset:
        //                getEmailContPasswordReset( ref _sSubject, ref _sBody, _sTwoStepAuthOrPwdResetCode );
        //                break;
        //            case EnumCommunicationType.closed_account:
        //                getEmailContAccountClosed( ref _sSubject, ref _sBody );
        //                break;
        //            case EnumCommunicationType.project_approved:
        //                getEmailContProjectApproved( ref _sSubject, ref _sBody, _Project );
        //                break;
        //            case EnumCommunicationType.project_expiring:
        //                getEmailContProjectExpiring( ref _sSubject, ref _sBody, _Project );
        //                break;
        //            case EnumCommunicationType.bid_awarded:
        //                getEmailContBidAwarded( ref _sSubject, ref _sBody, _Project );
        //                break;
        //            case EnumCommunicationType.new_bid:
        //                getEmailContNewBid( ref _sSubject, ref _sBody, _Project );
        //                break;
        //            case EnumCommunicationType.new_job_application:
        //                getEmailContNewJobApplication( ref _sSubject, ref _sBody, _Job );
        //                break;
        //            case EnumCommunicationType.job_expiring:
        //                getEmailContJobExpiring( ref _sSubject, ref _sBody, _Job );
        //                break;
        //            case EnumCommunicationType.job_application_shortlisted:
        //                getEmailContApplicationShortlisted( ref _sSubject, ref _sBody, _Job );
        //                break;
        //        }
        //        if ( _sSubject == string.Empty ) {
        //            return false;
        //        }
        //        return Mailer.send( $"\"{_ReceipientUser.Name } {_ReceipientUser.Surname }\" <{_ReceipientUser.Email}>", _sSubject, _sBody, _sCC: "" );
        //    } catch ( Exception _Ex ) {
        //        ErrorHandler.logError( "Class", "Common", "newAccount", _Ex.Message );
        //    }
        //    return false;
        //}

        //private static string getEmailContNewAccountPreapproval( ref string _sSubject, ref string _sBody ) {
        //    try {
        //        _sSubject = $"New {Constant.APP_NAME} account submitted for approval";
        //        _sBody = $@"{_sBody}<br><br>
        //                            Your {Constant.APP_NAME} account has been submitted for approval. You will get a response within the next 24 hours.<br><br>
        //                            {Constant.EMAIL_SALUTATION}";
        //    } catch ( Exception _Ex ) {
        //        ErrorHandler.logError( "Class", "Common", "getEmailContNewAccountPreapproval", _Ex.Message );
        //    }
        //    return string.Empty;
        //}

        //private static string getEmailContAccountApprovedl( ref string _sSubject, ref string _sBody ) {
        //    try {
        //        _sSubject = $"Welcome to {Constant.APP_NAME}, account approval";
        //        _sBody = $@"{_sBody}<br><br>
        //                            Your {Constant.APP_NAME} account has been approved. Thanks you for registering on this platform, we hope you find your endeavours here fruitful. <br><br>
        //                           We love to hear from you so please don't hesitate to email us for any feedback: Bugs, Frustrations, Improvements and Compliments alike :-). <br><br>
        //                           {Constant.EMAIL_SALUTATION}";
        //    } catch ( Exception _Ex ) {
        //        ErrorHandler.logError( "Class", "Common", "getEmailContAccountApprovedl", _Ex.Message );
        //    }
        //    return string.Empty;
        //}

        //private static string getEmailCont2StepAuth( ref string _sSubject, ref string _sBody, string _sAuthCode ) {
        //    if ( string.IsNullOrEmpty( _sAuthCode ) ) {
        //        return "";
        //    }
        //    try {
        //        _sSubject = $"Your two-step verification code for {Constant.APP_NAME}";
        //        _sBody = $@"{_sBody}<br><br>
        //                            Your two-step verification code is{_sAuthCode}, it expires in 2 hours.<br><br>
        //                            {Constant.EMAIL_SALUTATION}";
        //    } catch ( Exception _Ex ) {
        //        ErrorHandler.logError( "Class", "Common", "getEmailCont2StepAuth", _Ex.Message );
        //    }
        //    return string.Empty;
        //}

        //private static string getEmailContPasswordReset( ref string _sSubject, ref string _sBody, string _sResetCode ) {
        //    try {
        //        _sSubject = $"{Constant.APP_NAME} password reset requested";
        //        _sBody = $@"{_sBody}<br><br>
        //                            Your {Constant.APP_NAME} password reset code is {_sResetCode}.<br><br>
        //                            {Constant.EMAIL_SALUTATION}";
        //    } catch ( Exception _Ex ) {
        //        ErrorHandler.logError( "Class", "Common", "getEmailContNewAccount", _Ex.Message );
        //    }
        //    return string.Empty;
        //}

        //private static string getEmailContAccountClosed( ref string _sSubject, ref string _sBody ) {
        //    try {
        //        _sSubject = $"{Constant.APP_NAME} - it's sad to say Goodbye";
        //        _sBody = $@"{_sBody}<br><br>
        //                            Your {Constant.APP_NAME} account has been deactivated. You can always come back when you miss us :-)<br><br>                                    
        //                            {Constant.EMAIL_SALUTATION}";
        //    } catch ( Exception _Ex ) {
        //        ErrorHandler.logError( "Class", "Common", "getEmailContAccountClosed", _Ex.Message );
        //    }
        //    return string.Empty;
        //}

        //private static string getEmailContProjectApproved( ref string _sSubject, ref string _sBody, Project _Project ) {
        //    if ( _Project.ProjectID == default ) {
        //        return string.Empty;
        //    }
        //    try {
        //        _sSubject = $"Project listing approved on {Constant.APP_NAME}";
        //        _sBody = $@"{_sBody}<br><br>
        //                            Your project listing: {_Project.Title} has been approved and is now ready to attract bids. Good luck!<br><br>
        //                            {Constant.EMAIL_SALUTATION}";
        //    } catch ( Exception _Ex ) {
        //        ErrorHandler.logError( "Class", "Common", "getEmailContProjectApproved", _Ex.Message );
        //    }
        //    return string.Empty;
        //}

        //private static string getEmailContProjectExpiring( ref string _sSubject, ref string _sBody, Project _Project ) {
        //    if ( _Project.ProjectID == default ) {
        //        return string.Empty;
        //    }
        //    try {
        //        _sSubject = $"Your {Constant.APP_NAME} project listing is expiring soon";
        //        _sBody = $@"{_sBody}<br><br>
        //                            Your project listing: {_Project.Title} on {Constant.APP_NAME} is expiring on {_Project.ExpiryDate}. You can extend the expiration is you need to get more bids.<br><br>
        //                            {Constant.EMAIL_SALUTATION}";
        //    } catch ( Exception _Ex ) {
        //        ErrorHandler.logError( "Class", "Common", "getEmailContProjectExpiring", _Ex.Message );
        //    }
        //    return string.Empty;
        //}

        //private static string getEmailContBidAwarded( ref string _sSubject, ref string _sBody, Project _Project ) {
        //    try {
        //        _sSubject = $"Congratulations, you've won a project bid on {Constant.APP_NAME}!";
        //        _sBody = $@"{_sBody}<br><br>
        //                            Your bid for project: {_Project.Title} was successful. All be the best in completing the project and cheers to great reviews afterwards! 
        //                            You can get in touch if you need any kind of assistance during the course of the project.<br><br>
        //                            {Constant.EMAIL_SALUTATION}";
        //    } catch ( Exception _Ex ) {
        //        ErrorHandler.logError( "Class", "Common", "getEmailContBidAwarded", _Ex.Message );
        //    }
        //    return string.Empty;
        //}

        //private static string getEmailContNewBid( ref string _sSubject, ref string _sBody, Project _Project ) {
        //    if ( _Project.ProjectID == default ) {
        //        return string.Empty;
        //    }
        //    try {
        //        _sSubject = $"New bid for your {Constant.APP_NAME} project listing";
        //        _sBody = $@"{_sBody}<br><br>
        //                            A new bid has been placed on your project listing: {_Project.Title} on {Constant.APP_NAME}. Please sign in to check all the bids that have been received so far.<br><br>
        //                            {Constant.EMAIL_SALUTATION}";
        //    } catch ( Exception _Ex ) {
        //        ErrorHandler.logError( "Class", "Common", "getEmailContNewBid", _Ex.Message );
        //    }
        //    return string.Empty;
        //}

        //private static string getEmailContNewJobApplication( ref string _sSubject, ref string _sBody, Consultation _Job ) {
        //    if ( _Job.JobID == default ) {
        //        return string.Empty;
        //    }
        //    try {
        //        _sSubject = $"New application for your {Constant.APP_NAME} project listing";
        //        _sBody = $@"{_sBody}<br><br>
        //                            A new application has been placed on your job listing: {_Job.Title} on {Constant.APP_NAME}. Please sign in to check all the applications that have been received so far.<br><br>
        //                            {Constant.EMAIL_SALUTATION}";
        //    } catch ( Exception _Ex ) {
        //        ErrorHandler.logError( "Class", "Common", "getEmailContNewJobApplication", _Ex.Message );
        //    }
        //    return string.Empty;
        //}

        //private static string getEmailContJobExpiring( ref string _sSubject, ref string _sBody, Consultation _Job ) {
        //    if ( _Job.JobID == default ) {
        //        return string.Empty;
        //    }
        //    try {
        //        _sSubject = $"Your {Constant.APP_NAME} job listing is expiring soon";
        //        _sBody = $@"{_sBody}<br><br>
        //                            Your job listing: {_Job.Title} on {Constant.APP_NAME} is expiring on {_Job.ExpiryDate}. You can extend the expiration is you need to get more applications.<br><br>
        //                            {Constant.EMAIL_SALUTATION}";
        //    } catch ( Exception _Ex ) {
        //        ErrorHandler.logError( "Class", "Common", "getEmailContJobExpiring", _Ex.Message );
        //    }
        //    return string.Empty;
        //}

        //private static string getEmailContApplicationShortlisted( ref string _sSubject, ref string _sBody, Consultation _Job ) {
        //    if ( _Job.JobID == default ) {
        //        return string.Empty;
        //    }
        //    try {
        //        _sSubject = $"Congratulations, you've been shortlisted for a job on {Constant.APP_NAME}!";
        //        _sBody = $@"{_sBody}<br><br>
        //                            Your application for job: {_Job.Title} was shortlisted. All the best with the selection process!<br><br>
        //                            {Constant.EMAIL_SALUTATION}";
        //    } catch ( Exception _Ex ) {
        //        ErrorHandler.logError( "Class", "Common", "getEmailContApplicationShortlisted", _Ex.Message );
        //    }
        //    return string.Empty;
        //}
    }
}