﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using PatientRegister.Models;
using PatientRegister.Repositories;
using PatientRegister.ViewModels;

public static class Common {
    public static string GetJobRefNo() {
        return DateTime.Now.Hour.ToString().Trim() + DateTime.Now.Minute.ToString().Trim() + DateTime.Now.Second.ToString().Trim() + DateTime.Now.Millisecond.ToString().Trim() + DateTime.Today.Day.ToString().Trim() + DateTime.Today.Month.ToString().Trim() + DateTime.Today.Year.ToString();
    }

    public static bool isLiveServer() {
        return false;// return HttpContext.Current.Request.Url.Host.StartsWith(Constant.DOMAIN_URL, StringComparison.CurrentCulture);
    }

    public static string getRecordHeaderText(string _sWholeText, int _iLettersToShow = 50) {
        try {
            _iLettersToShow = (_iLettersToShow < 10 ? 10 : _iLettersToShow); //Set minimum letters to show to 10
            if (_sWholeText.Length > _iLettersToShow) {
                _sWholeText = _sWholeText.Substring(0, _iLettersToShow - 3) + "..."; // if letters to show is less than string length, show part of string plus ellipsis (...)
            }
            return " - " + _sWholeText; //prepend a hiphen
        } catch (Exception _Ex) {
            ErrorHandler.logError("Class", "Common", "getRecordHeaderText", _Ex.Message);
        }
        return _sWholeText;
    }

    public static string getEllipsisText(string _sWholeText, int _iLettersToShow = 30) {
        try {
            _iLettersToShow = (_iLettersToShow < 10 ? 10 : _iLettersToShow); //Set minimum letters to show to 10
            if (_sWholeText.Length > _iLettersToShow) {
                _sWholeText = _sWholeText.Substring(0, _iLettersToShow - 3) + "..."; // if letters to show is less than string length, show part of string plus ellipsis (...)
            }
            return _sWholeText; //prepend a hiphen
        } catch (Exception _Ex) {
            ErrorHandler.logError("Class", "Common", "getEllipsisText", _Ex.Message);
        }
        return _sWholeText;
    }

    public static dynamic getListItemValue(List<ListItem> _lItems, string _sIndex) {
        try {
            foreach (ListItem _Value in _lItems) {
                if (_Value.inProperty(_sIndex)) {
                    return _Value.getProperty(_sIndex);
                }
            }
        } catch (Exception _Ex) {
            ErrorHandler.logError("Class", "Common", "getListItemValue", _Ex.Message);
        }
        return "";
    }

    public static bool inListItem(List<ListItem> _lItems, string _sIndex, out string _sValue) {
        _sValue = "";
        try {
            _sValue = getListItemValue(_lItems, _sIndex);
            return _sValue != String.Empty;
        } catch (Exception _Ex) {
            ErrorHandler.logError("Class", "Common", "inListItem", _Ex.Message);
        }
        return false;
    }

    public static bool inListItem(List<ListItem> _lItems, string _sIndex, out bool _bValue) {
        _bValue = false;

        try {
            if (bool.TryParse(getListItemValue(_lItems, _sIndex).ToString(), out _bValue)) {
                return true;
            }
        } catch (Exception _Ex) {
            ErrorHandler.logError("Class", "Common", "inListItem", _Ex.Message);
        }
        return false;
    }

    public static bool inListItem(List<ListItem> _lItems, string _sIndex, out int _iRefIndex) {
        ListItem _ListItem = null;
        _iRefIndex = 0;
        try {
            int _iCount = _lItems.Count;

            for (int _iIndex = 0; _iIndex < _iCount; _iIndex++) {
                _ListItem = (ListItem)_lItems[_iIndex];

                if (_ListItem.inProperty(_sIndex)) {
                    _iRefIndex = _iIndex;
                    return true;
                }
            }
        } catch (Exception _Ex) {
            ErrorHandler.logError("Class", "Common", "inListItem", _Ex.Message);
        }

        return false;
    }

    public static bool validImageFileExtensions(string _sFileExt) {
        string[] _sAllowableExtensions = { ".jpg", ".png", ".jpeg" };
        try {
            foreach (string _sVal in _sAllowableExtensions) {
                if (_sVal.Contains(_sFileExt.ToLower())) {
                    return true;
                }
            }
        } catch (Exception _Ex) {
            ErrorHandler.logError("Class", "Common", "validImageFileExtensions", _Ex.Message);
        }
        return false;
    }

    public static bool validFileExtensions(string _sFileExt) {
        string[] _sAllowableExtensions = { ".pdf", ".docx", ".xlsx" };
        try {
            foreach (string _sVal in _sAllowableExtensions) {
                if (_sVal.Contains(_sFileExt.ToLower())) {
                    return true;
                }
            }
        } catch (Exception _Ex) {
            ErrorHandler.logError("Class", "Common", "validFileExtensions", _Ex.Message);
        }
        return false;
    }

    public static string getDescriptiveTime(DateTime _dtDate) {
        try {
            DateTime _dtNow = DateTime.Now;
            bool _bPast = _dtDate < _dtNow;
            TimeSpan _TimeSpan = _dtNow.Subtract(_dtDate);
            int _iDayDiff = (int)_TimeSpan.TotalDays;
            int _iHourDiff = (int)_TimeSpan.TotalHours;
            int _iMinDiff = (int)_TimeSpan.TotalMinutes;
            int _iSecDiff = (int)_TimeSpan.TotalSeconds;
            _iDayDiff = (_iDayDiff < 0 ? _iDayDiff * -1 : _iDayDiff);
            _iHourDiff = (_iHourDiff < 0 ? _iHourDiff * -1 : _iHourDiff);
            _iMinDiff = (_iMinDiff < 0 ? _iMinDiff * -1 : _iMinDiff);
            _iSecDiff = (_iSecDiff < 0 ? _iSecDiff * -1 : _iSecDiff);
            if (_iDayDiff == 0) {
                if (_iSecDiff < 60) {
                    return "Just now";
                }
                int _iHoursToday = _iMinDiff / 60;
                int _iMins = _iMinDiff % 60;
                return getDescriptiveTimeTense((_iHoursToday == 0 ? "" : _iHoursToday.ToString() + " hour" + (_iHoursToday == 1 ? "" : "s")) +
                                                                        (_iMins != 0 && _iHoursToday != 0 ? ", " : "") +
                                                                        (_iMins == 0 ? "" : _iMins.ToString() + " minute" + (_iMins == 1 ? "" : "s")),
                                                                        _bPast);
            }
            if (_iDayDiff < 7) {
                int _iDaysHr = _iHourDiff / 24;
                int _iHours = _iHourDiff % 24;
                return getDescriptiveTimeTense(_iDaysHr.ToString() + " day" +
                                                                    (_iDaysHr == 1 ? "" : "s") +
                                                                    (_iHours == 0 ? "" : ", " + _iHours.ToString() + " hour" + (_iHours == 1 ? "" : "s")),
                                                                    _bPast);
            }
            if (_iDayDiff < 31) {
                int _iWeeks = _iDayDiff / 7;
                int _iDaysWeek = _iDayDiff % 7;
                return getDescriptiveTimeTense(_iWeeks.ToString() + " week" +
                                                                    (_iWeeks == 1 ? "" : "s") +
                                                                    (_iDaysWeek == 0 ? "" : ", " + _iDaysWeek.ToString() + " day" + (_iDaysWeek == 1 ? "" : "s")),
                                                                    _bPast);
            }
            if (_iDayDiff < 365) {
                int _iMonth = _iDayDiff / 31;
                int _iDaysMonth = _iDayDiff % 31;
                return getDescriptiveTimeTense(_iMonth.ToString() + " month" +
                                                                    (_iMonth == 1 ? "" : "s") +
                                                                    (_iDaysMonth == 0 ? "" : ", " + _iDaysMonth.ToString() + " day" + (_iDaysMonth == 1 ? "" : "s")),
                                                                    _bPast);
            }
            int _iYear = _iDayDiff / 365;
            return getDescriptiveTimeTense(_iYear.ToString() + " year" +
                                                                (_iYear == 1 ? "" : "s"),
                                                                _bPast) + ", on " + _dtDate.ToString("dd MMM HH:mm");
        } catch (Exception _Ex) {
            ErrorHandler.logError("Class", "Common", "getDescriptiveTime", _Ex.Message);
        }
        return "On " + _dtDate.ToString();
    }

    private static string getDescriptiveTimeTense(string _sTimeDescr, bool _bPast) {
        if (_bPast) {
            return _sTimeDescr + " ago";
        }
        return _sTimeDescr + " left";
    }

    public static bool convDate(string _sValue, out string _sDate, bool _bToSql = true) {
        _sDate = string.Empty;
        string _sSep, _sFmtIn, _sFmtOut;
        int _iIndex;

        try {
            if (_bToSql) {
                _sFmtIn = Constant.DATE_FORMAT_SHORT;
                _sFmtOut = Constant.DATE_FORMAT_SHORT;
            } else {
                _sFmtIn = Constant.DATE_FORMAT_SHORT;
                _sFmtOut = Constant.DATE_FORMAT_SHORT;
            }

            //Format in - current format
            if (!Validate.hasAccepDateSep(_sFmtIn, out _sSep))
                return false;
            string[] _sFmtInParts = _sFmtIn.Split(char.Parse(_sSep));
            if (_sFmtInParts.Length != 3)
                return false;

            //Format out - converted format
            if (!Validate.hasAccepDateSep(_sFmtOut, out _sSep))
                return false;
            string[] _sFmtOutParts = _sFmtOut.Split(char.Parse(_sSep));
            if (_sFmtOutParts.Length != 3)
                return false;

            //Date parts - value to be converted
            if (!Validate.hasAccepDateSep(_sValue, out _sSep))
                return false;
            string[] _sValues = _sValue.Split(' ');
            string[] _sDateParts = _sValues[0].Split(char.Parse(_sSep));
            if (_sDateParts.Length != 3)
                return false;

            List<string> _lConvDateParts = new List<string>();

            foreach (string _sFmtOutPart in _sFmtOutParts) {
                //Determine year
                if (Validate.isFmtNotationYear(_sFmtOutPart)) {
                    if (!Validate.isFmtNotationYear(_sFmtInParts, out _iIndex))
                        return false;
                    _lConvDateParts.Add(
                        !_bToSql
                            || _sDateParts[_iIndex].Length == 4
                        ? _sDateParts[_iIndex]
                        : "20" + _sDateParts[_iIndex]
                    );
                    continue;
                }

                //Determine month
                if (Validate.isFmtNotationMonth(_sFmtOutPart)) {
                    if (!Validate.isFmtNotationMonth(_sFmtInParts, out _iIndex))
                        return false;
                    _lConvDateParts.Add(_sDateParts[_iIndex]);
                    continue;
                }

                //Determine day
                if (Validate.isFmtNotationDay(_sFmtOutPart)) {
                    if (!Validate.isFmtNotationDay(_sFmtInParts, out _iIndex))
                        return false;
                    _lConvDateParts.Add(_sDateParts[_iIndex]);
                    continue;
                }
            }

            //Handle date part
            if (_lConvDateParts.Count != 3)
                return false;
            _sDate = String.Join(_sSep, _lConvDateParts);

            //Handle time part
            if (_sValues.Length > 1) {
                string _sTime = string.Empty;

                for (int _iI = 1; _iI < _sValues.Length; _iI++) {
                    _sTime += _sValues[_iI];
                    if (_iI < (_sValues.Length - 1))
                        _sTime += " ";
                }

                if (_sTime.Trim() != String.Empty) {
                    if (_sTime.Contains("AM")
                            || _sTime.Contains("PM"))
                        _sTime = DateTime.Parse(DateTime.Now.Date.ToString(Constant.DATE_FORMAT_SHORT) + " " + _sTime).ToString("HH:mm:ss");

                    _sDate += " " + _sTime;
                }
            }
            _sDate = _sDate.Trim();
        } catch (Exception _Ex) {
            ErrorHandler.logError("Class", "Common", "convDate", _Ex.Message);
        }
        return _sDate.Trim() != string.Empty;
    }

    public static string getRandomText(int _iLength) {
        try {
            Random random = new Random(_iLength);
            string _sChar = "!@#$%&*()_+/><AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789";//Excluded power sign
            string _sReturn = string.Empty;
            Random _Rm = new Random();
            for (int i = 0; i < _iLength; i++) {
                _sReturn += _sChar[_Rm.Next(0, _sChar.Length - 1)];
            }
            return _sReturn;
        } catch (Exception _Ex) {
            ErrorHandler.logError("Class", "Common", "getRandomText", _Ex.Message);
        }
        return "1T5AW350M3";
    }

    public static string showNotification(string _sMessage, EnumNotificationType _NotificationType, string _sElement = "", EnumNotificationSide _NotificationSide = EnumNotificationSide.none, int _iDelayMillisec = 0) {
        if (_iDelayMillisec == 0) {
            return "<script>$(function () { showNotification( '" + _sMessage + "', '" + (int)_NotificationType + "', '" + _sElement + "', '" + (_NotificationSide == EnumNotificationSide.none ? "" : _NotificationSide.ToString()) + "' ) });</script>";
        }
        return "<script>setTimeout(function () { showNotification( '" + _sMessage + "', '" + (int)_NotificationType + "', '" + _sElement + "', '" + (_NotificationSide == EnumNotificationSide.none ? "" : _NotificationSide.ToString()) + "' ) }, " + _iDelayMillisec.ToString() + ");</script>";
    }

    public static FileModel getFileDetail(string _sFilePath) {
        try {
            return new FileModel { FilePath = _sFilePath.Replace("~", ""), FileName = Path.GetFileName(System.Web.Hosting.HostingEnvironment.MapPath(_sFilePath)) };
        } catch (Exception _Ex) {
            //   ErrorLog.handleError("Class", "DataController", "exportFile", _Ex.Message);
        }
        return null;
    }

    public static string getStatisticsChangeDescription(int _iPrevious, int _iCurrent, string _sPeriodDescription = "month") {
        try {
            string _sRet = "";
            if (_iPrevious == _iCurrent) {
                return _sRet = $"Same as this time last {_sPeriodDescription}";
            }
            if (_iPrevious == 0) {
                return _sRet = $"No records available for last {_sPeriodDescription}";
            }
            int _iDifference = 100 * (_iPrevious - _iCurrent) / _iPrevious;
            if (_iDifference == 0) { //Not same number but percentage is rounded to zero
                return _sRet = $"Insignificant change from this time last {_sPeriodDescription}";
            }
            if (_iPrevious < _iCurrent) {
                _iDifference *= -1;
                _sRet = $"Increased by { _iDifference }% from this time last {_sPeriodDescription}";
            }
            if (_iPrevious > _iCurrent) {
                _sRet = $"Decreased by { _iDifference }% from this time last {_sPeriodDescription}";
            }
            return _sRet;
        } catch {
        }
        return $"Last {_sPeriodDescription} value was {_iPrevious}";
    }
}