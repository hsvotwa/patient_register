﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PatientRegister.Models;
using PatientRegister.Repositories;

public class UserSession : UserClass {
    public static void setUser(UserClass _UserClass, bool _bKeepSession = false) {
        if (_UserClass != null) {
            HttpContext.Current.Session["User"] = new SessionUserMdl {
                UserKey = _UserClass.UserID.ToString(),
                Email = _UserClass.Email,
                Fullname = _UserClass.Name + " " + _UserClass.Surname,
                FirstName = _UserClass.Name,
                StaffType = _UserClass.StaffTypeID
            };
        }
        if (_bKeepSession) {
            HttpContext.Current.Session.Timeout = 20160 /*tot mins in 14 days*/;
        } else {
            HttpContext.Current.Session.Timeout = 30;
        }
    }

    public static void killUserSession() {
        HttpContext.Current.Session["User"] = null;
    }

    public static void redirect(string _sRedirectUrl) {
        if (string.IsNullOrEmpty(getUser().UserKey)) {
            HttpContext.Current.Response.Redirect("/Account/Login/");
        } else {
            HttpContext.Current.Response.Redirect(_sRedirectUrl);
        }
    }

    public static void checkLogIn() {
        if ((string.IsNullOrEmpty(getUser().UserKey))) {
            HttpContext.Current.Session["nextpage"] = HttpContext.Current.Request.Url.AbsoluteUri;
            HttpContext.Current.Response.Redirect("/Account/Login/");
        } else {
            HttpContext.Current.Session["nextpage"] = null;
        }
    }

    public static string checkLogInAjax() {
        if ((string.IsNullOrEmpty(getUser().UserKey))) {
            return "<p class='err_mess'>You are nolonger logged in. Your request has been aborted. Click <a href='login?r=true'>here</a> to log in.</p>";
        }
        return string.Empty;
    }

    public static void checkUserLogIn() {
        if ((string.IsNullOrEmpty(getUser().UserKey))) {
            HttpContext.Current.Response.Redirect("/Home/");
        }
    }

    public static string getCurrentUserKey() {
        return getUser().UserKey;
    }

    public static string getUserFullName() {
        return getUser().Fullname;
    }

    public static string getUserEmail() {
        return getUser().Email;
    }

    public static string getUserFirstName() {
        return getUser().FirstName;
    }

    public static EnumStaffType getStaffType() {
        return (EnumStaffType)getUser().StaffType;
    }

    public static SessionUserMdl getUser() {
        try {
            if (HttpContext.Current.Session["User"] != null) {
                if (HttpContext.Current.Session["User"] is SessionUserMdl) {
                    return HttpContext.Current.Session["User"] as SessionUserMdl;
                }
            }
        } catch (Exception _Ex) {
            ErrorHandler.handleError("Class", "UserSession", "getCurrent", _Ex.Message);
        }
        return new SessionUserMdl();
    }

    public static bool hasAccessTo(EnumUserRole _EnumUserRole, EnumAccessLevel _EnumAccessLevel) {
        List<UserRoleAccess> _RoleList = new List<UserRoleAccess>();
        try {
            UnitOfWork _UnitOfWork = new UnitOfWork(new PatientRegisterContext());
            Guid _CurrentUserGuid = Guid.Parse(getCurrentUserKey());
            IEnumerable<UserRoleAccess> _AllCurrentAccessRoles = _UnitOfWork.UserRoleAccesses.GetFiltered(
                _Rec => _Rec.UserID == _CurrentUserGuid &&
                _Rec.UserRoleID == (int)_EnumUserRole
                );
            if (_AllCurrentAccessRoles == null || _AllCurrentAccessRoles.Count() == 0) {
                return false;
            }
            if ((int)_EnumAccessLevel == _AllCurrentAccessRoles.FirstOrDefault().AccessLevelID) {
                return true;
            }
            if (_EnumAccessLevel == EnumAccessLevel.read && _AllCurrentAccessRoles.FirstOrDefault().AccessLevelID == (int)EnumAccessLevel.write) { //We're asking for read but you can write
                return true;
            }
            return false;
        } catch (Exception _Ex) {
            ErrorHandler.handleError("Class", "UserClass", "hasAccessTo", _Ex.Message);
        }
        return false;
    }

    public static void checkUserAccess(EnumUserRole _EnumUserRole, EnumAccessLevel _EnumAccessLevel) {
        if (!hasAccessTo(_EnumUserRole, _EnumAccessLevel)) {
            HttpContext.Current.Response.Redirect("/Error/NoAccess");
        }
    }
}