﻿public enum EnumRequestType {
    login = 1,
    save_branch = 2,
    save_diagnosis = 3,
    save_symptom = 4,
    save_medical_stock = 5,
    save_medica_aid_company = 6,
    get_list = 7,
    file_upload = 8,
    save_user = 9,
    save_patient = 10,
    save_consultation = 11,
    save_operation = 12,
    save_prescription = 13,
    save_referral = 14,
    settings = 15,
    change_password = 16,
    reset_password = 17
}

public enum EnumUserRole {
    users = 1,
    patients = 2,
    consultations = 3,
    operations = 4,
    referrals = 5,
    prescriptions = 6,
    branches = 7,
    diagnoses = 8,
    symptoms = 9,
    settings = 10,
    medical_stock_item = 11,
    medical_aid_company = 12
}

public enum EnumStatus {
    none = 0,
    active = 1,
    inactive = 3,
    deleted = 4
}

public enum EnumMeasurementUnit {
    none = 0,
    units = 1,
    miligrams = 3
}

public enum EnumTimeUnit {
    none = 0,
    day = 1,
    week = 2,
    month = 3
}

public enum EnumStaffType {
    none = 0,
    administrator = 1,
    doctor = 2,
    doctor_and_surgeon = 3,
    anaesthetist = 4
}

public enum EnumGender {
    not_set = 0,
    male = 1,
    female = 2
}

public enum EnumPersonRelationship {
    other = 100,
    spouse = 1,
    parent = 2,
    sibling = 3,
    grandparent = 4,
    grandchild = 5
}

public enum EnumCommunicationOption {
    none = 100,
    sms = 1,
    email = 2,
    sms_and_email = 3
}

public enum EnumAccessLevel {
    none = 0,
    read = 1,
    write = 2
}

public enum EnumResponseStatus {
    success = 1,
    failed = 2,
    error = 3,
    warning = 4, //
    info = 5, //For stuff like "Already saved"
}

public enum EnumFileEntityType {
    patient = 1,
    consultation = 2,
    prescription = 3,
    operation = 4,
    referral = 5,
    user = 6
}

public enum EnumConfiguration {
    none = 0,
    logo_path = 1,
    practice_name = 2,
    referral_record_count = 3,
}

public enum EnumYesNo {
    yes = 1,
    no = 2,
    none = 3,
}

public enum EnumNotificationType {
    success = 1,
    error = 2,
    warning = 3,
    info = 4
}

public enum EnumNotificationSide {
    none,
    top,
    left,
    bottom,
    right
}

public enum EnumCommunicationType {
    new_patient,
    new_user_account,
    appointment_reminder
}