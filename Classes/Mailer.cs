﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;

public class Mailer {
    public static bool send( string _sTo, string _sSubject, string _sBody, string _sCC = "", string _sBCC = "", IEnumerable<string> _Attachments = null ) {
        try {
            var _SenderEmail = new MailAddress( Constant.FROM_EMAIL, Constant.APP_NAME );
            var _ReceiverEmail = new MailAddress( _sTo, "" );
            var _SmtpClient = new SmtpClient {
                Host = Constant.SMTP_SERVER,
                //Port = Constant.PORT_NUMBER,
                EnableSsl = false,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential( _SenderEmail.Address, Constant.WEBMAIL_PASSWORD )
            };
            using ( var _vMailMessage = new MailMessage( _SenderEmail, _ReceiverEmail ) {
                Subject = _sSubject,
                Body = _sBody,
                IsBodyHtml = true
            } ) {
                if ( !string.IsNullOrEmpty( _sCC ) ) {
                    _vMailMessage.CC.Add( _sCC );
                }
                if ( !string.IsNullOrEmpty( _sBCC ) ) {
                    _vMailMessage.Bcc.Add( _sBCC );
                }
                if ( _Attachments != null && _Attachments.Count() > 0 ) {
                    foreach ( string _sFilePath in _Attachments ) {
                        Attachment _Attachment = new Attachment( _sFilePath, MediaTypeNames.Application.Octet );
                        _vMailMessage.Attachments.Add( _Attachment );
                    }
                }
                _SmtpClient.Send( _vMailMessage );
            }
            return true;
        } catch ( Exception _Ex ) {
        }
        return false;
    }

    public static string getSignature() {
        string _sSignature = "Cheers,<br><br>";
        try {
            _sSignature += "<br/><img src=\"" + Constant.SIGNATUTE_IMAGE_URL + "\"/ height=\"80\" width=\"140\">";
            _sSignature += "<br/><a href=\"" + Constant.WEBSITE_URL + "\" target=\"_blank\">" + Constant.WEBSITE_URL + "</a>";
        } catch ( Exception ) {
        }
        return _sSignature;
    }
}