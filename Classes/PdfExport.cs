﻿using System;
using System.Web;
using System.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using PatientRegister.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using PatientRegister.Repositories;

namespace PatientRegister {
    public class pdfExports {
        private static BaseColor g_TitleBlueColor = new BaseColor(8, 53, 131); //BaseColor
        private static BaseColor g_HeaderBlueColor = new BaseColor(138, 176, 227);
        private static BaseColor g_GrayColor = new BaseColor(82, 82, 82);
        private static BaseColor g_BlackColor = new BaseColor(0, 0, 0);
        private static BaseColor g_WhiteColor = new BaseColor(255, 255, 255);
        public static string generatePdfReport(DataSet _DataSet, string[] _ExcludeCols, float[] columnDefinitionSize,
            string _sDocName, string _sReportTitle, bool _bLandscape = false, bool _bGrouped = false, string[] _sGroupByCols = null, string _sTotallingColumn = "amount") {
            string _sPath = "~/PdfExports/" + _sDocName + " " + "_" + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".pdf";
            string _sLogoPath = "~/assets/images/logo_gt.png";
            try {
                DataTable _DataTable = _DataSet.Tables[0];
                DataTable _DataTableExclGrouped = _DataSet.Tables[0].Clone();
                foreach (DataRow _Dr in _DataTable.Rows) {
                    _DataTableExclGrouped.Rows.Add(_Dr.ItemArray);
                }
                _DataTableExclGrouped.TableName = "tmp_table";
                for (int _iI = 0; _iI < _ExcludeCols.Length; _iI++) {
                    if (_DataTable.Columns.Contains(_ExcludeCols[_iI])) {
                        _DataTable.Columns.Remove(_ExcludeCols[_iI]);
                        _DataTableExclGrouped.Columns.Remove(_ExcludeCols[_iI]);
                    }
                }
                if (_sGroupByCols != null) {
                    foreach (string _sCol in _sGroupByCols) {
                        if (_bGrouped && _DataTableExclGrouped.Columns.Contains(_sCol)) { //Remove grouping cols from display cols
                            _DataTableExclGrouped.Columns.Remove(_sCol);
                            _DataTable.RejectChanges();
                        }
                    }
                }
                int _iColCount = _DataTableExclGrouped.Columns.Count;
                var _Document = new Document(PageSize.LETTER_LANDSCAPE);
                if (_bLandscape) {
                    _Document.SetPageSize(PageSize.A4.Rotate());
                }
                Font _Font = new Font(Font.FontFamily.HELVETICA);
                Font _FontHeader = new Font(Font.FontFamily.HELVETICA);
                Font _FontWeight = FontFactory.GetFont(FontFactory.TIMES_BOLD, 22);
                Font _FontWeightSmaller = FontFactory.GetFont(FontFactory.TIMES_BOLD, 12);
                Font _FontUnderline = FontFactory.GetFont(FontFactory.TIMES, 22, Font.UNDERLINE);
                _Font.Size = 10;
                _FontHeader.Size = 15;
                _Font.SetStyle("Bold");
                Paragraph _paragraph = new Paragraph();
                Phrase _phrase = new Phrase();
                _paragraph.Alignment = 1;
                PdfWriter _PdfWriter = PdfWriter.GetInstance(_Document,
                           new System.IO.FileStream(HttpContext.Current.Server.MapPath(_sPath),
                               System.IO.FileMode.Create));
                _Document.Open();
                Image _ImgLogo = Image.GetInstance(System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(_sLogoPath)), System.Drawing.Imaging.ImageFormat.Png);
                _ImgLogo.Alignment = Image.LEFT_ALIGN;
                _ImgLogo.ScaleToFit(110f, 70f);
                float[] _fColumnDefinitionSizeHeader = { 70F, 30F };
                PdfPTable _PdfPTable = new PdfPTable(_fColumnDefinitionSizeHeader);
                _PdfPTable.WidthPercentage = 100;
                Paragraph _pEntityNameTag = new Paragraph(
                        new Chunk(
                            Configuration.getValue(EnumConfiguration.practice_name),
                            FontFactory.GetFont(
                                    BaseFont.HELVETICA,
                                    _FontHeader.Size,
                                    1,
                                    new BaseColor(0, 0, 10)
                                )
                            )
                    );
                _pEntityNameTag.Alignment = Element.ALIGN_LEFT;
                Paragraph _pTitle = new Paragraph(
                           new Chunk(
                              _sReportTitle.ToUpper(),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       11,
                                       1,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           );
                Paragraph _pUserDet = new Paragraph(
                           new Chunk(
                            "Downloaded by " + UserSession.getUserFullName() + " @ " + DateTime.Now.ToString("dd MMM yyyy HH:mm"),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       10,
                                       0,
                                       new BaseColor(120, 120, 120)
                                   )
                               )
                           );
                PdfPTable _PdfPTableHeader0 = new PdfPTable(1);
                PdfPCell _PdfPCell1 = null;
                _PdfPCell1 = new PdfPCell(_pEntityNameTag);
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPTableHeader0.AddCell(_PdfPCell1);
                _PdfPCell1 = null;
                _PdfPCell1 = new PdfPCell(new Paragraph(new Chunk("Tel: " + Constant.COMPANY_PHONE, FontFactory.GetFont(BaseFont.HELVETICA, 11))));
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPTableHeader0.AddCell(_PdfPCell1);
                _PdfPCell1 = new PdfPCell(new Paragraph(new Chunk("Email: " + Constant.COMPANY_EMAIL, FontFactory.GetFont(BaseFont.HELVETICA, 11))));
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPTableHeader0.AddCell(_PdfPCell1);
                _PdfPCell1 = new PdfPCell(new Paragraph(new Chunk("Fax: " + Constant.COMPANY_FAX, FontFactory.GetFont(BaseFont.HELVETICA, 11))));
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPTableHeader0.AddCell(_PdfPCell1);

                _PdfPCell1 = new PdfPCell(_ImgLogo);
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPTable.AddCell(_PdfPCell1);
                _PdfPCell1 = new PdfPCell(_PdfPTableHeader0);
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPTable.AddCell(_PdfPCell1);
                _PdfPCell1 = new PdfPCell(_pTitle);
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.Colspan = 2;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPTable.AddCell(_PdfPCell1);
                _PdfPCell1 = new PdfPCell(_pUserDet);
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.Colspan = 2;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPTable.AddCell(_PdfPCell1);

                _Document.Add(_PdfPTable);

                BaseColor _BgColour = null;
                PdfPTable _PdfTable = null;
                _PdfTable = new PdfPTable(columnDefinitionSize);
                PdfPCell _PdfCell = null;
                _PdfTable.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfTable.WidthPercentage = 100;
                for (int _iColInx = 0; _iColInx < _iColCount; _iColInx++) {
                    bool isNumber = (_DataTableExclGrouped.Columns[_iColInx].DataType.FullName == "System.Int32"
                            || _DataTableExclGrouped.Columns[_iColInx].DataType.FullName == "System.Int16"
                            || _DataTableExclGrouped.Columns[_iColInx].DataType.FullName == "System.Double"
                             || _DataTableExclGrouped.Columns[_iColInx].DataType.FullName == "System.Decimal");

                    _PdfCell = new PdfPCell(
                        new Paragraph(
                            new Chunk(
                               Conv.toUpperFirst(_DataTableExclGrouped.Columns[_iColInx].ColumnName.Replace("_", " ")),
                                    FontFactory.GetFont(
                                        BaseFont.HELVETICA,
                                       10,
                                        1,
                                        new BaseColor(0, 0, 10)
                                    )
                                )
                            )
                        );
                    _PdfCell.Border = Rectangle.NO_BORDER;
                    _PdfCell.Padding = 4;
                    _PdfCell.PaddingLeft = 3;
                    _PdfCell.PaddingBottom = 6;
                    _PdfCell.HorizontalAlignment = isNumber ? Element.ALIGN_RIGHT : Element.ALIGN_MIDDLE;
                    _PdfCell.VerticalAlignment = Element.ALIGN_JUSTIFIED;
                    _PdfTable.AddCell(_PdfCell);
                }
                int _iIndex = 0;
                BaseColor _AltBgColor = new BaseColor(240, 240, 240);
                Dictionary<string, string> _PreviousValues = new Dictionary<string, string>();
                Dictionary<string, decimal> _TotalValues = new Dictionary<string, decimal>();
                int _iIndentationIncrement = 8;
                foreach (DataRow _DataRow in _DataTable.Rows) {
                    if (_sGroupByCols != null && _sGroupByCols.Length > 0) {
                        for (int _iI = _sGroupByCols.Length - 1; _iI >= 0; _iI--) {
                            string _sGroupCol = _sGroupByCols[_iI];
                            if (_TotalValues.ContainsKey(_sGroupCol) && _PreviousValues[_sGroupCol] != _DataRow[_sGroupCol].ToString()) {
                                _PdfCell = new PdfPCell(
                                    new Paragraph(
                                        new Chunk(
                                             _PreviousValues[_sGroupCol].ToString() + " total",
                                                FontFactory.GetFont(
                                                    BaseFont.HELVETICA,
                                                     _Font.Size,
                                                    1,
                                                    new BaseColor(0, 0, 10)
                                                )
                                            )
                                        )
                                    );
                                _PdfCell.PaddingLeft = (_sGroupByCols.ToList().IndexOf(_sGroupCol) + 1) * _iIndentationIncrement;
                                _PdfCell.Colspan = _iColCount - 1;
                                _PdfCell.Border = Rectangle.NO_BORDER;
                                _PdfTable.AddCell(_PdfCell);
                                //
                                _PdfCell = new PdfPCell(
                                    new Paragraph(
                                        new Chunk(
                                             Conv.toNum(_TotalValues[_sGroupCol].ToString()),
                                                FontFactory.GetFont(
                                                    BaseFont.HELVETICA,
                                                     _Font.Size,
                                                    1,
                                                    new BaseColor(0, 0, 10)
                                                )
                                            )
                                        )
                                    );
                                _PdfCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                _PdfCell.PaddingLeft = (_sGroupByCols.ToList().IndexOf(_sGroupCol) + 1) * _iIndentationIncrement;
                                _PdfCell.Border = Rectangle.NO_BORDER;
                                _PdfTable.AddCell(_PdfCell);
                                _iIndex = 0;
                                _TotalValues[_sGroupCol] = 0;
                            }
                        }
                        foreach (string _sGroupCol in _sGroupByCols) {
                            if (!_PreviousValues.ContainsKey(_sGroupCol) || _PreviousValues[_sGroupCol] != _DataRow[_sGroupCol].ToString()) {
                                _PdfCell = new PdfPCell(
                                    new Paragraph(
                                        new Chunk(
                                             _DataRow[_sGroupCol].ToString(),
                                                FontFactory.GetFont(
                                                    BaseFont.HELVETICA,
                                                     _Font.Size,
                                                    1,
                                                    new BaseColor(0, 0, 10)
                                                )
                                            )
                                        )
                                    );
                                _PdfCell.PaddingLeft = (_sGroupByCols.ToList().IndexOf(_sGroupCol) + 1) * _iIndentationIncrement;
                                _PdfCell.Colspan = _iColCount;
                                _PdfCell.Border = Rectangle.NO_BORDER;
                                _PdfTable.AddCell(_PdfCell);
                                _iIndex = 0;
                            }
                            if (_PreviousValues.ContainsKey(_sGroupCol)) {
                                _PreviousValues[_sGroupCol] = _DataRow[_sGroupCol].ToString();
                            } else {
                                _PreviousValues.Add(_sGroupCol, _DataRow[_sGroupCol].ToString());
                            }
                            if (_TotalValues.ContainsKey(_sGroupCol)) {
                                _TotalValues[_sGroupCol] += decimal.Parse(_DataRow[_sTotallingColumn].ToString());
                            } else {
                                _TotalValues.Add(_sGroupCol, decimal.Parse(_DataRow[_sTotallingColumn].ToString()));
                            }
                        }
                    }
                    _BgColour = (
                        _iIndex++ % 2 > 0
                        ? new BaseColor(255, 255, 255)
                        : _AltBgColor
                    );
                    int _iCount = 0;
                    foreach (DataColumn _Col in _DataTableExclGrouped.Columns) {
                        bool _bIsInt = false;
                        if (_DataTableExclGrouped.Columns[_Col.ColumnName].DataType.FullName == "System.Int32"
                                    || _DataTableExclGrouped.Columns[_Col.ColumnName].DataType.FullName == "System.Int16") {
                            _bIsInt = true;
                        }
                        bool isNumber = (_DataTableExclGrouped.Columns[_Col.ColumnName].DataType.FullName == "System.Int32"
                            || _DataTableExclGrouped.Columns[_Col.ColumnName].DataType.FullName == "System.Int16"
                            || _DataTableExclGrouped.Columns[_Col.ColumnName].DataType.FullName == "System.Double"
                            || _DataTableExclGrouped.Columns[_Col.ColumnName].DataType.FullName == "System.Decimal");

                        bool isDate = _DataTableExclGrouped.Columns[_Col.ColumnName].DataType.FullName == "System.Date" || _DataTableExclGrouped.Columns[_Col.ColumnName].DataType.FullName == "System.DateTime";
                        _PdfCell = new PdfPCell(
                            new Paragraph(
                                new Chunk(
                                     isNumber ? Conv.toNum(_DataRow[_Col.ColumnName].ToString(), false, _bIsInt ? 0 : 2, "-") : isDate ? Conv.toDate(_DataRow[_Col.ColumnName].ToString(), false, true) : _DataRow[_Col.ColumnName].ToString(),
                                        FontFactory.GetFont(
                                            BaseFont.HELVETICA,
                                            _Font.Size,
                                            0,
                                            new BaseColor(0, 0, 10)
                                        )
                                    )
                                )
                            );
                        _PdfCell.Border = Rectangle.NO_BORDER;
                        _PdfCell.Padding = 5;
                        if (++_iCount == 1 && _bGrouped) {
                            _PdfCell.PaddingLeft = (_sGroupByCols.Length + 1) * _iIndentationIncrement;
                        } else {
                            _PdfCell.PaddingLeft = 3;
                        }
                        if (_DataTableExclGrouped.Columns[_Col.ColumnName].DataType.FullName == "System.Int32"
                            || _DataTableExclGrouped.Columns[_Col.ColumnName].DataType.FullName == "System.Int16"
                            || _DataTableExclGrouped.Columns[_Col.ColumnName].DataType.FullName == "System.Double"
                            || _DataTableExclGrouped.Columns[_Col.ColumnName].DataType.FullName == "System.Decimal") {
                            _PdfCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        } else {
                            _PdfCell.HorizontalAlignment = Element.ALIGN_MIDDLE;
                        }
                        _PdfCell.BackgroundColor = _BgColour;
                        _PdfTable.AddCell(_PdfCell);
                        //IsNumericColumn[colInx] = (col.DataType.FullName == "System.Decimal") || (col.DataType.FullName == "System.Int32") || (col.DataType.FullName == "System.Double") || (col.DataType.FullName == "System.Single");
                    }
                }
                if (_sGroupByCols != null && _sGroupByCols.Length > 0) {
                    for (int _iI = _sGroupByCols.Length - 1; _iI >= 0; _iI--) {
                        string _sGroupCol = _sGroupByCols[_iI];
                        _PdfCell = new PdfPCell(
                            new Paragraph(
                                new Chunk(
                                     _PreviousValues[_sGroupCol].ToString() + " total",
                                        FontFactory.GetFont(
                                            BaseFont.HELVETICA,
                                             _Font.Size,
                                            1,
                                            new BaseColor(0, 0, 10)
                                        )
                                    )
                                )
                            );
                        _PdfCell.PaddingLeft = (_sGroupByCols.ToList().IndexOf(_sGroupCol) + 1) * _iIndentationIncrement;
                        _PdfCell.Colspan = _iColCount - 1;
                        _PdfCell.Border = Rectangle.NO_BORDER;
                        _PdfTable.AddCell(_PdfCell);
                        //
                        _PdfCell = new PdfPCell(
                            new Paragraph(
                                new Chunk(
                                     Conv.toNum(_TotalValues[_sGroupCol].ToString()),
                                        FontFactory.GetFont(
                                            BaseFont.HELVETICA,
                                             _Font.Size,
                                            1,
                                            new BaseColor(0, 0, 10)
                                        )
                                    )
                                )
                            );
                        _PdfCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        _PdfCell.PaddingLeft = (_sGroupByCols.ToList().IndexOf(_sGroupCol) + 1) * _iIndentationIncrement;
                        _PdfCell.Border = Rectangle.NO_BORDER;
                        _PdfTable.AddCell(_PdfCell);
                        _iIndex = 0;
                    }
                }
                for (int _iColInx = 0; _iColInx < _iColCount; _iColInx++) {
                    decimal _dColTotal = 0;
                    bool _iIsInt = false;
                    if (_DataTableExclGrouped.Columns[_iColInx].DataType.FullName == "System.Decimal" || _DataTableExclGrouped.Columns[_iColInx].DataType.FullName == "System.Double"
                            || _DataTableExclGrouped.Columns[_iColInx].DataType.FullName == "System.Int32"
                            || _DataTableExclGrouped.Columns[_iColInx].DataType.FullName == "System.Int16") {
                        foreach (DataRow _DataRow in _DataTableExclGrouped.Rows) {
                            try {
                                _dColTotal += (_DataRow[_iColInx] == null ? 0 : decimal.Parse(_DataRow[_iColInx].ToString()));
                            } catch (Exception) {
                                _dColTotal += 0;
                            }
                        }
                        if (_DataTableExclGrouped.Columns[_iColInx].DataType.FullName == "System.Int32"
                                    || _DataTableExclGrouped.Columns[_iColInx].DataType.FullName == "System.Int16") {
                            _iIsInt = true;
                        }
                        _PdfCell = new PdfPCell(
                            new Paragraph(
                                new Chunk(
                                     Conv.toNum(_dColTotal.ToString(), false, _iIsInt ? 0 : 2, "-"),
                                        FontFactory.GetFont(
                                            BaseFont.HELVETICA,
                                            _Font.Size + 1,
                                            1,
                                            new BaseColor(0, 0, 10)
                                        )
                                    )
                                )
                            );
                        _PdfCell.Border = Rectangle.NO_BORDER;
                        _PdfCell.Padding = 3;
                        _PdfCell.PaddingLeft = 0;
                        _PdfCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        _PdfCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        _PdfTable.AddCell(_PdfCell);
                    } else {
                        _PdfCell = new PdfPCell(
                            new Paragraph(
                                new Chunk(
                                     "",
                                        FontFactory.GetFont(
                                            BaseFont.HELVETICA,
                                            _Font.Size + 1,
                                            1,
                                            new BaseColor(0, 0, 10)
                                        )
                                    )
                                )
                            );
                        _PdfCell.Border = Rectangle.NO_BORDER;
                        _PdfCell.Padding = 3;
                        _PdfCell.PaddingLeft = 0;
                        _PdfCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        _PdfCell.HorizontalAlignment = Element.ALIGN_MIDDLE;
                        _PdfTable.AddCell(_PdfCell);
                    }
                }
                _Document.Add(new Paragraph("\n"));
                _Document.Add(_PdfTable);
                _Document.Close();
                return _sPath;//for Js
            } catch (Exception _Ex) {
                //     ErrorLog.handleError( "Class", "PdfExport", "generatePdfReport", _Ex.Message );
            }
            return _sPath;
        }

        public static string generateReferralLetter(Guid _Guid) {
            UnitOfWork _UnitOfWork = new UnitOfWork(new PatientRegisterContext());
            Referral _Referral = _UnitOfWork.Referrals.GetById(_Guid);
            string _sPath = "~/PdfExports/ReferralLetter_" + _Referral.Patient.PatientNo + "_" + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".pdf";
            string _sLogoPath = "~/assets/images/logo_gt.png";
            try {
                var _Document = new Document(PageSize.LETTER_LANDSCAPE);
                Font _Font = new Font(Font.FontFamily.HELVETICA);
                Font _FontHeader = new Font(Font.FontFamily.HELVETICA);
                Font _FontWeight = FontFactory.GetFont(FontFactory.TIMES_BOLD, 22);
                Font _FontWeightSmaller = FontFactory.GetFont(FontFactory.TIMES_BOLD, 12);
                Font _FontUnderline = FontFactory.GetFont(FontFactory.TIMES, 22, Font.UNDERLINE);
                _Font.Size = 10;
                _FontHeader.Size = 18;
                _FontHeader.SetStyle("Bold");
                Paragraph _paragraph = new Paragraph();
                Phrase _phrase = new Phrase();
                _paragraph.Alignment = 1;
                PdfWriter _PdfWriter = PdfWriter.GetInstance(_Document,
                           new FileStream(HttpContext.Current.Server.MapPath(_sPath),
                               FileMode.Create));
                _Document.Open();
                Image _ImgLogo = Image.GetInstance(System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(_sLogoPath)), System.Drawing.Imaging.ImageFormat.Png);
                _ImgLogo.Alignment = Image.LEFT_ALIGN;
                _ImgLogo.ScaleToFit(110f, 70f);
                float[] _fColumnDefinitionSizeHeader = { 65F, 35F };
                PdfPTable _PdfPTable = new PdfPTable(_fColumnDefinitionSizeHeader);
                _PdfPTable.WidthPercentage = 100;
                Paragraph _pEntityNameTag = new Paragraph(
                        new Chunk(
                            Configuration.getValue(EnumConfiguration.practice_name),
                            FontFactory.GetFont(
                                    BaseFont.HELVETICA,
                                    _FontHeader.Size,
                                    1,
                                    new BaseColor(0, 0, 10)
                                )
                            )
                    );
                _pEntityNameTag.Alignment = Element.ALIGN_LEFT;
                Paragraph _pTitle = new Paragraph(
                           new Chunk(
                            "REFERRAL LETTER\n\n",
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       13,
                                       1,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           );
                //Logo
                PdfPCell _PdfPCell1 = new PdfPCell(_ImgLogo);
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.Colspan = 2;
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                _PdfPTable.AddCell(_PdfPCell1);
                //EntityName
                _PdfPCell1 = new PdfPCell(_pEntityNameTag);
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.Colspan = 2;
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                _PdfPTable.AddCell(_PdfPCell1);
                //Dr
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("Dr ", _Referral.Doctor.Surname),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                        _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingTop = 20;
                _PdfPCell1.PaddingBottom = 20;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPTable.AddCell(_PdfPCell1);
                //TEL
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("TEL: ", Constant.PHONE),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                        _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingTop = 20;
                _PdfPCell1.PaddingBottom = 20;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_RIGHT;
                _PdfPTable.AddCell(_PdfPCell1);
                //Title
                _PdfPCell1 = new PdfPCell(_pTitle);
                _PdfPCell1.Border = Rectangle.TOP_BORDER;
                _PdfPCell1.BorderWidth = 3;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 20;
                _PdfPCell1.PaddingTop = 20;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.Colspan = 2;
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                _PdfPTable.AddCell(_PdfPCell1);
                //Dear Dr
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("Dear Dr ", _Referral.ReferredToDoctor, "\n\nThank you for seeing this patient:"),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.PaddingBottom = 25;
                _PdfPCell1.Colspan = 2;
                _PdfPTable.AddCell(_PdfPCell1);
                //Patients Name
                _PdfPCell1 = new PdfPCell(new Paragraph(
                          new Chunk(
                           string.Concat("PATIENT NAME: ", _Referral.Patient.Surname, " ", _Referral.Patient.Name),
                              FontFactory.GetFont(
                                      BaseFont.HELVETICA,
                                       _Font.Size,
                                      0,
                                      new BaseColor(0, 0, 10)
                                  )
                              )
                          ));
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPTable.AddCell(_PdfPCell1);
                //
                _PdfPCell1 = new PdfPCell(new Paragraph(
                         new Chunk(
                          string.Concat("AGE: ", ((int)(DateTime.Today.Subtract(_Referral.Patient.DateOfBirth).TotalDays / 365)).ToString(), "            GENDER: ", _Referral.Patient.Gender.Name),
                             FontFactory.GetFont(
                                     BaseFont.HELVETICA,
                                      _Font.Size,
                                     0,
                                     new BaseColor(0, 0, 10)
                                 )
                             )
                         ));
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPTable.AddCell(_PdfPCell1);
                //Address
                _PdfPCell1 = new PdfPCell(new Paragraph(
                       new Chunk(
                        string.Concat("ADDRESS: ", _Referral.Patient.ResidentialAddress),
                           FontFactory.GetFont(
                                   BaseFont.HELVETICA,
                                    _Font.Size,
                                   0,
                                   new BaseColor(0, 0, 10)
                               )
                           )
                       ));
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPTable.AddCell(_PdfPCell1);
                //
                _PdfPCell1 = new PdfPCell(new Paragraph(
                       new Chunk(
                        string.Concat("DATE: ", Conv.toDate(_Referral.Date.ToString(), false, true), "\n\n"),
                           FontFactory.GetFont(
                                   BaseFont.HELVETICA,
                                    _Font.Size,
                                   0,
                                   new BaseColor(0, 0, 10)
                               )
                           )
                       ));
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPTable.AddCell(_PdfPCell1);
                //History
                _PdfPCell1 = new PdfPCell(new Paragraph(
                       new Chunk(
                        string.Concat("HISTORY: ", _Referral.History),
                           FontFactory.GetFont(
                                   BaseFont.HELVETICA,
                                    _Font.Size,
                                   0,
                                   new BaseColor(0, 0, 10)
                               )
                           )
                       ));
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.PaddingBottom = 15;
                _PdfPCell1.Colspan = 2;
                _PdfPTable.AddCell(_PdfPCell1);
                //Previous Medical History
                _PdfPCell1 = new PdfPCell(new Paragraph(
                       new Chunk(
                        string.Concat("PREVIOUS MEDICAL HISTORY: ", ""), //Get last X records
                           FontFactory.GetFont(
                                   BaseFont.HELVETICA,
                                    _Font.Size,
                                   0,
                                   new BaseColor(0, 0, 10)
                               )
                           )
                       ));
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.PaddingBottom = 15;
                _PdfPCell1.Colspan = 2;
                _PdfPTable.AddCell(_PdfPCell1);
                //Examination and investigation performed
                _PdfPCell1 = new PdfPCell(new Paragraph(
                       new Chunk(
                        string.Concat("EXAMINATION AND INVESTIGATION PERFORMED: ", _Referral.ExaminationAndInvestigation), //Get last X records
                           FontFactory.GetFont(
                                   BaseFont.HELVETICA,
                                    _Font.Size,
                                   0,
                                   new BaseColor(0, 0, 10)
                               )
                           )
                       ));
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.PaddingBottom = 15;
                _PdfPCell1.Colspan = 2;
                _PdfPTable.AddCell(_PdfPCell1);
                //Current Medication
                _PdfPCell1 = new PdfPCell(new Paragraph(
                       new Chunk(
                        string.Concat("CURRENT MEDICATIONn: ", _Referral.CurrentMedication), //Get last X records
                           FontFactory.GetFont(
                                   BaseFont.HELVETICA,
                                    _Font.Size,
                                   0,
                                   new BaseColor(0, 0, 10)
                               )
                           )
                       ));
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.PaddingBottom = 15;
                _PdfPCell1.Colspan = 2;
                _PdfPTable.AddCell(_PdfPCell1);
                //Regards
                _PdfPCell1 = new PdfPCell(new Paragraph(
                       new Chunk(
                        "Kindly assess and assist on further management.\n\n\nThank you.", //Get last X records
                           FontFactory.GetFont(
                                   BaseFont.HELVETICA,
                                    _Font.Size,
                                   0,
                                   new BaseColor(0, 0, 10)
                               )
                           )
                       ));
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.PaddingTop = 20;
                _PdfPCell1.Colspan = 2;
                _PdfPTable.AddCell(_PdfPCell1);
                _Document.Add(_PdfPTable);
                _Document.Close();
                return _sPath;//for Js
            } catch (Exception _Ex) {
                //     ErrorLog.handleError( "Class", "PdfExport", "generatePdfReport", _Ex.Message );
            }
            return _sPath;
        }

        public static string generatePrescriptionDownload(Guid _Guid) {
            UnitOfWork _UnitOfWork = new UnitOfWork(new PatientRegisterContext());
            Prescription _Prescription = _UnitOfWork.Prescriptions.GetById(_Guid);
            string _sPath = "~/PdfExports/Prescription_" + _Prescription.Patient.PatientNo + "_" + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".pdf";
            string _sLogoPath = "~/assets/images/logo_gt.png";
            try {
                var _Document = new Document(PageSize.LETTER_LANDSCAPE);
                Font _Font = new Font(Font.FontFamily.HELVETICA);
                Font _FontHeader = new Font(Font.FontFamily.HELVETICA);
                Font _FontWeight = FontFactory.GetFont(FontFactory.TIMES_BOLD, 22);
                Font _FontWeightSmaller = FontFactory.GetFont(FontFactory.TIMES_BOLD, 12);
                Font _FontUnderline = FontFactory.GetFont(FontFactory.TIMES, 22, Font.UNDERLINE);
                _Font.Size = 10;
                _FontHeader.Size = 18;
                _FontHeader.SetStyle("Bold");
                Paragraph _paragraph = new Paragraph();
                Phrase _phrase = new Phrase();
                _paragraph.Alignment = 1;
                PdfWriter _PdfWriter = PdfWriter.GetInstance(_Document,
                           new FileStream(HttpContext.Current.Server.MapPath(_sPath),
                               FileMode.Create));
                _Document.Open();
                Image _ImgLogo = Image.GetInstance(System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(_sLogoPath)), System.Drawing.Imaging.ImageFormat.Png);
                _ImgLogo.Alignment = Image.LEFT_ALIGN;
                _ImgLogo.ScaleToFit(110f, 70f);
                float[] _fColumnDefinitionSizeHeader = { 65F, 35F };
                PdfPTable _PdfPTable = new PdfPTable(_fColumnDefinitionSizeHeader);
                _PdfPTable.WidthPercentage = 100;
                Paragraph _pEntityNameTag = new Paragraph(
                        new Chunk(
                             Configuration.getValue(EnumConfiguration.practice_name),
                            FontFactory.GetFont(
                                    BaseFont.HELVETICA,
                                    _FontHeader.Size,
                                    1,
                                    new BaseColor(0, 0, 10)
                                )
                            )
                    );
                _pEntityNameTag.Alignment = Element.ALIGN_LEFT;
                Paragraph _pTitle = new Paragraph(
                           new Chunk(
                            "PRESCRIPTION\n\n",
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       13,
                                       1,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           );
                //Logo
                PdfPCell _PdfPCell1 = new PdfPCell(_ImgLogo);
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.Colspan = 2;
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                _PdfPTable.AddCell(_PdfPCell1);
                //EntityName
                _PdfPCell1 = new PdfPCell(_pEntityNameTag);
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.Colspan = 2;
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                _PdfPTable.AddCell(_PdfPCell1);
                //Dr
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("Dr ", _Prescription.Doctor.Surname),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                        _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingTop = 20;
                _PdfPCell1.PaddingBottom = 20;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPTable.AddCell(_PdfPCell1);
                //TEL
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("TEL: ", Constant.PHONE),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                        _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingTop = 20;
                _PdfPCell1.PaddingBottom = 20;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_RIGHT;
                _PdfPTable.AddCell(_PdfPCell1);
                //Title
                _PdfPCell1 = new PdfPCell(_pTitle);
                _PdfPCell1.Border = Rectangle.TOP_BORDER;
                _PdfPCell1.BorderWidth = 3;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 20;
                _PdfPCell1.PaddingTop = 20;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.Colspan = 2;
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                _PdfPTable.AddCell(_PdfPCell1);
                //Patients Name
                _PdfPCell1 = new PdfPCell(new Paragraph(
                          new Chunk(
                           string.Concat("PATIENT NAME: ", _Prescription.Patient.Surname, " ", _Prescription.Patient.Name),
                              FontFactory.GetFont(
                                      BaseFont.HELVETICA,
                                       _Font.Size,
                                      0,
                                      new BaseColor(0, 0, 10)
                                  )
                              )
                          ));
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPTable.AddCell(_PdfPCell1);
                //
                _PdfPCell1 = new PdfPCell(new Paragraph(
                         new Chunk(
                          string.Concat("AGE: ", ((int)(DateTime.Today.Subtract(_Prescription.Patient.DateOfBirth).TotalDays / 365)).ToString(), "             ", "GENDER: ", _Prescription.Patient.Gender.Name),
                             FontFactory.GetFont(
                                     BaseFont.HELVETICA,
                                      _Font.Size,
                                     0,
                                     new BaseColor(0, 0, 10)
                                 )
                             )
                         ));
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPTable.AddCell(_PdfPCell1);
                //Address
                _PdfPCell1 = new PdfPCell(new Paragraph(
                       new Chunk(
                        string.Concat("ADDRESS: ", _Prescription.Patient.ResidentialAddress),
                           FontFactory.GetFont(
                                   BaseFont.HELVETICA,
                                    _Font.Size,
                                   0,
                                   new BaseColor(0, 0, 10)
                               )
                           )
                       ));
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.PaddingBottom = 20;
                _PdfPTable.AddCell(_PdfPCell1);
                //
                _PdfPCell1 = new PdfPCell(new Paragraph(
                       new Chunk(
                        string.Concat("DATE: ", Conv.toDate(_Prescription.Date.ToString(), false, true), "\n\n"),
                           FontFactory.GetFont(
                                   BaseFont.HELVETICA,
                                    _Font.Size,
                                   0,
                                   new BaseColor(0, 0, 10)
                               )
                           )
                       ));
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.PaddingBottom = 20;
                _PdfPTable.AddCell(_PdfPCell1);
                //Rx.
                _PdfPCell1 = new PdfPCell(new Paragraph(
                       new Chunk(
                        string.Concat("Rx: "),
                           FontFactory.GetFont(
                                   BaseFont.HELVETICA,
                                    _FontHeader.Size,
                                   1,
                                   new BaseColor(0, 0, 10)
                               )
                           )
                       ));
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPCell1.Colspan = 2;
                _PdfPTable.AddCell(_PdfPCell1);
                //Stock items
                int _iCount = 0;
                foreach (PrescriptionMedication _PrescriptionMedication in _Prescription.getMedications()) {
                    _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat(
                              ++_iCount, ". ", _PrescriptionMedication.MedicalStockItem.Name, " - ",
                                _PrescriptionMedication.Quantity, " ",
                                _PrescriptionMedication.MeasurementUnit.Name.ToLower(), " for ",
                                Conv.toNum(_PrescriptionMedication.TimeLength.ToString(), _iDec: 0), " ",
                                _PrescriptionMedication.TimeUnit.Name.ToLower()
                            ),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                        _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                    _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                    _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                    _PdfPCell1.Border = Rectangle.NO_BORDER;
                    _PdfPCell1.PaddingBottom = 10;
                    _PdfPCell1.Colspan = 2;
                    _PdfPTable.AddCell(_PdfPCell1);
                }
                _Document.Add(_PdfPTable);
                _Document.Close();
                return _sPath;//for Js
            } catch (Exception _Ex) {
                //     ErrorLog.handleError( "Class", "PdfExport", "generatePdfReport", _Ex.Message );
            }
            return _sPath;
        }

        public static string generateOperationCardDownload(Guid _Guid) {
            UnitOfWork _UnitOfWork = new UnitOfWork(new PatientRegisterContext());
            Operation _Operation = _UnitOfWork.Operations.GetById(_Guid);
            string _sPath = "~/PdfExports/OperationConsent_" + _Operation.Patient.PatientNo + "_" + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".pdf";
            string _sLogoPath = "~/assets/images/logo_gt.png";
            try {
                var _Document = new Document(PageSize.LETTER_LANDSCAPE);
                Font _Font = new Font(Font.FontFamily.HELVETICA);
                Font _FontHeader = new Font(Font.FontFamily.HELVETICA);
                Font _FontWeight = FontFactory.GetFont(FontFactory.TIMES_BOLD, 22);
                Font _FontWeightSmaller = FontFactory.GetFont(FontFactory.TIMES_BOLD, 12);
                Font _FontUnderline = FontFactory.GetFont(FontFactory.TIMES, 22, Font.UNDERLINE);
                _Font.Size = 10;
                _FontHeader.Size = 18;
                _FontHeader.SetStyle("Bold");
                Paragraph _paragraph = new Paragraph();
                Phrase _phrase = new Phrase();
                _paragraph.Alignment = 1;
                PdfWriter _PdfWriter = PdfWriter.GetInstance(_Document,
                           new FileStream(HttpContext.Current.Server.MapPath(_sPath),
                               FileMode.Create));
                _Document.Open();
                Image _ImgLogo = Image.GetInstance(System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(_sLogoPath)), System.Drawing.Imaging.ImageFormat.Png);
                _ImgLogo.Alignment = Image.LEFT_ALIGN;
                _ImgLogo.ScaleToFit(110f, 70f);
                float[] _fColumnDefinitionSizeHeader = { 65F, 35F };
                PdfPTable _PdfPTable = new PdfPTable(_fColumnDefinitionSizeHeader);
                _PdfPTable.WidthPercentage = 100;
                Paragraph _pEntityNameTag = new Paragraph(
                        new Chunk(
                             Configuration.getValue(EnumConfiguration.practice_name),
                            FontFactory.GetFont(
                                    BaseFont.HELVETICA,
                                    _FontHeader.Size,
                                    1,
                                    new BaseColor(0, 0, 10)
                                )
                            )
                    );
                _pEntityNameTag.Alignment = Element.ALIGN_LEFT;
                Paragraph _pTitle = new Paragraph(
                           new Chunk(
                            "OPERATION CARD\n\n",
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       13,
                                       1,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           );
                //Logo
                PdfPCell _PdfPCell1 = new PdfPCell(_ImgLogo);
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.Colspan = 2;
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                _PdfPTable.AddCell(_PdfPCell1);
                //EntityName
                _PdfPCell1 = new PdfPCell(_pEntityNameTag);
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.Colspan = 2;
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                _PdfPTable.AddCell(_PdfPCell1);
                //Dr
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("Dr ", _Operation.Doctor.Surname),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                        _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingTop = 20;
                _PdfPCell1.PaddingBottom = 20;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_LEFT;
                _PdfPTable.AddCell(_PdfPCell1);
                //TEL
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("TEL: ", Constant.PHONE),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                        _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingTop = 20;
                _PdfPCell1.PaddingBottom = 20;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_RIGHT;
                _PdfPTable.AddCell(_PdfPCell1);
                //Title
                _PdfPCell1 = new PdfPCell(_pTitle);
                _PdfPCell1.Border = Rectangle.TOP_BORDER;
                _PdfPCell1.BorderWidth = 3;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 20;
                _PdfPCell1.PaddingTop = 20;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.Colspan = 2;
                _PdfPCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                _PdfPTable.AddCell(_PdfPCell1);
                //FULL NAME
                _PdfPCell1 = new PdfPCell(new Paragraph(
                          new Chunk(
                           string.Concat("FULL NAME: ", _Operation.Patient.Surname, " ", _Operation.Patient.Name),
                              FontFactory.GetFont(
                                      BaseFont.HELVETICA,
                                      _Font.Size,
                                      0,
                                      new BaseColor(0, 0, 10)
                                  )
                              )
                          ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPCell1.Colspan = 2;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPTable.AddCell(_PdfPCell1);
                //DOB
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("DATE OF BIRTH: ", Conv.toDate(_Operation.Patient.DateOfBirth.ToString(), false, true)),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPTable.AddCell(_PdfPCell1);
                //ID NUMBER
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("ID NUMBER: ", _Operation.Patient.IDNumber),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPTable.AddCell(_PdfPCell1);
                //Address
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("ADDRESS: ", _Operation.Patient.ResidentialAddress),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.Colspan = 2;
                _PdfPTable.AddCell(_PdfPCell1);
                //GENDER ETC
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("GENDER: ", _Operation.Patient.Gender.Name, "               AGE: ", ((int)(DateTime.Today.Subtract(_Operation.Patient.DateOfBirth).TotalDays / 365)).ToString(),
                            "                TEL: ", _Operation.Patient.CellNo),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.Colspan = 2;
                _PdfPTable.AddCell(_PdfPCell1);
                //DR ETC
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("DATE: ", Conv.toDate(_Operation.Date.ToString(), false, true),
                            "                TIME: ", Conv.toDate(_Operation.StartTime.ToString(), _sFormat: "HH:mm")),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 20;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.Colspan = 2;
                _PdfPTable.AddCell(_PdfPCell1);
                //PREMEDICATION
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("PREMEDICATION: ", _Operation.Premedication),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.Colspan = 2;
                _PdfPTable.AddCell(_PdfPCell1);
                //OPERATION PERFORMED
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("OPERATION PERFORMED: ", _Operation.OperationName),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.Colspan = 2;
                _PdfPTable.AddCell(_PdfPCell1);
                //ANAESTHETIC
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("ANAESTHETIC: ", _Operation.Anaesthetic, "\n\n"),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 20;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.Colspan = 2;
                _PdfPTable.AddCell(_PdfPCell1);
                //SIGNATURES
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("SIGNATURES: \n"),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       _Font.Size,
                                       1,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.Colspan = 2;
                _PdfPTable.AddCell(_PdfPCell1);
                //SURGEON
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("SURGEON:________________________"),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.Colspan = 2;
                _PdfPTable.AddCell(_PdfPCell1);
                //ASST SURGEON
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("ASST SURGEON:____________________"),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.Colspan = 2;
                _PdfPTable.AddCell(_PdfPCell1);
                //ANAESTHETIST
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("ANAESTHETIST:____________________"),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 20;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.Colspan = 2;
                _PdfPTable.AddCell(_PdfPCell1);
                //COMMENCEMENT TIME
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("TIME OF COMMENCEMENT: ", Conv.toDate(_Operation.StartTime.ToString(), _sFormat: "HH:mm")),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 10;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPTable.AddCell(_PdfPCell1);
                //END TIME
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("TIME COMPLETED: ", Conv.toDate(_Operation.EndTime.ToString(), _sFormat: "HH:mm")),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingBottom = 20;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPTable.AddCell(_PdfPCell1);
                //REMARKS
                _PdfPCell1 = new PdfPCell(new Paragraph(
                           new Chunk(
                            string.Concat("REMARKS: ", _Operation.Remarks),
                               FontFactory.GetFont(
                                       BaseFont.HELVETICA,
                                       _Font.Size,
                                       0,
                                       new BaseColor(0, 0, 10)
                                   )
                               )
                           ));
                _PdfPCell1.Border = Rectangle.NO_BORDER;
                _PdfPCell1.VerticalAlignment = Element.ALIGN_TOP;
                _PdfPCell1.PaddingLeft = 0;
                _PdfPCell1.Colspan = 2;
                _PdfPTable.AddCell(_PdfPCell1);
                _Document.Add(_PdfPTable);
                _Document.Close();
                return _sPath;//for Js
            } catch (Exception _Ex) {
                //     ErrorLog.handleError( "Class", "PdfExport", "generatePdfReport", _Ex.Message );
            }
            return _sPath;
        }
    }
}