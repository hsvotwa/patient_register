﻿public class SessionUserMdl {
    #region Properties
    public string UserKey { get; set; }
    public string BranchKey { get; set; }
    public string Email { get; set; }
    public string Fullname { get; set; }
    public string FirstName { get; set; }
    public int StaffType { get; set; }
    #endregion
}