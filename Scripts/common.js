﻿var g_get_method = "GET";
var g_post_method = "POST";
var g_exception_message = "Oops, sorry about that! There was an error while processing your request. Please retry :/";

var RequestType = {
    login: 1,
    save_branch: 2,
    save_diagnosis: 3,
    save_symptom: 4,
    save_medical_stock: 5,
    save_medica_aid_company: 6,
    get_list: 7,
    file_upload: 8,
    save_user: 9,
    save_patient: 10,
    save_consultation: 11,
    save_operation: 12,
    save_prescription: 13,
    save_referral: 14,
    save_settings: 15,
    change_password: 16,
    reset_password: 17,
    delete_file: 999,
    other: 1000
};

var Configuration = {
    practice_name: 2,
    referral_record_count: 3
};

var FieldListType = {
    all: 1,
    required: 2
};

var YesNo = {
    yes: 1,
    no: 2
};


var ResponseStatus = {
    success: 1,
    failed: 2,
    error: 3,
    warning: 4, //
    info: 5
};

function toggleLoader(show) {
    if (!show) {
        $("#loader_image").css("display", "none");
    } else {
        $("#loader_image").css("display", "block");
    }
}

function uploadOnSelect(type_id, field_name, link_guid) {
    var files = $("#" + field_name)[0].files;
    if (files.length > 0) {
        if (window.FormData !== undefined) {
            var data = new FormData();
            for (var x = 0; x < files.length; x++) {
                data.append(files[x].name, files[x]);
            }
            $.ajax({
                type: "POST",
                url: '/File/Create/?id=' + link_guid + "&type_id=" + type_id,
                contentType: false,
                processData: false,
                data: data,
                success: function (result) {
                    handleSuccessResponse(RequestType.file_upload, result);
                    refreshFileList();
                    $("#upload").val('');
                },
                error: function (xhr, status, p3, p4) {
                    var err = "Error " + " " + status + " " + p3 + " " + p4;
                    if (xhr.responseText && xhr.responseText[0] === "{")
                        err = JSON.parse(xhr.responseText).Message;
                    handleErrorResponse();
                }
            });
        } else {
            alert("This browser doesn't support HTML5 file uploads!");
        }
    }
}

function getListFiles(link_guid, type_id) {
    var Data = {
        LinkGuid: link_guid,
        EntityTypeID: type_id
    };
    handleRequest(RequestType.get_list, '/File/ListView/', Data, "FileList");
}

function removeFile(record_guid) {
    var Data = {
        _Guid: record_guid
    };
    handleRequest(RequestType.delete_file, '/File/Delete/', Data);
}

function deterReqFieldList(request_type) {
    var field_list = "";
    switch (parseInt(request_type)) {
        case RequestType.save_branch:
        case RequestType.save_diagnosis:
        case RequestType.save_medical_stock:
        case RequestType.save_medica_aid_company:
        case RequestType.save_symptom:
            return "Name,StatusID";
        case RequestType.save_user:
            return "Name,StatusID,Surname,StaffTypeID,Email";
        case RequestType.save_patient:
            return "Name,Surname,GenderID,DateOfBirth,IDNumber,ResidentialAddress,EmergencyContactName,EmergencyContactPhone";
        case RequestType.save_consultation:
            return "Date,PatientID,DoctorID,Temperature,SummaryComment,DetailedComment";
        case RequestType.save_operation:
            return "PatientID,DoctorID,Date,Premedication,OperationName,StartTime,EndTime,Remarks,SurgeonID";
        case RequestType.save_referral:
            return "PatientID,DoctorID,Date,ReferredToDoctor,History,ExaminationAndInvestigation,SummaryComment";
        case RequestType.save_prescription:
            return "PatientID,DoctorID,Date";
        case RequestType.save_settings:
            return Configuration.practice_name.toString() + "," + Configuration.referral_record_count.toString();
        case RequestType.change_password:
            return "CurrentPassword,NewPassword,ConfirmNewPassword";
        case RequestType.login:
            return "Email,Password";
        case RequestType.reset_password:
            return "Email";
    }
    return field_list;
}

function clearFields(request_type) {
    var field_list = deterReqFieldList(request_type);
    //switch (parseInt(request_type)) {
    //    case RequestType.register_user:
    //        field_list = field_list + "Password_Repeat";
    //        break;
    //}
    var fields = field_list.split(',');
    var out_data = new Object();
    for (var field in fields) {
        field_name = fields[field];
        var input_type = $('#' + field_name).attr('type');
        if (input_type !== "radio" && input_type !== "checkbox") {
            $('#' + field_name).val("");
        }
    }
    return true;
}

function handleRequest(request_type, url, req_model_or_form, sender_control) {
    if (!isReqFieldsValid(request_type) || !doOtherValidation(request_type)) {
        return false;
    }
    toggleLoader(true);
    var return_data = null;
    var in_data = typeof req_model_or_form === "object" ? req_model_or_form : $("#" + req_model_or_form).serialize(); //$("#" + req_model_or_form).length ? $("#" + req_model_or_form).serialize() : req_model_or_form;
    var save_data = $.ajax({
        type: deterMethod(request_type),
        url: url,
        data: in_data,
        success: function (result) {
            return_data = result;
            handleSuccessResponse(request_type, return_data, sender_control);
        },
        complete: function () {
            toggleLoader(false);
        },
        error: function (data) {
            handleErrorResponse();
        }
    });
    //  save_data.error(function (data) { handleErrorResponse(); });
}

function deterMethod(request_type) {
    switch (parseInt(request_type)) {
        case RequestType.save_branch:
        case RequestType.save_diagnosis:
        case RequestType.save_medical_stock:
        case RequestType.save_medica_aid_company:
        case RequestType.save_symptom:
        case RequestType.delete_file:
        case RequestType.login:
        case RequestType.reset_password:
            return g_post_method;
        case RequestType.get_list:
            return g_get_method;
    }
    return g_post_method;
}

function handleErrorResponse() {
    showErrorNotification(g_exception_message);
}

function handleSuccessResponse(request_type, return_data, feedback_control) {
    try {
        switch (parseInt(request_type)) {
            case RequestType.get_list:
                displayResponseAsHTML(request_type, return_data, feedback_control);
                return;
        }
        //The rest
        var _Obj = return_data;
        if (_Obj.ResponseStatus === ResponseStatus.success) {
            showSuccessNotification(_Obj.Description);
        } else if (_Obj.ResponseStatus === ResponseStatus.info) {
            showInfoNotification(_Obj.Description);
        } else {
            showErrorNotification(_Obj.Description);
        }
        doOtherResponseTask(request_type, _Obj.ResponseStatus);
        if (_Obj.JsFunc !== "") {
            setTimeout(function () {
                eval(_Obj.JsFunc);
            }, 4000);
        }
    } catch (_Ex) {
        showErrorNotification(g_exception_message);
    }
    return true;
}

function displayResponseAsHTML(request_type, return_data, feedback_control) {
    if (typeof (feedback_control) === "undefined") {
        feedback_control = "SearchResult";
    }
    switch (parseInt(request_type)) {
        case RequestType.get_list:
            $("#" + feedback_control).html(return_data);
            break;
    }
}

function doOtherResponseTask(request_type, response_status) {
    switch (parseInt(request_type)) {
        case RequestType.save_patient:
            if (response_status === ResponseStatus.success) {
                refreshFileList();
                $('#files_section_title').fadeIn();
                $('#files_section_upload').fadeIn();
                $('#files_section_uploaded').fadeIn();
                $('#files_section_new').fadeIn();
            }
            break;
        case RequestType.save_consultation:
            if (response_status === ResponseStatus.success) {
                $("#SymptomDiagnosisBlock").fadeIn();
                $("#MedicationBlock").fadeIn();
                refreshFileList();
                $('#files_section_title').fadeIn();
                $('#files_section_upload').fadeIn();
                $('#files_section_uploaded').fadeIn();
                $('#files_section_new').fadeIn();
            }
            break;
        case RequestType.save_prescription:
            if (response_status === ResponseStatus.success) {
                refreshFileList();
                $("#MedicationBlock").fadeIn();
                $('#files_section_title').fadeIn();
                $('#files_section_upload').fadeIn();
                $('#files_section_uploaded').fadeIn();
                $('#files_section_new').fadeIn();
            }
            break;
        case RequestType.save_user:
            if (response_status === ResponseStatus.success) {
                refreshFileList();
                $('#files_section_title').fadeIn();
                $('#files_section_upload').fadeIn();
                $('#files_section_uploaded').fadeIn();
                $('#files_section_new').fadeIn();
            }
            break;
        case RequestType.save_referral:
            if (response_status === ResponseStatus.success) {
                refreshFileList();
                $('#files_section_title').fadeIn();
                $('#files_section_upload').fadeIn();
                $('#files_section_uploaded').fadeIn();
                $('#files_section_new').fadeIn();
            }
            break;
        case RequestType.save_operation:
            if (response_status === ResponseStatus.success) {
                refreshFileList();
                $('#files_section_title').fadeIn();
                $('#files_section_upload').fadeIn();
                $('#files_section_uploaded').fadeIn();
                $('#files_section_new').fadeIn();
            }
            break;
        case RequestType.delete_file:
            if (response_status === ResponseStatus.success) {
                refreshFileList();
            }
            break;
        case RequestType.change_password:
            if (response_status === ResponseStatus.success) {
                $('#CurrentPassword').val('');
                $('#NewPassword').val('');
                $('#ConfirmNewPassword').val('');
            }
            break;
    }
    return true;
}

function showSuccessNotification(notification_text) {
    Snackbar.show({
        text: notification_text,
        pos: 'top-center',
        showAction: false,
        actionText: "Dismiss",
        duration: 8000,
        textColor: '#000',
        backgroundColor: '#ccffcc',
        animation: "fadein 0.5s, fadeout 0.5s 2.5s"
        //customClass: "succ_mess"
    });
}

function showErrorNotification(notification_text) {
    Snackbar.show({
        text: notification_text,
        pos: 'top-center',
        showAction: false,
        actionText: "Dismiss",
        duration: 8000,
        textColor: '#000',
        backgroundColor: '#f9e0e0',
        animation: "fadein 0.5s, fadeout 0.5s 2.5s"
        //customClass: "err_mess"
    });
}

function showWarningNotification(notification_text) {
    Snackbar.show({
        text: notification_text,
        pos: 'top-center',
        showAction: false,
        actionText: "Dismiss",
        duration: 8000,
        textColor: '#000',
        backgroundColor: '#fee9d5',
        animation: "fadein 0.5s, fadeout 0.5s 2.5s"
        //customClass: "warn_mess"
    });
}

function showInfoNotification(notification_text) {
    Snackbar.show({
        text: notification_text,
        pos: 'top-center',
        showAction: false,
        actionText: "Dismiss",
        duration: 8000,
        textColor: '#000',
        backgroundColor: '#f90caf9',
        animation: "fadein 0.5s, fadeout 0.5s 2.5s"
        //customClass: "err_mess"
    });
}

function doOtherValidation(request_type) {
    var return_val = true;
    var err_message = "";
    switch (parseInt(request_type)) {
        case RequestType.save_patient:
            if ($("#MedicalAidCompanyID").val() !== "" && $("#MedicalAidCompanyID").val() !== "0") {
                if (!validFields('MedicalAidMainMemberIDNo,MedicalAidMainMemberIDNo,MedicalAidNo,MedicalAidMainMemberPhone,MedicalAidMainMemberRelationshipID')) {
                    err_message = "Since you specified a Medical Aid Company, all detail under the Medical Aid section is compulsory.";
                    return_val = false;
                }
            }
            break;
        case RequestType.change_password:
            if (($("#NewPassword").val() === "" || $("#ConfirmNewPassword").val() === "")) {
                return_val = false;
                err_message = "You should specify the New Password, since the Current Password field is not empty.";
                setFieldValidity("NewPassword", false);
                setFieldValidity("ConfirmNewPassword", false);
            }
            if ($("#NewPassword").val() !== $("#ConfirmNewPassword").val()) {
                return_val = false;
                err_message = "The two passwords fields must contain the same text.";
                setFieldValidity("NewPassword", false);
                setFieldValidity("ConfirmNewPassword", false);
            }
            break;
    }
    if (!return_val) {
        showErrorNotification(err_message);
    }
    return return_val;
}

function isReqFieldsValid(request_type) {
    return validFields(deterReqFieldList(request_type));
}
function validFields(check_fields) {
    if (check_fields === "") {
        return true;
    }
    var field_value = '',
        fields = check_fields.split(','),
        fields_least_one,
        return_value = true,
        all_is_empty = true,
        field_name = '',
        focus_field = '';
    for (var field in fields) {
        field_name = fields[field];
        if (field_name.indexOf(':') === -1) {
            field_value = document.getElementById(field_name).value;
            if (field_value === ''
                || field_value === '0') {
                if (!document.getElementById(field_name).disabled
                    && focus_field === '') {
                    focus_field = field_name;
                }
                setFieldValidity(field_name, false);
                if (return_value) {
                    return_value = false;
                }
            } else {
                setFieldValidity(field_name, true);
            }
        }
    }
    if (focus_field !== '') {
        $('#' + focus_field).focus();
    }
    if (!return_value) {
        showErrorNotification("The highlighted fields are required, and should be completed.");
    }
    return return_value;
}

function setFieldValidity(field_name, is_valid) {
    if (!is_valid) {
        $('#' + field_name).css("background-color", "#ffe6e6");
        bindValidatedField(field_name);
        $('#' + field_name).focus();
    } else {
        $('#' + field_name).css("background-color", "");
    }
}

function bindValidatedField(bind_fields) {
    var fields = bind_fields.split(',');
    for (var field in fields) {
        var field_name = fields[field];
        $('#' + field_name).bind('change', function () {
            if (this.value !== ''
                && this.value !== '0') {
                $('#' + this.id)
                    .css("background-color", "");
            } else {
                $('#' + this.id)
                    .css("background-color", "#ffe6e6");
            }
        });
    }
}

function updateMenuItemAsActive(menu_id) {
    if ($('#' + menu_id).length) {
        $('#' + menu_id).addClass("active");
    }
}

function downloadFileFromRequest(path, data) {
    $.ajax({
        type: "POST",
        url: path,
        data: data,
        success: function (result) {
            var link = document.createElement('a');
            link.href = result.FilePath;
            link.download = result.FileName;
            link.dispatchEvent(new MouseEvent('click'));
        },
        error: function (xhr, status, p3, p4) {
            var err = "Error " + " " + status + " " + p3 + " " + p4;
            if (xhr.responseText && xhr.responseText[0] === "{")
                err = JSON.parse(xhr.responseText).Message;
            handleErrorResponse();
        }
    });
}

function isEmpty(value) {
    //Undefined or null
    if (typeof (value) === 'undefined'
        || value === null) {
        return true;
    }

    //Arrays && Strings
    if (typeof (value.length) !== 'undefined') {
        return value.length === 0;
    }

    //Numbers or boolean
    if (typeof (value) === 'number'
        || typeof (value) === 'boolean') {
        return false;
    }

    //Objects
    var count = 0;

    for (var i in value) {
        if (value.hasOwnProperty(i)) {
            count++;
        }
    }
    return count === 0;
}

function datePickerDateToSql(value) {
    var _return = "";
    if (!isEmpty(value)) {
        value = value.split("/");
        if (value.length === 3) {
            _return = value[2] + "-" + value[1] + "-" + value[0];
        }
    }
    return _return;
}

//Start Consultation functions
function addSymptom() {
    if (!validFields('SymptomID')) {
        return;
    }
    toggleLoader(true);
    var return_data = null;
    var Data = {
        SymptomID: $("#SymptomID").val(),
        ConsultationID: $("#ConsultationID").val(),
        Deleted: YesNo.no
    };
    $.ajax({
        type: "POST",
        url: "/Consultation/SaveSymptom/",
        data: Data,
        success: function (result) {
            return_data = result;
            handleSuccessResponse(RequestType.other, return_data, null);
            if (return_data.ResponseStatus === ResponseStatus.success) {
                getListSymptom();
                $("#SymptomID option[value*='" + $("#SymptomID").val() + "']").prop('disabled', true);
                $("#SymptomID").val('');
            }
        },
        complete: function () {
            toggleLoader(false);
        },
        error: function (data) {
            handleErrorResponse();
        }
    });
}

function addDiagnosis() {
    if (!validFields('DiagnosisID')) {
        return;
    }
    toggleLoader(true);
    var return_data = null;
    var Data = {
        DiagnosisID: $("#DiagnosisID").val(),
        ConsultationID: $("#ConsultationID").val(),
        Deleted: YesNo.no
    };
    $.ajax({
        type: "POST",
        url: "/Consultation/SaveDiagnosis/",
        data: Data,
        success: function (result) {
            return_data = result;
            handleSuccessResponse(RequestType.other, return_data, null);
            if (return_data.ResponseStatus === ResponseStatus.success) {
                getListDiagnosis();
                $("#DiagnosisID option[value*='" + $("#DiagnosisID").val() + "']").prop('disabled', true);
                $("#DiagnosisID").val('');
            }
        },
        complete: function () {
            toggleLoader(false);
        },
        error: function (data) {
            handleErrorResponse();
        }
    });
}

function addMedication() {
    if (!validFields('MedicationID,Quantity,MeasurementUnitID,TimeLength,TimeUnitID')) {
        return;
    }
    if (isNaN($("#Quantity").val()) || parseFloat($("#Quantity").val()) <= 0) {
        showErrorNotification("Quantity is compulsory and must be greater than zero.");
        return;
    }
    if (isNaN($("#TimeLength").val()) || parseFloat($("#TimeLength").val()) <= 0) {
        showErrorNotification("Dosage Period is compulsory and must be greater than zero.");
        return;
    }
    toggleLoader(true);
    var return_data = null;
    var Data = {
        ConsultationID: $("#ConsultationID").val(),
        MedicalStockItemID: $("#MedicationID").val(),
        Quantity: $("#Quantity").val(),
        MeasurementUnitID: $("#MeasurementUnitID").val(),
        TimeLength: $("#TimeLength").val(),
        TimeUnitID: $("#TimeUnitID").val(),
        Deleted: YesNo.no
    };
    $.ajax({
        type: "POST",
        url: "/Consultation/SaveMedication/",
        data: Data,
        success: function (result) {
            return_data = result;
            handleSuccessResponse(RequestType.other, return_data, null);
            if (return_data.ResponseStatus === ResponseStatus.success) {
                getListMedication();
                $("#MedicationID option[value*='" + $("#MedicationID").val() + "']").prop('disabled', true);
                $("#MedicationID").val('');
                $("#Quantity").val('');
                $("#MeasurementUnitID").val('');
                $("#TimeLength").val('');
                $("#TimeUnitID").val('');
            }
        },
        complete: function () {
            toggleLoader(false);
        },
        error: function (data) {
            handleErrorResponse();
        }
    });
}

function removeSymptom(record_guid, dropdown_value) {
    toggleLoader(true);
    var return_data = null;
    var Data = {
        ConsultationSymptomID: record_guid,
        Deleted: YesNo.yes
    };
    $.ajax({
        type: "POST",
        url: "/Consultation/SaveSymptom/",
        data: Data,
        success: function (result) {
            return_data = result;
            handleSuccessResponse(RequestType.other, return_data, null);
            if (return_data.ResponseStatus === ResponseStatus.success) {
                getListSymptom();
                $("#SymptomID option[value*='" + dropdown_value + "']").prop('disabled', '');
            }
        },
        complete: function () {
            toggleLoader(false);
        },
        error: function (data) {
            handleErrorResponse();
        }
    });
}

function removeDiagnosis(record_guid, dropdown_value) {
    toggleLoader(true);
    var return_data = null;
    var Data = {
        ConsultationDiagnosisID: record_guid,
        Deleted: YesNo.yes
    };
    $.ajax({
        type: "POST",
        url: "/Consultation/SaveDiagnosis/",
        data: Data,
        success: function (result) {
            return_data = result;
            handleSuccessResponse(RequestType.other, return_data, null);
            if (return_data.ResponseStatus === ResponseStatus.success) {
                getListDiagnosis();
                $("#DiagnosisID option[value*='" + dropdown_value + "']").prop('disabled', '');
            }
        },
        complete: function () {
            toggleLoader(false);
        },
        error: function (data) {
            handleErrorResponse();
        }
    });
}

function removeMedication(record_guid, dropdown_value) {
    toggleLoader(true);
    var return_data = null;
    var Data = {
        ConsultationMedicationID: record_guid,
        Deleted: YesNo.yes
    };
    $.ajax({
        type: "POST",
        url: "/Consultation/SaveMedication/",
        data: Data,
        success: function (result) {
            return_data = result;
            handleSuccessResponse(RequestType.other, return_data, null);
            if (return_data.ResponseStatus === ResponseStatus.success) {
                getListMedication();
                $("#MedicationID option[value*='" + dropdown_value + "']").prop('disabled', '');
            }
        },
        complete: function () {
            toggleLoader(false);
        },
        error: function (data) {
            handleErrorResponse();
        }
    });
}

function getListSymptom() {
    handleRequest(RequestType.get_list, '/Consultation/SymptomList/?_ConsultationGuid=' + $("#ConsultationID").val(), null, "SymptomList");
}

function getListDiagnosis() {
    handleRequest(RequestType.get_list, '/Consultation/DiagnosisList/?_ConsultationGuid=' + $("#ConsultationID").val(), null, "DiagnosisList");
}

function getListMedication() {
    handleRequest(RequestType.get_list, '/Consultation/MedicationList/?_ConsultationGuid=' + $("#ConsultationID").val(), null, "MedicationList");
}

function refreshAllLinkedRecords() {
    $("#SymptomDiagnosisBlock").fadeIn();
    $("#MedicationBlock").fadeIn();
    getListSymptom();
    getListDiagnosis();
    getListMedication();
}
//End Consultation functions

//Prescription
function addPrescriptionMedication() {
    if (!validFields('MedicationID,Quantity,MeasurementUnitID,TimeLength,TimeUnitID')) {
        return;
    }
    if (isNaN($("#Quantity").val()) || parseFloat($("#Quantity").val()) <= 0) {
        showErrorNotification("Quantity is compulsory and must be greater than zero.");
        return;
    }
    if (isNaN($("#TimeLength").val()) || parseFloat($("#TimeLength").val()) <= 0) {
        showErrorNotification("Dosage Period is compulsory and must be greater than zero.");
        return;
    }
    toggleLoader(true);
    var return_data = null;
    var Data = {
        PrescriptionID: $("#PrescriptionID").val(),
        MedicalStockItemID: $("#MedicationID").val(),
        Quantity: $("#Quantity").val(),
        MeasurementUnitID: $("#MeasurementUnitID").val(),
        TimeLength: $("#TimeLength").val(),
        TimeUnitID: $("#TimeUnitID").val(),
        Deleted: YesNo.no
    };
    $.ajax({
        type: "POST",
        url: "/Prescription/SaveMedication/",
        data: Data,
        success: function (result) {
            return_data = result;
            handleSuccessResponse(RequestType.other, return_data, null);
            if (return_data.ResponseStatus === ResponseStatus.success) {
                getListPrescriptionMedication();
                $("#MedicationID option[value*='" + $("#MedicationID").val() + "']").prop('disabled', true);
                $("#MedicationID").val('');
                $("#Quantity").val('');
                $("#MeasurementUnitID").val('');
                $("#TimeLength").val('');
                $("#TimeUnitID").val('');
            }
        },
        complete: function () {
            toggleLoader(false);
        },
        error: function (data) {
            handleErrorResponse();
        }
    });
}

function removePrescriptionMedication(record_guid, dropdown_value) {
    toggleLoader(true);
    var return_data = null;
    var Data = {
        PrescriptionMedicationID: record_guid,
        Deleted: YesNo.yes
    };
    $.ajax({
        type: "POST",
        url: "/Prescription/SaveMedication/",
        data: Data,
        success: function (result) {
            return_data = result;
            handleSuccessResponse(RequestType.other, return_data, null);
            if (return_data.ResponseStatus === ResponseStatus.success) {
                getListPrescriptionMedication();
                $("#MedicationID option[value*='" + dropdown_value + "']").prop('disabled', '');
            }
        },
        complete: function () {
            toggleLoader(false);
        },
        error: function (data) {
            handleErrorResponse();
        }
    });
}

function getListPrescriptionMedication() {
    handleRequest(RequestType.get_list, '/Prescription/MedicationList/?_PrescriptionGuid=' + $("#PrescriptionID").val(), null, "MedicationList");
}